import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

/**
  * A base class for retrieving various AUI-ADG resources. The actual location of the resources as well as the protocol
  * are specified by sub classes.
  */
abstract class AuiResourcesBase extends Simulation {
  def getProtocol: String
  def getAddress: String
  def getResource: String

  val versionFeeder = csv("data/aui-versions.csv").random

  val httpConf = http
    .baseURL(getProtocol + "://" + getAddress + "/aui-adg/")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.97 Safari/537.36 Vivaldi/1.94.1008.34")
    .acceptEncodingHeader("gzip, deflate")

  // This breaks down the individual versions in the stats
//  val auiScenario = scenario("AUI CDN Usage").feed(versionFeeder).exec(
//    http("Get resource ${version}/" + getResource).get("${version}/" + getResource)
//  ).pause(2);

  // This combines all of the versions in the stats
  val auiScenario = scenario("AUI CDN Usage").feed(versionFeeder).exec(
    http("Get " + getResource).get("${version}/" + getResource)
  ).pause(2);

  setUp(
    auiScenario.inject(constantUsersPerSec(10) during(2 minutes))
  ).protocols(httpConf)
}
