#!/usr/bin/env bash

# Compare the contents of the aui-adg folder in the original AUI CDN bucket, with the contents of the new
# bucket.
#
# This script relies on you having the cloudtoken tools installed as described at
# https://extranet.atlassian.com/pages/viewpage.action?pageId=3110863330

usage_then_exit () {
    echo "Usage: $0 [dev|stg|prod]"
    exit 1 
}

# Find if a value is present in an array
# $1 is the item to be searched for
# $2 is the array to search within
# return 1 if the item is NOT found
# return 0 (success) if the item IS found
is_present_in_array() {
    local seek=$1
    shift
    local dir=("$@")
    for item in ${dir[*]}; do
        if [ "$seek" == "$item" ]; then
            echo "true"
            return 0
        fi
    done

    echo "false"
}

if [ -z "$1" ]; then
    usage_then_exit
fi

# stop execution on error (-e)
set -e

old_bucket="s3://aui-origin.herokuapp.com/orig/aui-adg/"

dev_bucket="s3://3f01e575428762ed8ccdf24e0173cec72ed15002/aui-adg/"
stg_bucket="s3://3cf44c7ba3a59dc748cbfaf56ef583a28bc8dee8/aui-adg/"
prod_bucket="s3://140350b6bf33a8aff586f9c3fb30044059fffcea/aui-adg/"

dev_role="Truman-Dev-User"
stg_role="Truman-Stg-User"
prod_role="Truman-Prod-User"

if [ "$1" == 'dev' ]
    then
        role=$dev_role
        bucket=$dev_bucket
elif [ "$1" == 'stg' ]
    then
        role=$stg_role
        bucket=$stg_bucket
elif [ "$1" == 'prod' ]
    then
        role=$prod_role
        bucket=$prod_bucket
else
    usage_then_exit
fi

echo "Setting up the token for the AUI-Team role to grab the old bucket listing"
cloudtoken --duo-method phone -f AUI-Team
echo "Grabbing the S3 bucket ${old_bucket}"
orig_dirs=($(aws s3 ls $old_bucket | awk '{sub("^\s+","");sub("/$",""); print $2}'))

echo "Setting up the token for the $role role to grab the new $1 bucket listing"
cloudtoken --duo-method phone -f $role
echo "Grabbing the S3 bucket ${bucket}"
new_dirs=($(aws s3 ls $bucket | awk '{sub("^\s+","");sub("/$",""); print $2}'))

# Loop over each entry in orig_dirs and ensure it is found in new_dirs
# Report any entries from orig_dirs which are not found in new_dirs
echo "The missing entries from ${bucket} are:"
for original in ${orig_dirs[*]}; do
    is_found=$( is_present_in_array $original "${new_dirs[*]}" )
    if [ "${is_found}" == "false" ]; then
        echo "$original"
    fi
done