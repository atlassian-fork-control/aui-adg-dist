#!/usr/bin/env bash

# Dump the contents of the pr folder on the old aui-bucket
# This script relies on you having the cloudtoken tools installed as described at
# https://extranet.atlassian.com/pages/viewpage.action?pageId=3110863330

# stop execution on error (-e)
set -e

bucket="s3://aui-origin.herokuapp.com/orig/atlaskit/pr/"

echo "Setting up the token for the AUI-Team role"
cloudtoken --duo-method phone -f AUI-Team

dirs=($(aws s3 ls s3://aui-origin.herokuapp.com/orig/atlaskit/pr/ | awk '{sub("^\s+","");sub("/$",""); print $2}'))

echo "Downloading ${#dirs[@]} directories from S3"


# TODO
# Define array of directories to skip
# Modify the for loop below to skip a dir if it exists in this array e.g.
# containsElement () {
#  local e match="$1"
#  shift
#  for e; do [[ "$e" == "$match" ]] && return 0; done
#  return 1
# }


counter=1
for dir in ${dirs[*]};
    do
        mkdir legacy/atlaskit/pr/$dir
        aws s3 cp s3://aui-origin.herokuapp.com/orig/atlaskit/pr/$dir legacy/atlaskit/pr/$dir/  --recursive
        counter=$[$counter+1]
        echo "$dir" >> done.txt
        if ( expr $counter % 50 = 0 ); then
            echo "Completed $counter directories"
            cloudtoken --duo-method phone -f AUI-Team
        fi
    done