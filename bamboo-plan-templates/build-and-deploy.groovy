plan(key:'AUICDNDIST', name:'AUI ADG CDN distribution - Build', description:'Build the AUI ADG CDN distribution assets') {
    project(key:'FAB', name:'Product Fabric')

    repository(name:'aui-adg-dist')

    label(name:'AUI')

    label(name:'build')

    label(name:'design-platform')

    trigger(type:'polling',description:'Check for changes', strategy:'periodically',frequency:'180') {
        repository(name:'aui-adg-dist')
    }
    stage(name:'Default Stage') {
        job(key:'JOB1', name:'Build distribution', description:'This builds the many different versions of the AUI CDN assets') {
            requirement(key:'os', condition:'matches', value:'Linux')

            artifactDefinition(name: 'aui-cdn-dist-artifacts', location: './dist',
                    pattern: '**/*', shared: 'true')

            miscellaneousConfiguration(cleanupWorkdirAfterBuild:'true')

            task(type:'checkout',description:'Checkout AUI ADG distribution repository',
                    cleanCheckout:'true') {
                repository(name:'aui-adg-dist')
            }

            task(type:'script', description:'Copy the various distribution versions into place', scriptBody:'''
                    ./bin/create-existing-distributions.sh
                '''.stripIndent())

        }
    }
    dependencies(triggerForBranches:'true')

    permissions() {
        anonymous(permissions:'read')
        loggedInUser(permissions:'read,build')
    }

	planMiscellaneous() {
	   planOwnership(owner:'pcurren')
	}
}

deployment(name:'AUI ADG CDN distribution - Deploy', planKey:'FAB-AUICDNDIST',
    description:'Deploys the AUI ADG assets to Micros (static) managed CDN') {
 
    versioning(version:'release-${bamboo.planKey}-1',autoIncrementNumber:'true')

    environment(name:'Dev') {
        trigger(type:'afterSuccessfulPlan', description:'Automatically go to dev-west2')
        notification(type:'Deployment Failed', recipient:'user', user:'pcurren')

        task(type:'addRequirement',description:'Require Linux') {
            requirement(key:'os',condition:'equals',value:'Linux')
        }

        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download AUI ADG assets artifact', planKey:'FAB-AUICDNDIST') {
            artifact(name:'listing-artifacts', localPath:'./dist')
        }

        task(type:'checkout') {
            repository(name:'aui-adg-dist')
        }

        task(type:'script', description:'Deploy to dev-west2', scriptBody:'''
                ./bin/deploy-existing-distributions.sh dev-west2
            '''.stripIndent())
    }

    environment(name:'Staging') {
        trigger(type:'afterSuccessfulDeployment', environment:'Dev')
        notification(type:'Deployment Failed', recipient:'user',user:'pcurren')

        task(type:'addRequirement',description:'Require Linux') {
            requirement(key:'os',condition:'equals',value:'Linux')
        }

        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download AUI ADG assets artifact', planKey:'FAB-AUICDNDIST') {
            artifact(name:'listing-artifacts', localPath:'./dist')
        }

        task(type:'checkout') {
            repository(name:'aui-adg-dist')
        }

        task(type:'script', description:'Deploy to stg-west2', scriptBody:'''
                ./bin/deploy-existing-distributions.sh stg-west2
            '''.stripIndent())
    }

    environment(name:'Production') {
        notification(type:'Deployment Failed', recipient:'user',user:'pcurren')

        task(type:'addRequirement',description:'Require Linux') {
            requirement(key:'os',condition:'equals',value:'Linux')
        }

        task(type:'cleanWorkingDirectory')

        task(type:'artifactDownload',description:'Download AUI ADG assets artifact', planKey:'FAB-AUICDNDIST') {
            artifact(name:'listing-artifacts', localPath:'./dist')
        }

        task(type:'checkout') {
            repository(name:'aui-adg-dist')
        }

        task(type:'script', description:'Deploy to prod-west2', scriptBody:'''
                ./bin/deploy-existing-distributions.sh prod-west2
            '''.stripIndent())
    }

    permissions() {
        anonymous(permissions:'read')
        loggedInUser(permissions:'read,build')
    } 
}