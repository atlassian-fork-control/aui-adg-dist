#!/bin/sh
set -e

# download previous file from registry to local file
curl -s -o ./mergeData.json 'https://aui-cdn.atlassian.com/atlaskit/registry/api/full.json'

# create output directory
rm -rf ./api
mkdir -p ./api

# run panop
npm run panop -- \
  --repo=atlassian/atlaskit \
  --json=./api/full.json \
  --mergeData=./mergeData.json \
  --requestsPerSecond=40 \
  --plugins=npm-info,js-stats,css-stats

# build single page app json page files
npm run spa-data

# compile site
jekyll build $@
