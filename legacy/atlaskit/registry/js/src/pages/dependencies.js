import { define, prop, props, h } from 'skatejs'; // eslint-disable-line
import { BASE_URL } from '../config';
import TabContent from '../components/tab_content';

const tableStyle = require('!css!less!../css/table.less'); // eslint-disable-line
const typeStyle = require('!css!less!../css/type.less'); // eslint-disable-line

// define columns here. override dependency type with object.
const interestingDeps = [
  { package: 'react', source: 'peerDependencies' },
  { package: 'skatejs', source: 'dependencies' },
  { package: 'akutil-common', source: 'dependencies' },
  { package: 'akutil-shared-styles', source: 'dependencies' },
];

const firstColWidth = 10 - interestingDeps.length;

export default define('akr-page-component-dependencies', {
  props: {
    components: prop.array(),
  },
  attached(elem) {
    fetch('/atlaskit/registry/spa_data/dependencies_page.json')
      .then(result => result.json())
      .then((json) => {
        elem.components = json;
      });
  },
  render(elem) {
    const { components } = elem;

    const header = (
      <div class="row table-row" style={{ 'font-weight': 'bold', 'margin-top': '20px' }}>
        <div class={`col-${firstColWidth}`}>Component</div>
        {
          interestingDeps.map(dep => (
            <div class="col-1">{dep.package}</div>
          ))
        }
      </div>
    );

    function row(component) {
      return (
        <div class="row table-row">
          <div class={`col-${firstColWidth}`}>
            <a href={`${BASE_URL}/${component.name}/latest/index.html`}>{component.name}</a><br />
          </div>
          {
            interestingDeps.map(dep => (
              <div class="col-1">
                {component[dep.source][dep.package] || '-'}
                &nbsp;
              </div>
            ))
          }
        </div>
      );
    }

    return (
      <div>
        <style>
          {tableStyle.toString()}
          {typeStyle.toString()}
        </style>
        <h2>Dependencies</h2>
        <TabContent
          components={components}
          header={header}
          rowFn={row}
        />
      </div>
    );
  },
});
