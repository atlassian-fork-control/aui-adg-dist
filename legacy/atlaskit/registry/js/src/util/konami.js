import Konami from 'konami-js';

export default new Konami(() => {
  document.location.href = '/atlaskit/registry/morty.html';
});
