export function iconClick(elem, componentName) {
  return () => {
    elem.components = elem.components.slice().map((c) => {
      if (c.name === componentName) {
        c.selected = !c.selected;
      }
      return c;
    });
  };
}

export function versionSelected(elem, componentName, version) {
  return () => {
    elem.components = elem.components.slice().map((c) => {
      if (c.name === componentName) {
        c.selectedVersion = version;
      }
      return c;
    });
  };
}

export function resetToLatest(elem) {
  return () => {
    elem.components = elem.components.slice().map((c) => {
      c.selectedVersion = c.versions[0];
      return c;
    });
  };
}

export function generateString(elem) {
  const selectedComponents = elem.components.filter(c => c.selected === true);
  if (!selectedComponents.length) return false;

  const url = selectedComponents.map(c => `${c.name}@${c.selectedVersion}`).join(',');
  return {
    url,
    tag: `<script src="https://eovrzrbij2.execute-api.ap-southeast-2.amazonaws.com/dev/bundle.js?packages=skatejs-web-components,${url}"></script>`,
  };
}

export function setComponentsFromUrl(elem, url) {
  const param = url.match(/packages=([^"]+)/);
  const parsedComponents = param ? param[1].split(',').map(val => val.split('@')) : [];
  elem.components = elem.components.map((component) => {
    const parsedComponent = parsedComponents.find(c => c[0] === component.name);
    component.selected = !!parsedComponent;
    if (parsedComponent) {
      component.selectedVersion = parsedComponent[1] || 'latest';
    }
    return component;
  });
}

export function allSelected(elem) {
  const anyUnselected = elem.components.find(c => !c.selected);
  return anyUnselected ? false : true; // eslint-disable-line
}

export function checkAll(elem) {
  const setTo = !allSelected(elem);
  return () => {
    elem.components = elem.components.slice().map((c) => {
      c.selected = setTo;
      return c;
    });
  };
}
