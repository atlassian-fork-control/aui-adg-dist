import {
  define,
  h, // eslint-disable-line no-unused-vars
} from 'skatejs';
import { BASE_URL } from '../config';

const typeStyle = require('!css!less!../css/type.less'); // eslint-disable-line

export default define('atlaskit-registry-footer', {
  render() {
    return (
      <footer>
        <style>{`
        footer {
          padding: 40px 0;
          color: #999;
        }

        ${typeStyle.toString()}
        `}</style>
        <a href="http://www.atlassian.com/" target="_blank" rel="noopener noreferrer">Atlassian</a>
        &nbsp;
        &bull;
        &nbsp;
        <a href={`${BASE_URL}/api/full.json`}>API data</a>
      </footer>
    );
  },
});
