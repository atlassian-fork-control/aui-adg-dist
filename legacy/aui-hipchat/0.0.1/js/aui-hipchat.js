// node_modules/@atlassian/aui/src/js/aui/jquery.js
(typeof window === 'undefined' ? global : window).__2c07683a8e1fdd32a12b7cc2f919ce59 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  "use strict";
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = window.jQuery || window.Zepto;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/create-element.js
(typeof window === 'undefined' ? global : window).__f22d2496fae72b889743e16dda60342b = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function createElement() {
      var res = null;
  
      if (arguments.length && typeof arguments[0] === 'string') {
          res = (0, _jquery2.default)(document.createElement(arguments[0]));
  
          if (arguments.length === 2) {
              res.html(arguments[1]);
          }
      }
  
      //We can't use the deprecate module or we will introduce a circular dependency
      if (typeof console !== 'undefined' && console.warn) {
          console.warn('AJS\'s create element functionality has been deprecated since 5.9.0.\nNo alternative will be provided.\nUse document.createElement() or jQuery.parseHTML(), or preferably use a templating library.');
      }
  
      return res;
  }
  
  exports.default = createElement;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/object-assign/index.js
(typeof window === 'undefined' ? global : window).__ec027ca8ea7e1fb0ea94a657a558d5df = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  /* eslint-disable no-unused-vars */
  
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  var propIsEnumerable = Object.prototype.propertyIsEnumerable;
  
  function toObject(val) {
  	if (val === null || val === undefined) {
  		throw new TypeError('Object.assign cannot be called with null or undefined');
  	}
  
  	return Object(val);
  }
  
  module.exports = Object.assign || function (target, source) {
  	var from;
  	var to = toObject(target);
  	var symbols;
  
  	for (var s = 1; s < arguments.length; s++) {
  		from = Object(arguments[s]);
  
  		for (var key in from) {
  			if (hasOwnProperty.call(from, key)) {
  				to[key] = from[key];
  			}
  		}
  
  		if (Object.getOwnPropertySymbols) {
  			symbols = Object.getOwnPropertySymbols(from);
  			for (var i = 0; i < symbols.length; i++) {
  				if (propIsEnumerable.call(from, symbols[i])) {
  					to[symbols[i]] = from[symbols[i]];
  				}
  			}
  		}
  	}
  
  	return to;
  };
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/globalize.js
(typeof window === 'undefined' ? global : window).__ec4168b7e78952673964d3581cbee91b = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  exports.default = function (name, value) {
      window[NAMESPACE] = (0, _objectAssign2.default)(_createElement2.default, window[NAMESPACE]);
  
      return window[NAMESPACE][name] = value;
  };
  
  var _createElement = __f22d2496fae72b889743e16dda60342b;
  
  var _createElement2 = _interopRequireDefault(_createElement);
  
  var _objectAssign = __ec027ca8ea7e1fb0ea94a657a558d5df;
  
  var _objectAssign2 = _interopRequireDefault(_objectAssign);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var NAMESPACE = 'AJS';
  
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/format.js
(typeof window === 'undefined' ? global : window).__1269d9fdd712a875e5137f374ca87dae = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * Replaces tokens in a string with arguments, similar to Java's MessageFormat.
   * Tokens are in the form {0}, {1}, {2}, etc.
   *
   * This version also provides support for simple choice formats (excluding floating point numbers) of the form
   * {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}
   *
   * Number format is currently not implemented, tokens of the form {0,number} will simply be printed as {0}
   *
   * @method format
   * @param message the message to replace tokens in
   * @param arg (optional) replacement value for token {0}, with subsequent arguments being {1}, etc.
   * @return {String} the message with the tokens replaced
   * @usage formatString("This is a {0} test", "simple");
   */
  function formatString(message) {
      var apos = /'(?!')/g,
          // founds "'", but not "''"
      simpleFormat = /^\d+$/,
          numberFormat = /^(\d+),number$/,
          // TODO: incomplete, as doesn't support floating point numbers
      choiceFormat = /^(\d+)\,choice\,(.+)/,
          choicePart = /^(\d+)([#<])(.+)/; // TODO: does not work for floating point numbers!
      // we are caching RegExps, so will not spend time on recreating them on each call
  
      // formats a value, currently choice and simple replacement are implemented, proper
      var getParamValue = function getParamValue(format, args) {
          // simple substitute
          /*jshint boss:true */
          var res = '',
              match;
          if (match = format.match(simpleFormat)) {
              // TODO: heavy guns for checking whether format is a simple number...
              res = args.length > ++format ? args[format] : ''; // use the argument as is, or use '' if not found
          }
  
          // number format
          else if (match = format.match(numberFormat)) {
                  // TODO: doesn't actually format the number...
                  res = args.length > ++match[1] ? args[match[1]] : '';
              }
  
              // choice format
              else if (match = format.match(choiceFormat)) {
                      // format: "0,choice,0#0 issues|1#1 issue|1<{0,number} issues"
                      // match[0]: "0,choice,0#0 issues|1#1 issue|1<{0,number} issues"
                      // match[1]: "0"
                      // match[2]: "0#0 issues|1#1 issue|1<{0,number} issues"
  
                      // get the argument value we base the choice on
                      var value = args.length > ++match[1] ? args[match[1]] : null;
                      if (value !== null) {
                          // go through all options, checking against the number, according to following formula,
                          // if X < the first entry then the first entry is returned, if X > last entry, the last entry is returned
                          //
                          //    X matches j if and only if limit[j] <= X < limit[j+1]
                          //
                          var options = match[2].split('|');
  
                          var prevOptionValue = null; // holds last passed option
                          for (var i = 0; i < options.length; i++) {
                              // option: "0#0 issues"
                              // part[0]: "0#0 issues"
                              // part[1]: "0"
                              // part[2]: "#"
                              // part[3]" "0 issues";
                              var parts = options[i].match(choicePart);
  
                              // if value is smaller, we take the previous value, or the current if no previous exists
                              var argValue = parseInt(parts[1], 10);
                              if (value < argValue) {
                                  if (prevOptionValue) {
                                      res = prevOptionValue;
                                      break;
                                  } else {
                                      res = parts[3];
                                      break;
                                  }
                              }
                              // if value is equal the condition, and the match is equality match we accept it
                              if (value == argValue && parts[2] == '#') {
                                  res = parts[3];
                                  break;
                              } else {}
                              // value is greater the condition, fall through to next iteration
  
  
                              // check whether we are the last option, in which case accept it even if the option does not match
                              if (i == options.length - 1) {
                                  res = parts[3];
                              }
  
                              // retain current option
                              prevOptionValue = parts[3];
                          }
  
                          // run result through format, as the parts might contain substitutes themselves
                          var formatArgs = [res].concat(Array.prototype.slice.call(args, 1));
                          res = formatString.apply(null, formatArgs);
                      }
                  }
          return res;
      };
  
      // drop in replacement for the token regex
      // splits the message to return the next accurance of a i18n placeholder.
      // Does not use regexps as we need to support nested placeholders
      // text between single ticks ' are ignored
      var _performTokenRegex = function _performTokenRegex(message) {
          var tick = false,
              openIndex = -1,
              openCount = 0;
          for (var i = 0; i < message.length; i++) {
              // handle ticks
              var c = message.charAt(i);
              if (c == "'") {
                  // toggle
                  tick = !tick;
              }
              // skip if we are between ticks
              if (tick) {
                  continue;
              }
              // check open brackets
              if (c === '{') {
                  if (openCount === 0) {
                      openIndex = i;
                  }
                  openCount++;
              } else if (c === '}') {
                  if (openCount > 0) {
                      openCount--;
                      if (openCount === 0) {
                          // we found a bracket match - generate the result array (
                          var match = [];
                          match.push(message.substring(0, i + 1)); // from begin to match
                          match.push(message.substring(0, openIndex)); // everything until match start
                          match.push(message.substring(openIndex + 1, i)); // matched content
                          return match;
                      }
                  }
              }
          }
          return null;
      };
  
      var _formatString = function _formatString(message) {
          var args = arguments;
          var res = '';
  
          if (!message) {
              return res;
          }
  
          var match = _performTokenRegex(message);
  
          while (match) {
              // reduce message to string after match
              message = message.substring(match[0].length);
  
              // add value before match to result
              res += match[1].replace(apos, '');
  
              // add formatted parameter
              res += getParamValue(match[2], args);
  
              // check for next match
              match = _performTokenRegex(message); //message.match(token);
          }
          // add remaining message to result
          res += message.replace(apos, '');
          return res;
      };
  
      return _formatString.apply(null, arguments);
  }
  
  (0, _globalize2.default)('format', formatString);
  
  exports.default = formatString;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/i18n/aui.js
(typeof window === 'undefined' ? global : window).__bd4bbe259c14e0a6862728a4451ab4b0 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.default = {
      'aui.dropdown.async.error': 'Error loading dropdown',
      'aui.dropdown.async.loading': 'Loading dropdown',
      'aui.words.add': 'Add',
      'aui.words.update': 'Update',
      'aui.words.delete': 'Delete',
      'aui.words.remove': 'Remove',
      'aui.words.cancel': 'Cancel',
      'aui.words.loading': 'Loading',
      'aui.words.close': 'Close',
      'aui.enter.value': 'Enter value',
      'aui.words.more': 'More',
      'aui.words.moreitem': 'More…',
      'aui.keyboard.shortcut.type.x': "Type ''{0}''",
      'aui.keyboard.shortcut.then.x': "then ''{0}''",
      'aui.keyboard.shortcut.or.x': "OR ''{0}''",
      'aui.sidebar.expand.tooltip': 'Expand sidebar ( [ )',
      'aui.sidebar.collapse.tooltip': 'Collapse sidebar ( [ )',
      'aui.validation.message.maxlength': 'Must be fewer than or equal to {0} characters',
      'aui.validation.message.minlength': 'Must be greater than or equal to {0} characters',
      'aui.validation.message.exactlength': 'Must be exactly {0} characters',
      'aui.validation.message.matchingfield': '{0} and {1} do not match.',
      'aui.validation.message.matchingfield-novalue': 'These fields do not match.',
      'aui.validation.message.doesnotcontain': 'Do not include the phrase {0} in this field',
      'aui.validation.message.pattern': 'This field does not match the required format',
      'aui.validation.message.required': 'This is a required field',
      'aui.validation.message.validnumber': 'Please enter a valid number',
      'aui.validation.message.min': 'Enter a value greater than {0}',
      'aui.validation.message.max': 'Enter a value less than {0}',
      'aui.validation.message.dateformat': 'Enter a valid date',
      'aui.validation.message.minchecked': 'Tick at least {0,choice,0#0 checkboxes|1#1 checkbox|1<{0,number} checkboxes}.',
      'aui.validation.message.maxchecked': 'Tick at most {0,choice,0#0 checkboxes|1#1 checkbox|1<{0,number} checkboxes}.',
      'aui.checkboxmultiselect.clear.selected': 'Clear selected items',
      'aui.select.no.suggestions': 'No suggestions',
      'aui.select.new.suggestions': 'New suggestions added. Please use the up and down arrows to select.',
      'aui.select.new.value': 'new value',
      'aui.toggle.on': 'On',
      'aui.toggle.off': 'Off',
      'ajs.datepicker.localisations.day-names.sunday': 'Sunday',
      'ajs.datepicker.localisations.day-names.monday': 'Monday',
      'ajs.datepicker.localisations.day-names.tuesday': 'Tuesday',
      'ajs.datepicker.localisations.day-names.wednesday': 'Wednesday',
      'ajs.datepicker.localisations.day-names.thursday': 'Thursday',
      'ajs.datepicker.localisations.day-names.friday': 'Friday',
      'ajs.datepicker.localisations.day-names.saturday': 'Saturday',
      'ajs.datepicker.localisations.day-names-min.sunday': 'Sun',
      'ajs.datepicker.localisations.day-names-min.monday': 'Mon',
      'ajs.datepicker.localisations.day-names-min.tuesday': 'Tue',
      'ajs.datepicker.localisations.day-names-min.wednesday': 'Wed',
      'ajs.datepicker.localisations.day-names-min.thursday': 'Thu',
      'ajs.datepicker.localisations.day-names-min.friday': 'Fri',
      'ajs.datepicker.localisations.day-names-min.saturday': 'Sat',
      'ajs.datepicker.localisations.first-day': 0,
      'ajs.datepicker.localisations.is-RTL': false,
      'ajs.datepicker.localisations.month-names.january': 'January',
      'ajs.datepicker.localisations.month-names.february': 'February',
      'ajs.datepicker.localisations.month-names.march': 'March',
      'ajs.datepicker.localisations.month-names.april': 'April',
      'ajs.datepicker.localisations.month-names.may': 'May',
      'ajs.datepicker.localisations.month-names.june': 'June',
      'ajs.datepicker.localisations.month-names.july': 'July',
      'ajs.datepicker.localisations.month-names.august': 'August',
      'ajs.datepicker.localisations.month-names.september': 'September',
      'ajs.datepicker.localisations.month-names.october': 'October',
      'ajs.datepicker.localisations.month-names.november': 'November',
      'ajs.datepicker.localisations.month-names.december': 'December',
      'ajs.datepicker.localisations.show-month-after-year': false,
      'ajs.datepicker.localisations.year-suffix': null
  };
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/i18n.js
(typeof window === 'undefined' ? global : window).__686ed4df314a83846f5a3834d1c7497d = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _format = __1269d9fdd712a875e5137f374ca87dae;
  
  var _format2 = _interopRequireDefault(_format);
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  var _aui = __bd4bbe259c14e0a6862728a4451ab4b0;
  
  var _aui2 = _interopRequireDefault(_aui);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * Returns the value defined in AJS.I18n.keys for the given key. If AJS.I18n.keys does not exist, or if the given key does not exist,
   * the key is returned - this could occur in plugin mode if the I18n transform is not performed;
   * or in flatpack mode if the i18n JS file is not loaded.
   */
  var i18n = {
      keys: _aui2.default,
      getText: function getText(key) {
          var params = Array.prototype.slice.call(arguments, 1);
  
          if (Object.prototype.hasOwnProperty.call(this.keys, key)) {
              return _format2.default.apply(null, [this.keys[key]].concat(params));
          }
  
          return key;
      }
  };
  
  (0, _globalize2.default)('I18n', i18n);
  
  exports.default = i18n;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/skatejs-template-html/dist/template-html.js
(typeof window === 'undefined' ? global : window).__13ac5d5912c1c2250ae1f594e62da187 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__13ac5d5912c1c2250ae1f594e62da187");
  define.amd = true;
  
  'use strict';
  
  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
  
  (function () {
  
    var DocumentFragment = window.DocumentFragment;
    var elProto = window.HTMLElement.prototype;
    var matchesSelector = elProto.matches || elProto.msMatchesSelector || elProto.webkitMatchesSelector || elProto.mozMatchesSelector || elProto.oMatchesSelector;
  
    function getData(element, name) {
      if (element.__SKATE_TEMPLATE_HTML_DATA) {
        return element.__SKATE_TEMPLATE_HTML_DATA[name];
      }
    }
  
    function setData(element, name, value) {
      if (!element.__SKATE_TEMPLATE_HTML_DATA) {
        element.__SKATE_TEMPLATE_HTML_DATA = {};
      }
  
      element.__SKATE_TEMPLATE_HTML_DATA[name] = value;
  
      return element;
    }
  
    function createFragmentFromString(domString) {
      var specialMap = {
        caption: 'table',
        dd: 'dl',
        dt: 'dl',
        li: 'ul',
        tbody: 'table',
        td: 'tr',
        thead: 'table',
        tr: 'tbody'
      };
  
      var tag = domString.match(/\s*<([^\s>]+)/);
      var div = document.createElement(tag && specialMap[tag[1]] || 'div');
  
      div.innerHTML = domString;
  
      return createFragmentFromNodeList(div.childNodes);
    }
  
    function createFragmentFromNodeList(nodeList) {
      var frag = document.createDocumentFragment();
  
      while (nodeList && nodeList.length) {
        frag.appendChild(nodeList[0]);
      }
  
      return frag;
    }
  
    function getNodesBetween(startNode, endNode) {
      var nodes = [];
      var nextNode = startNode.nextSibling;
  
      while (nextNode !== endNode) {
        nodes.push(nextNode);
        nextNode = nextNode.nextSibling;
      }
  
      return nodes;
    }
  
    function findChildrenMatchingSelector(sourceNode, selector) {
      if (selector) {
        var found = sourceNode.querySelectorAll(selector);
        var foundLength = found.length;
        var filtered = [];
  
        for (var a = 0; a < foundLength; a++) {
          var node = found[a];
  
          if (node.parentNode === sourceNode) {
            filtered.push(node);
          }
        }
  
        return filtered;
      }
  
      return [].slice.call(sourceNode.childNodes) || [];
    }
  
    function htmlTemplateParentWrapper(element) {
      var contentNodes = getData(element, 'content');
      var contentNodesLen = contentNodes.length;
  
      return {
        childNodes: {
          get: function get() {
            var nodes = [];
  
            for (var a = 0; a < contentNodesLen; a++) {
              var contentNode = contentNodes[a];
  
              if (contentNode.isDefault) {
                continue;
              }
  
              nodes = nodes.concat(getNodesBetween(contentNode.startNode, contentNode.endNode));
            }
  
            return nodes;
          }
        },
  
        firstChild: {
          get: function get() {
            var childNodes = this.childNodes;
            return childNodes.length && childNodes[0] || null;
          }
        },
  
        innerHTML: {
          get: function get() {
            var html = '';
            var childNodes = this.childNodes;
            var childNodesLen = childNodes.length;
  
            for (var a = 0; a < childNodesLen; a++) {
              var childNode = childNodes[a];
              html += childNode.outerHTML || childNode.textContent;
            }
  
            return html;
          },
          set: function set(html) {
            var targetFragment = createFragmentFromString(html);
  
            for (var a = 0; a < contentNodesLen; a++) {
              var contentNode = contentNodes[a];
              var childNodes = getNodesBetween(contentNode.startNode, contentNode.endNode);
  
              // Remove all nodes (including default content).
              for (var b = 0; b < childNodes.length; b++) {
                var childNode = childNodes[b];
                childNode.parentNode.removeChild(childNode);
              }
  
              var foundNodes = findChildrenMatchingSelector(targetFragment, contentNode.selector);
  
              // Add any matched nodes from the given HTML.
              for (var c = 0; c < foundNodes.length; c++) {
                contentNode.container.insertBefore(foundNodes[c], contentNode.endNode);
              }
  
              // If no nodes were found, set the default content.
              if (foundNodes.length) {
                removeDefaultContent(contentNode);
              } else {
                addDefaultContent(contentNode);
              }
            }
          }
        },
  
        lastChild: {
          get: function get() {
            for (var a = contentNodesLen - 1; a > -1; a--) {
              var contentNode = contentNodes[a];
  
              if (contentNode.isDefault) {
                continue;
              }
  
              var childNodes = this.childNodes;
              var childNodesLen = childNodes.length;
  
              return childNodes[childNodesLen - 1];
            }
  
            return null;
          }
        },
  
        outerHTML: {
          get: function get() {
            var name = this.tagName.toLowerCase();
            var html = '<' + name;
            var attrs = this.attributes;
  
            if (attrs) {
              var attrsLength = attrs.length;
  
              for (var a = 0; a < attrsLength; a++) {
                var attr = attrs[a];
                html += ' ' + attr.nodeName + '="' + attr.nodeValue + '"';
              }
            }
  
            html += '>';
            html += this.innerHTML;
            html += '</' + name + '>';
  
            return html;
          }
        },
  
        textContent: {
          get: function get() {
            var textContent = '';
            var childNodes = this.childNodes;
            var childNodesLength = this.childNodes.length;
  
            for (var a = 0; a < childNodesLength; a++) {
              textContent += childNodes[a].textContent;
            }
  
            return textContent;
          },
          set: function set(textContent) {
            var acceptsTextContent;
  
            // Removes all nodes (including default content).
            this.innerHTML = '';
  
            // Find the first content node without a selector.
            for (var a = 0; a < contentNodesLen; a++) {
              var contentNode = contentNodes[a];
  
              if (!contentNode.selector) {
                acceptsTextContent = contentNode;
                break;
              }
            }
  
            // There may be no content nodes that accept text content.
            if (acceptsTextContent) {
              if (textContent) {
                removeDefaultContent(acceptsTextContent);
                acceptsTextContent.container.insertBefore(document.createTextNode(textContent), acceptsTextContent.endNode);
              } else {
                addDefaultContent(acceptsTextContent);
              }
            }
          }
        },
  
        appendChild: {
          value: function value(node) {
            if (node instanceof DocumentFragment) {
              var fragChildNodes = node.childNodes;
  
              [].slice.call(fragChildNodes).forEach(function (node) {
                this.appendChild(node);
              }.bind(this));
  
              return this;
            }
  
            for (var b = 0; b < contentNodesLen; b++) {
              var contentNode = contentNodes[b];
              var contentSelector = contentNode.selector;
  
              if (!contentSelector || node instanceof window.HTMLElement && matchesSelector.call(node, contentSelector)) {
                removeDefaultContent(contentNode);
                contentNode.endNode.parentNode.insertBefore(node, contentNode.endNode);
                break;
              }
            }
  
            return this;
          }
        },
  
        insertAdjacentHTML: {
          value: function value(where, html) {
            if (where === 'afterbegin') {
              this.insertBefore(createFragmentFromString(html), this.childNodes[0]);
            } else if (where === 'beforeend') {
              this.appendChild(createFragmentFromString(html));
            } else {
              element.insertAdjacentHTML(where, html);
            }
  
            return this;
          }
        },
  
        insertBefore: {
          value: function value(node, referenceNode) {
            // If no reference node is supplied, we append. This also means that we
            // don't need to add / remove any default content because either there
            // aren't any nodes or appendChild will handle it.
            if (!referenceNode) {
              return this.appendChild(node);
            }
  
            // Handle document fragments.
            if (node instanceof DocumentFragment) {
              var fragChildNodes = node.childNodes;
  
              if (fragChildNodes) {
                var fragChildNodesLength = fragChildNodes.length;
  
                for (var a = 0; a < fragChildNodesLength; a++) {
                  this.insertBefore(fragChildNodes[a], referenceNode);
                }
              }
  
              return this;
            }
  
            var hasFoundReferenceNode = false;
  
            // There's no reason to handle default content add / remove because:
            // 1. If no reference node is supplied, appendChild handles it.
            // 2. If a reference node is supplied, there already is content.
            // 3. If a reference node is invalid, an exception is thrown, but also
            //    it's state would not change even if it wasn't.
            mainLoop: for (var b = 0; b < contentNodesLen; b++) {
              var contentNode = contentNodes[b];
              var betweenNodes = getNodesBetween(contentNode.startNode, contentNode.endNode);
              var betweenNodesLen = betweenNodes.length;
  
              for (var c = 0; c < betweenNodesLen; c++) {
                var betweenNode = betweenNodes[c];
  
                if (betweenNode === referenceNode) {
                  hasFoundReferenceNode = true;
                }
  
                if (hasFoundReferenceNode) {
                  var selector = contentNode.selector;
  
                  if (!selector || matchesSelector.call(node, selector)) {
                    betweenNode.parentNode.insertBefore(node, betweenNode);
                    break mainLoop;
                  }
                }
              }
            }
  
            // If no reference node was found as a child node of the element we must
            // throw an error. This works for both no child nodes, or if the
            // reference wasn't found to be a child node.
            if (!hasFoundReferenceNode) {
              throw new Error('DOMException 8: The node before which the new node is to be inserted is not a child of this node.');
            }
  
            return node;
          }
        },
  
        removeChild: {
          value: function value(childNode) {
            var removed = false;
  
            for (var a = 0; a < contentNodesLen; a++) {
              var contentNode = contentNodes[a];
  
              if (contentNode.container === childNode.parentNode) {
                contentNode.container.removeChild(childNode);
                removed = true;
                break;
              }
  
              if (contentNode.startNode.nextSibling === contentNode.endNode) {
                addDefaultContent(contentNode);
              }
            }
  
            if (!removed) {
              throw new Error('DOMException 8: The node in which you are trying to remove is not a child of this node.');
            }
  
            return childNode;
          }
        },
  
        replaceChild: {
          value: function value(newChild, oldChild) {
            for (var a = 0; a < contentNodesLen; a++) {
              var contentNode = contentNodes[a];
  
              if (contentNode.container === oldChild.parentNode) {
                contentNode.container.replaceChild(newChild, oldChild);
                break;
              }
            }
  
            return this;
          }
        }
      };
    }
  
    function addDefaultContent(content) {
      var nodes = content.defaultNodes;
      var nodesLen = nodes.length;
  
      for (var a = 0; a < nodesLen; a++) {
        content.container.insertBefore(nodes[a], content.endNode);
      }
  
      content.isDefault = true;
    }
  
    function removeDefaultContent(content) {
      var nodes = content.defaultNodes;
      var nodesLen = nodes.length;
  
      for (var a = 0; a < nodesLen; a++) {
        var node = nodes[a];
        node.parentNode.removeChild(node);
      }
  
      content.isDefault = false;
    }
  
    function createProxyProperty(node, name) {
      return {
        get: function get() {
          var value = node[name];
  
          if (typeof value === 'function') {
            return value.bind(node);
          }
  
          return value;
        },
  
        set: function set(value) {
          node[name] = value;
        }
      };
    }
  
    function wrapNodeWith(node, wrapper) {
      var wrapped = {};
  
      for (var name in node) {
        var inWrapper = name in wrapper;
  
        if (inWrapper) {
          Object.defineProperty(wrapped, name, wrapper[name]);
        } else {
          Object.defineProperty(wrapped, name, createProxyProperty(node, name));
        }
      }
  
      return wrapped;
    }
  
    function cacheContentData(node) {
      var contentNodes = node.getElementsByTagName('content');
      var contentNodesLen = contentNodes && contentNodes.length;
  
      if (contentNodesLen) {
        var contentData = [];
  
        while (contentNodes.length) {
          var contentNode = contentNodes[0];
          var parentNode = contentNode.parentNode;
          var selector = contentNode.getAttribute('select');
          var startNode = document.createComment(' content ');
          var endNode = document.createComment(' /content ');
  
          contentData.push({
            container: parentNode,
            contentNode: contentNode,
            defaultNodes: [].slice.call(contentNode.childNodes),
            endNode: endNode,
            isDefault: true,
            selector: selector,
            startNode: startNode
          });
  
          parentNode.replaceChild(endNode, contentNode);
          parentNode.insertBefore(startNode, endNode);
  
          // Cache data in the comment that can be read if no content information
          // is cached. This allows seamless server-side rendering.
          startNode.textContent += JSON.stringify({
            defaultContent: contentNode.innerHTML,
            selector: selector
          }) + ' ';
        }
  
        setData(node, 'content', contentData);
      }
    }
  
    // Content Parser
    // --------------
  
    function parseCommentNode(node) {
      var data;
      var matches = node.textContent.match(/^ (\/?)content (.*)/i);
  
      if (matches) {
        if (matches[2]) {
          try {
            data = JSON.parse(matches[2]);
          } catch (e) {
            throw new Error('Unable to parse content comment data: "' + e + '" in "<!--' + node.textContent + '-->".');
          }
        }
  
        return {
          data: data || {
            defaultContent: undefined,
            isDefault: undefined,
            selector: undefined
          },
          type: matches[1] ? 'close' : 'open'
        };
      }
    }
  
    function parseNodeForContent(node) {
      var a;
      var childNodes = node.childNodes;
      var childNodesLen = childNodes.length;
      var contentDatas = [];
      var lastContentNode;
  
      for (a = 0; a < childNodesLen; a++) {
        var childNode = childNodes[a];
  
        if (childNode.nodeType === 8) {
          var contentInfo = parseCommentNode(childNode);
  
          if (contentInfo) {
            if (contentInfo.type === 'open') {
              if (lastContentNode) {
                throw new Error('Cannot have an opening content placeholder after another content placeholder at the same level in the DOM tree: "' + childNode.textContent + '" in "' + childNode.parentNode.innerHTML + '".');
              }
  
              lastContentNode = {
                container: childNode.parentNode,
                contentNode: childNode,
                defaultNodes: contentInfo.data.defaultContent && createFragmentFromString(contentInfo.data.defaultContent).childNodes || [],
                isDefault: contentInfo.data.isDefault,
                selector: contentInfo.data.selector,
                startNode: childNode
              };
            } else if (contentInfo.type === 'close') {
              if (!lastContentNode) {
                throw new Error('Unmatched closing content placeholder: "' + childNode.textContent + '" in "' + childNode.parentNode.innerHTML + '".');
              }
  
              lastContentNode.endNode = childNode;
              contentDatas.push(lastContentNode);
              lastContentNode = undefined;
            }
          }
        } else {
          contentDatas = contentDatas.concat(parseNodeForContent(childNode));
        }
      }
  
      return contentDatas;
    }
  
    // Public API
    // ----------
  
    function skateTemplateHtml() {
      var template = [].slice.call(arguments).join('');
  
      return function (target) {
        var frag = createFragmentFromNodeList(target.childNodes);
  
        target.innerHTML = template;
        cacheContentData(target);
  
        if (frag.childNodes.length) {
          skateTemplateHtml.wrap(target).appendChild(frag);
        }
      };
    }
  
    skateTemplateHtml.wrap = function (node) {
      if (!getData(node, 'content')) {
        setData(node, 'content', parseNodeForContent(node));
      }
  
      return wrapNodeWith(node, htmlTemplateParentWrapper(node));
    };
  
    // Exporting
    // ---------
  
    // Global.
    window.skateTemplateHtml = skateTemplateHtml;
  
    // AMD.
    if (typeof define === 'function') {
      define(function () {
        return skateTemplateHtml;
      });
    }
  
    // CommonJS.
    if ((typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object') {
      module.exports = skateTemplateHtml;
    }
  })();
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/deprecation.js
(typeof window === 'undefined' ? global : window).__2d336d5b3a38711b48236d5af8c39b51 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.getMessageLogger = exports.propertyDeprecationSupported = exports.obj = exports.prop = exports.css = exports.construct = exports.fn = undefined;
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var has = Object.prototype.hasOwnProperty;
  var deprecationCalls = [];
  var deprecatedSelectorMap = [];
  
  function toSentenceCase(str) {
      str += '';
  
      if (!str) {
          return '';
      }
  
      return str.charAt(0).toUpperCase() + str.substring(1);
  }
  
  function getDeprecatedLocation(printFrameOffset) {
      var err = new Error();
      var stack = err.stack || err.stacktrace;
      var stackMessage = stack && stack.replace(/^Error\n/, '') || '';
  
      if (stackMessage) {
          stackMessage = stackMessage.split('\n');
          return stackMessage[printFrameOffset + 2];
      }
      return stackMessage;
  }
  
  function logger() {
      if (typeof console !== 'undefined' && console.warn) {
          Function.prototype.apply.call(console.warn, console, arguments);
      }
  }
  
  /**
   * Return a function that logs a deprecation warning to the console the first time it is called from a certain location.
   * It will also print the stack frame of the calling function.
   *
   * @param {string} displayName the name of the thing being deprecated
   * @param {object} options
   * @param {string} options.removeInVersion the version this will be removed in
   * @param {string} options.alternativeName the name of an alternative to use
   * @param {string} options.sinceVersion the version this has been deprecated since
   * @param {string} options.extraInfo extra information to be printed at the end of the deprecation log
   * @param {string} options.extraObject an extra object that will be printed at the end
   * @param {string} options.deprecationType type of the deprecation to append to the start of the deprecation message. e.g. JS or CSS
   * @return {Function} that logs the warning and stack frame of the calling function. Takes in an optional parameter for the offset of
   * the stack frame to print, the default is 0. For example, 0 will log it for the line of the calling function,
   * -1 will print the location the logger was called from
   */
  function getShowDeprecationMessage(displayName, options) {
      // This can be used internally to pas in a showmessage fn
      if (typeof displayName === 'function') {
          return displayName;
      }
  
      var called = false;
      options = options || {};
  
      return function (printFrameOffset) {
          var deprecatedLocation = getDeprecatedLocation(printFrameOffset ? printFrameOffset : 1) || '';
          // Only log once if the stack frame doesn't exist to avoid spamming the console/test output
          if (!called || deprecationCalls.indexOf(deprecatedLocation) === -1) {
              deprecationCalls.push(deprecatedLocation);
  
              called = true;
  
              var deprecationType = options.deprecationType + ' ' || '';
  
              var message = 'DEPRECATED ' + deprecationType + '- ' + toSentenceCase(displayName) + ' has been deprecated' + (options.sinceVersion ? ' since ' + options.sinceVersion : '') + ' and will be removed in ' + (options.removeInVersion || 'a future release') + '.';
  
              if (options.alternativeName) {
                  message += ' Use ' + options.alternativeName + ' instead. ';
              }
  
              if (options.extraInfo) {
                  message += ' ' + options.extraInfo;
              }
  
              if (deprecatedLocation === '') {
                  deprecatedLocation = ' \n ' + 'No stack trace of the deprecated usage is available in your current browser.';
              } else {
                  deprecatedLocation = ' \n ' + deprecatedLocation;
              }
  
              if (options.extraObject) {
                  message += '\n';
                  logger(message, options.extraObject, deprecatedLocation);
              } else {
                  logger(message, deprecatedLocation);
              }
          }
      };
  }
  
  function logCssDeprecation(selectorMap, newNode) {
      var displayName = selectorMap.options.displayName;
      displayName = displayName ? ' (' + displayName + ')' : '';
  
      var options = _jquery2.default.extend({
          deprecationType: 'CSS',
          extraObject: newNode
      }, selectorMap.options);
  
      getShowDeprecationMessage('\'' + selectorMap.selector + '\' pattern' + displayName, options)();
  }
  
  /**
  * Returns a wrapped version of the function that logs a deprecation warning when the function is used.
  * @param {Function} fn the fn to wrap
  * @param {string} displayName the name of the fn to be displayed in the message
  * @param {string} options.removeInVersion the version this will be removed in
  * @param {string} options.alternativeName the name of an alternative to use
  * @param {string} options.sinceVersion the version this has been deprecated since
  * @param {string} options.extraInfo extra information to be printed at the end of the deprecation log
  * @return {Function} wrapping the original function
  */
  function deprecateFunctionExpression(fn, displayName, options) {
      options = options || {};
      options.deprecationType = options.deprecationType || 'JS';
  
      var showDeprecationMessage = getShowDeprecationMessage(displayName || fn.name || 'this function', options);
      return function () {
          showDeprecationMessage();
          return fn.apply(this, arguments);
      };
  }
  
  /**
  * Returns a wrapped version of the constructor that logs a deprecation warning when the constructor is instantiated.
  * @param {Function} constructorFn the constructor function to wrap
  * @param {string} displayName the name of the fn to be displayed in the message
  * @param {string} options.removeInVersion the version this will be removed in
  * @param {string} options.alternativeName the name of an alternative to use
  * @param {string} options.sinceVersion the version this has been deprecated since
  * @param {string} options.extraInfo extra information to be printed at the end of the deprecation log
  * @return {Function} wrapping the original function
  */
  function deprecateConstructor(constructorFn, displayName, options) {
      options = options || {};
      options.deprecationType = options.deprecationType || 'JS';
  
      var deprecatedConstructor = deprecateFunctionExpression(constructorFn, displayName, options);
      deprecatedConstructor.prototype = constructorFn.prototype;
      _jquery2.default.extend(deprecatedConstructor, constructorFn); //copy static methods across;
  
      return deprecatedConstructor;
  }
  
  var supportsProperties = false;
  try {
      if (Object.defineProperty) {
          Object.defineProperty({}, 'blam', { get: function get() {}, set: function set() {} });
          exports.propertyDeprecationSupported = supportsProperties = true;
      }
  } catch (e) {}
  /* IE8 doesn't support on non-DOM elements */
  
  
  /**
  * Wraps a "value" object property in a deprecation warning in browsers supporting Object.defineProperty
  * @param {Object} obj the object containing the property
  * @param {string} prop the name of the property to deprecate
  * @param {string} options.removeInVersion the version this will be removed in
  * @param {string} options.displayName the display name of the property to deprecate (optional, will fall back to the property name)
  * @param {string} options.alternativeName the name of an alternative to use
  * @param {string} options.sinceVersion the version this has been deprecated since
  * @param {string} options.extraInfo extra information to be printed at the end of the deprecation log
  */
  function deprecateValueProperty(obj, prop, options) {
      if (supportsProperties) {
          var oldVal = obj[prop];
          options = options || {};
          options.deprecationType = options.deprecationType || 'JS';
  
          var displayNameOrShowMessageFn = options.displayName || prop;
          var showDeprecationMessage = getShowDeprecationMessage(displayNameOrShowMessageFn, options);
          Object.defineProperty(obj, prop, {
              get: function get() {
                  showDeprecationMessage();
                  return oldVal;
              },
              set: function set(val) {
                  oldVal = val;
                  showDeprecationMessage();
                  return val;
              }
          });
      }
  }
  
  /**
  * Wraps an object property in a deprecation warning, if possible. functions will always log warnings, but other
  * types of properties will only log in browsers supporting Object.defineProperty
  * @param {Object} obj the object containing the property
  * @param {string} prop the name of the property to deprecate
  * @param {string} options.removeInVersion the version this will be removed in
  * @param {string} options.displayName the display name of the property to deprecate (optional, will fall back to the property name)
  * @param {string} options.alternativeName the name of an alternative to use
  * @param {string} options.sinceVersion the version this has been deprecated since
  * @param {string} options.extraInfo extra information to be printed at the end of the deprecation log
  */
  function deprecateObjectProperty(obj, prop, options) {
      if (typeof obj[prop] === 'function') {
          options = options || {};
          options.deprecationType = options.deprecationType || 'JS';
  
          var displayNameOrShowMessageFn = options.displayName || prop;
          obj[prop] = deprecateFunctionExpression(obj[prop], displayNameOrShowMessageFn, options);
      } else {
          deprecateValueProperty(obj, prop, options);
      }
  }
  
  /**
  * Wraps all an objects properties in a deprecation warning, if possible. functions will always log warnings, but other
  * types of properties will only log in browsers supporting Object.defineProperty
  * @param {Object} obj the object to be wrapped
  * @param {string} objDisplayPrefix the object's prefix to be used in logs
  * @param {string} options.removeInVersion the version this will be removed in
  * @param {string} options.alternativeNamePrefix the name of another object to prefix the deprecated objects properties with
  * @param {string} options.sinceVersion the version this has been deprecated since
  * @param {string} options.extraInfo extra information to be printed at the end of the deprecation log
  */
  function deprecateAllProperties(obj, objDisplayPrefix, options) {
      options = options || {};
      for (var attr in obj) {
          if (has.call(obj, attr)) {
              options.deprecationType = options.deprecationType || 'JS';
              options.displayName = objDisplayPrefix + attr;
              options.alternativeName = options.alternativeNamePrefix && options.alternativeNamePrefix + attr;
              deprecateObjectProperty(obj, attr, _jquery2.default.extend({}, options));
          }
      }
  }
  
  function matchesSelector(el, selector) {
      return (el.matches || el.msMatchesSelector || el.webkitMatchesSelector || el.mozMatchesSelector || el.oMatchesSelector).call(el, selector);
  }
  
  function handleAddingSelector(options) {
      return function (selector) {
          var selectorMap = {
              selector: selector,
              options: options || {}
          };
  
          deprecatedSelectorMap.push(selectorMap);
  
          // Search if matches have already been added
          var matches = document.querySelectorAll(selector);
          for (var i = 0; i < matches.length; i++) {
              logCssDeprecation(selectorMap, matches[i]);
          }
      };
  }
  
  /**
  * Return a function that logs a deprecation warning to the console the first time it is called from a certain location.
  * It will also print the stack frame of the calling function.
  *
  * @param {string|Array} selectors a selector or list of selectors that match deprecated markup
  * @param {object} options
  * @param {string} options.displayName a name describing these selectors
  * @param {string} options.alternativeName the name of an alternative to use
  * @param {string} options.removeInVersion the version these will be removed in
  * @param {string} options.sinceVersion the version these have been deprecated since
  * @param {string} options.extraInfo extra information to be printed at the end of the deprecation log
  */
  function deprecateCSS(selectors, options) {
      if (!window.MutationObserver) {
          logger('CSS could not be deprecated as Mutation Observer was not found.');
          return;
      }
  
      if (typeof selectors === 'string') {
          selectors = [selectors];
      }
  
      selectors.forEach(handleAddingSelector(options));
  }
  
  function testAndHandleDeprecation(newNode) {
      return function (selectorMap) {
          if (matchesSelector(newNode, selectorMap.selector)) {
              logCssDeprecation(selectorMap, newNode);
          }
      };
  }
  
  if (window.MutationObserver) {
      var observer = new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
              // TODO - should this also look at class changes, if possible?
              var addedNodes = mutation.addedNodes;
  
              for (var i = 0; i < addedNodes.length; i++) {
                  var newNode = addedNodes[i];
                  if (newNode.nodeType === 1) {
                      deprecatedSelectorMap.forEach(testAndHandleDeprecation(newNode));
                  }
              }
          });
      });
  
      var config = {
          childList: true,
          subtree: true
      };
  
      observer.observe(document, config);
  }
  
  var deprecate = {
      fn: deprecateFunctionExpression,
      construct: deprecateConstructor,
      css: deprecateCSS,
      prop: deprecateObjectProperty,
      obj: deprecateAllProperties,
      propertyDeprecationSupported: supportsProperties,
      getMessageLogger: getShowDeprecationMessage
  };
  
  (0, _globalize2.default)('deprecate', deprecate);
  
  exports.fn = deprecateFunctionExpression;
  exports.construct = deprecateConstructor;
  exports.css = deprecateCSS;
  exports.prop = deprecateObjectProperty;
  exports.obj = deprecateAllProperties;
  exports.propertyDeprecationSupported = supportsProperties;
  exports.getMessageLogger = getShowDeprecationMessage;
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/log.js
(typeof window === 'undefined' ? global : window).__3db7d70f11d6400b2fe84fc4d957c72d = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.error = exports.warn = exports.log = undefined;
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function polyfillConsole(prop) {
      return function () {
          if (typeof console !== 'undefined' && console[prop]) {
              Function.prototype.apply.call(console[prop], console, arguments);
          }
      };
  }
  
  var log = polyfillConsole('log');
  var warn = polyfillConsole('warn');
  var error = polyfillConsole('error');
  
  (0, _globalize2.default)('error', error);
  (0, _globalize2.default)('log', log);
  (0, _globalize2.default)('warn', warn);
  
  exports.log = log;
  exports.warn = warn;
  exports.error = error;
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/debounce.js
(typeof window === 'undefined' ? global : window).__b6c2d68b946981b653c1247d4bef7c4c = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.default = debounce;
  exports.debounceImmediate = debounceImmediate;
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function debounce(func, wait) {
      var timeout;
      var result;
  
      return function () {
          var args = arguments;
          var context = this;
          var later = function later() {
              result = func.apply(context, args);
              context = args = null;
          };
  
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
  
          return result;
      };
  }
  
  (0, _globalize2.default)('debounce', debounce);
  
  function debounceImmediate(func, wait) {
      var timeout = null;
      var result;
  
      return function () {
          var context = this;
          var args = arguments;
          var later = function later() {
              timeout = context = args = null;
          };
  
          if (timeout === null) {
              result = func.apply(context, args);
          }
  
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
  
          return result;
      };
  }
  
  (0, _globalize2.default)('debounceImmediate', debounceImmediate);
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/browser.js
(typeof window === 'undefined' ? global : window).__8cdce9eb1fdffc13125d51ff33f17b88 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.supportsCalc = supportsCalc;
  exports.supportsRequestAnimationFrame = supportsRequestAnimationFrame;
  exports.supportsVoiceOver = supportsVoiceOver;
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var supportsCalc = null;
  var isMacOSX = /Mac OS X/.test(navigator.userAgent);
  
  function supportsCalc() {
      if (supportsCalc === null) {
          var $d = (0, _jquery2.default)('<div style="height: 10px; height: -webkit-calc(20px + 0); height: calc(20px);"></div>');
          exports.supportsCalc = supportsCalc = 20 === $d.appendTo(document.documentElement).height();
          $d.remove();
      }
  
      return supportsCalc;
  }
  
  function supportsRequestAnimationFrame() {
      return !!window.requestAnimationFrame;
  }
  
  function supportsVoiceOver() {
      return isMacOSX;
  }
  
  return module.exports;
}).call(this);
// node_modules/tether/tether.js
(typeof window === 'undefined' ? global : window).__2c6af606fcf6072e609bc1ad585d898a = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__2c6af606fcf6072e609bc1ad585d898a");
  define.amd = true;
  
  /*! tether 0.6.5 */
  (function(root) {
  	(function() {
    var Evented, addClass, defer, deferred, extend, flush, getBounds, getOffsetParent, getOrigin, getScrollBarSize, getScrollParent, hasClass, node, removeClass, uniqueId, updateClasses, zeroPosCache,
      __hasProp = {}.hasOwnProperty,
      __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
      __slice = [].slice;
  
    if (this.Tether == null) {
      this.Tether = {
        modules: []
      };
    }
  
    getScrollParent = function(el) {
      var parent, position, scrollParent, style, _ref;
      position = getComputedStyle(el).position;
      if (position === 'fixed') {
        return el;
      }
      scrollParent = void 0;
      parent = el;
      while (parent = parent.parentNode) {
        try {
          style = getComputedStyle(parent);
        } catch (_error) {}
        if (style == null) {
          return parent;
        }
        if (/(auto|scroll)/.test(style['overflow'] + style['overflow-y'] + style['overflow-x'])) {
          if (position !== 'absolute' || ((_ref = style['position']) === 'relative' || _ref === 'absolute' || _ref === 'fixed')) {
            return parent;
          }
        }
      }
      return document.body;
    };
  
    uniqueId = (function() {
      var id;
      id = 0;
      return function() {
        return id++;
      };
    })();
  
    zeroPosCache = {};
  
    getOrigin = function(doc) {
      var id, k, node, v, _ref;
      node = doc._tetherZeroElement;
      if (node == null) {
        node = doc.createElement('div');
        node.setAttribute('data-tether-id', uniqueId());
        extend(node.style, {
          top: 0,
          left: 0,
          position: 'absolute'
        });
        doc.body.appendChild(node);
        doc._tetherZeroElement = node;
      }
      id = node.getAttribute('data-tether-id');
      if (zeroPosCache[id] == null) {
        zeroPosCache[id] = {};
        _ref = node.getBoundingClientRect();
        for (k in _ref) {
          v = _ref[k];
          zeroPosCache[id][k] = v;
        }
        defer(function() {
          return zeroPosCache[id] = void 0;
        });
      }
      return zeroPosCache[id];
    };
  
    node = null;
  
    getBounds = function(el) {
      var box, doc, docEl, k, origin, v, _ref;
      if (el === document) {
        doc = document;
        el = document.documentElement;
      } else {
        doc = el.ownerDocument;
      }
      docEl = doc.documentElement;
      box = {};
      _ref = el.getBoundingClientRect();
      for (k in _ref) {
        v = _ref[k];
        box[k] = v;
      }
      origin = getOrigin(doc);
      box.top -= origin.top;
      box.left -= origin.left;
      if (box.width == null) {
        box.width = document.body.scrollWidth - box.left - box.right;
      }
      if (box.height == null) {
        box.height = document.body.scrollHeight - box.top - box.bottom;
      }
      box.top = box.top - docEl.clientTop;
      box.left = box.left - docEl.clientLeft;
      box.right = doc.body.clientWidth - box.width - box.left;
      box.bottom = doc.body.clientHeight - box.height - box.top;
      return box;
    };
  
    getOffsetParent = function(el) {
      return el.offsetParent || document.documentElement;
    };
  
    getScrollBarSize = function() {
      var inner, outer, width, widthContained, widthScroll;
      inner = document.createElement('div');
      inner.style.width = '100%';
      inner.style.height = '200px';
      outer = document.createElement('div');
      extend(outer.style, {
        position: 'absolute',
        top: 0,
        left: 0,
        pointerEvents: 'none',
        visibility: 'hidden',
        width: '200px',
        height: '150px',
        overflow: 'hidden'
      });
      outer.appendChild(inner);
      document.body.appendChild(outer);
      widthContained = inner.offsetWidth;
      outer.style.overflow = 'scroll';
      widthScroll = inner.offsetWidth;
      if (widthContained === widthScroll) {
        widthScroll = outer.clientWidth;
      }
      document.body.removeChild(outer);
      width = widthContained - widthScroll;
      return {
        width: width,
        height: width
      };
    };
  
    extend = function(out) {
      var args, key, obj, val, _i, _len, _ref;
      if (out == null) {
        out = {};
      }
      args = [];
      Array.prototype.push.apply(args, arguments);
      _ref = args.slice(1);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        obj = _ref[_i];
        if (obj) {
          for (key in obj) {
            if (!__hasProp.call(obj, key)) continue;
            val = obj[key];
            out[key] = val;
          }
        }
      }
      return out;
    };
  
    removeClass = function(el, name) {
      var cls, _i, _len, _ref, _results;
      if (el.classList != null) {
        _ref = name.split(' ');
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          cls = _ref[_i];
          if (cls.trim()) {
            _results.push(el.classList.remove(cls));
          }
        }
        return _results;
      } else {
        return el.className = el.className.replace(new RegExp("(^| )" + (name.split(' ').join('|')) + "( |$)", 'gi'), ' ');
      }
    };
  
    addClass = function(el, name) {
      var cls, _i, _len, _ref, _results;
      if (el.classList != null) {
        _ref = name.split(' ');
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          cls = _ref[_i];
          if (cls.trim()) {
            _results.push(el.classList.add(cls));
          }
        }
        return _results;
      } else {
        removeClass(el, name);
        return el.className += " " + name;
      }
    };
  
    hasClass = function(el, name) {
      if (el.classList != null) {
        return el.classList.contains(name);
      } else {
        return new RegExp("(^| )" + name + "( |$)", 'gi').test(el.className);
      }
    };
  
    updateClasses = function(el, add, all) {
      var cls, _i, _j, _len, _len1, _results;
      for (_i = 0, _len = all.length; _i < _len; _i++) {
        cls = all[_i];
        if (__indexOf.call(add, cls) < 0) {
          if (hasClass(el, cls)) {
            removeClass(el, cls);
          }
        }
      }
      _results = [];
      for (_j = 0, _len1 = add.length; _j < _len1; _j++) {
        cls = add[_j];
        if (!hasClass(el, cls)) {
          _results.push(addClass(el, cls));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
  
    deferred = [];
  
    defer = function(fn) {
      return deferred.push(fn);
    };
  
    flush = function() {
      var fn, _results;
      _results = [];
      while (fn = deferred.pop()) {
        _results.push(fn());
      }
      return _results;
    };
  
    Evented = (function() {
      function Evented() {}
  
      Evented.prototype.on = function(event, handler, ctx, once) {
        var _base;
        if (once == null) {
          once = false;
        }
        if (this.bindings == null) {
          this.bindings = {};
        }
        if ((_base = this.bindings)[event] == null) {
          _base[event] = [];
        }
        return this.bindings[event].push({
          handler: handler,
          ctx: ctx,
          once: once
        });
      };
  
      Evented.prototype.once = function(event, handler, ctx) {
        return this.on(event, handler, ctx, true);
      };
  
      Evented.prototype.off = function(event, handler) {
        var i, _ref, _results;
        if (((_ref = this.bindings) != null ? _ref[event] : void 0) == null) {
          return;
        }
        if (handler == null) {
          return delete this.bindings[event];
        } else {
          i = 0;
          _results = [];
          while (i < this.bindings[event].length) {
            if (this.bindings[event][i].handler === handler) {
              _results.push(this.bindings[event].splice(i, 1));
            } else {
              _results.push(i++);
            }
          }
          return _results;
        }
      };
  
      Evented.prototype.trigger = function() {
        var args, ctx, event, handler, i, once, _ref, _ref1, _results;
        event = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
        if ((_ref = this.bindings) != null ? _ref[event] : void 0) {
          i = 0;
          _results = [];
          while (i < this.bindings[event].length) {
            _ref1 = this.bindings[event][i], handler = _ref1.handler, ctx = _ref1.ctx, once = _ref1.once;
            handler.apply(ctx != null ? ctx : this, args);
            if (once) {
              _results.push(this.bindings[event].splice(i, 1));
            } else {
              _results.push(i++);
            }
          }
          return _results;
        }
      };
  
      return Evented;
  
    })();
  
    this.Tether.Utils = {
      getScrollParent: getScrollParent,
      getBounds: getBounds,
      getOffsetParent: getOffsetParent,
      extend: extend,
      addClass: addClass,
      removeClass: removeClass,
      hasClass: hasClass,
      updateClasses: updateClasses,
      defer: defer,
      flush: flush,
      uniqueId: uniqueId,
      Evented: Evented,
      getScrollBarSize: getScrollBarSize
    };
  
  }).call(this);
  
  (function() {
    var MIRROR_LR, MIRROR_TB, OFFSET_MAP, Tether, addClass, addOffset, attachmentToOffset, autoToFixedAttachment, defer, extend, flush, getBounds, getOffsetParent, getOuterSize, getScrollBarSize, getScrollParent, getSize, now, offsetToPx, parseAttachment, parseOffset, position, removeClass, tethers, transformKey, updateClasses, within, _Tether, _ref,
      __slice = [].slice,
      __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
  
    if (this.Tether == null) {
      throw new Error("You must include the utils.js file before tether.js");
    }
  
    Tether = this.Tether;
  
    _ref = Tether.Utils, getScrollParent = _ref.getScrollParent, getSize = _ref.getSize, getOuterSize = _ref.getOuterSize, getBounds = _ref.getBounds, getOffsetParent = _ref.getOffsetParent, extend = _ref.extend, addClass = _ref.addClass, removeClass = _ref.removeClass, updateClasses = _ref.updateClasses, defer = _ref.defer, flush = _ref.flush, getScrollBarSize = _ref.getScrollBarSize;
  
    within = function(a, b, diff) {
      if (diff == null) {
        diff = 1;
      }
      return (a + diff >= b && b >= a - diff);
    };
  
    transformKey = (function() {
      var el, key, _i, _len, _ref1;
      el = document.createElement('div');
      _ref1 = ['transform', 'webkitTransform', 'OTransform', 'MozTransform', 'msTransform'];
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        key = _ref1[_i];
        if (el.style[key] !== void 0) {
          return key;
        }
      }
    })();
  
    tethers = [];
  
    position = function() {
      var tether, _i, _len;
      for (_i = 0, _len = tethers.length; _i < _len; _i++) {
        tether = tethers[_i];
        tether.position(false);
      }
      return flush();
    };
  
    now = function() {
      var _ref1;
      return (_ref1 = typeof performance !== "undefined" && performance !== null ? typeof performance.now === "function" ? performance.now() : void 0 : void 0) != null ? _ref1 : +(new Date);
    };
  
    (function() {
      var event, lastCall, lastDuration, pendingTimeout, tick, _i, _len, _ref1, _results;
      lastCall = null;
      lastDuration = null;
      pendingTimeout = null;
      tick = function() {
        if ((lastDuration != null) && lastDuration > 16) {
          lastDuration = Math.min(lastDuration - 16, 250);
          pendingTimeout = setTimeout(tick, 250);
          return;
        }
        if ((lastCall != null) && (now() - lastCall) < 10) {
          return;
        }
        if (pendingTimeout != null) {
          clearTimeout(pendingTimeout);
          pendingTimeout = null;
        }
        lastCall = now();
        position();
        return lastDuration = now() - lastCall;
      };
      _ref1 = ['resize', 'scroll', 'touchmove'];
      _results = [];
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        event = _ref1[_i];
        _results.push(window.addEventListener(event, tick));
      }
      return _results;
    })();
  
    MIRROR_LR = {
      center: 'center',
      left: 'right',
      right: 'left'
    };
  
    MIRROR_TB = {
      middle: 'middle',
      top: 'bottom',
      bottom: 'top'
    };
  
    OFFSET_MAP = {
      top: 0,
      left: 0,
      middle: '50%',
      center: '50%',
      bottom: '100%',
      right: '100%'
    };
  
    autoToFixedAttachment = function(attachment, relativeToAttachment) {
      var left, top;
      left = attachment.left, top = attachment.top;
      if (left === 'auto') {
        left = MIRROR_LR[relativeToAttachment.left];
      }
      if (top === 'auto') {
        top = MIRROR_TB[relativeToAttachment.top];
      }
      return {
        left: left,
        top: top
      };
    };
  
    attachmentToOffset = function(attachment) {
      var _ref1, _ref2;
      return {
        left: (_ref1 = OFFSET_MAP[attachment.left]) != null ? _ref1 : attachment.left,
        top: (_ref2 = OFFSET_MAP[attachment.top]) != null ? _ref2 : attachment.top
      };
    };
  
    addOffset = function() {
      var left, offsets, out, top, _i, _len, _ref1;
      offsets = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      out = {
        top: 0,
        left: 0
      };
      for (_i = 0, _len = offsets.length; _i < _len; _i++) {
        _ref1 = offsets[_i], top = _ref1.top, left = _ref1.left;
        if (typeof top === 'string') {
          top = parseFloat(top, 10);
        }
        if (typeof left === 'string') {
          left = parseFloat(left, 10);
        }
        out.top += top;
        out.left += left;
      }
      return out;
    };
  
    offsetToPx = function(offset, size) {
      if (typeof offset.left === 'string' && offset.left.indexOf('%') !== -1) {
        offset.left = parseFloat(offset.left, 10) / 100 * size.width;
      }
      if (typeof offset.top === 'string' && offset.top.indexOf('%') !== -1) {
        offset.top = parseFloat(offset.top, 10) / 100 * size.height;
      }
      return offset;
    };
  
    parseAttachment = parseOffset = function(value) {
      var left, top, _ref1;
      _ref1 = value.split(' '), top = _ref1[0], left = _ref1[1];
      return {
        top: top,
        left: left
      };
    };
  
    _Tether = (function() {
      _Tether.modules = [];
  
      function _Tether(options) {
        this.position = __bind(this.position, this);
        var module, _i, _len, _ref1, _ref2;
        tethers.push(this);
        this.history = [];
        this.setOptions(options, false);
        _ref1 = Tether.modules;
        for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
          module = _ref1[_i];
          if ((_ref2 = module.initialize) != null) {
            _ref2.call(this);
          }
        }
        this.position();
      }
  
      _Tether.prototype.getClass = function(key) {
        var _ref1, _ref2;
        if ((_ref1 = this.options.classes) != null ? _ref1[key] : void 0) {
          return this.options.classes[key];
        } else if (((_ref2 = this.options.classes) != null ? _ref2[key] : void 0) !== false) {
          if (this.options.classPrefix) {
            return "" + this.options.classPrefix + "-" + key;
          } else {
            return key;
          }
        } else {
          return '';
        }
      };
  
      _Tether.prototype.setOptions = function(options, position) {
        var defaults, key, _i, _len, _ref1, _ref2;
        this.options = options;
        if (position == null) {
          position = true;
        }
        defaults = {
          offset: '0 0',
          targetOffset: '0 0',
          targetAttachment: 'auto auto',
          classPrefix: 'tether'
        };
        this.options = extend(defaults, this.options);
        _ref1 = this.options, this.element = _ref1.element, this.target = _ref1.target, this.targetModifier = _ref1.targetModifier;
        if (this.target === 'viewport') {
          this.target = document.body;
          this.targetModifier = 'visible';
        } else if (this.target === 'scroll-handle') {
          this.target = document.body;
          this.targetModifier = 'scroll-handle';
        }
        _ref2 = ['element', 'target'];
        for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
          key = _ref2[_i];
          if (this[key] == null) {
            throw new Error("Tether Error: Both element and target must be defined");
          }
          if (this[key].jquery != null) {
            this[key] = this[key][0];
          } else if (typeof this[key] === 'string') {
            this[key] = document.querySelector(this[key]);
          }
        }
        addClass(this.element, this.getClass('element'));
        addClass(this.target, this.getClass('target'));
        if (!this.options.attachment) {
          throw new Error("Tether Error: You must provide an attachment");
        }
        this.targetAttachment = parseAttachment(this.options.targetAttachment);
        this.attachment = parseAttachment(this.options.attachment);
        this.offset = parseOffset(this.options.offset);
        this.targetOffset = parseOffset(this.options.targetOffset);
        if (this.scrollParent != null) {
          this.disable();
        }
        if (this.targetModifier === 'scroll-handle') {
          this.scrollParent = this.target;
        } else {
          this.scrollParent = getScrollParent(this.target);
        }
        if (this.options.enabled !== false) {
          return this.enable(position);
        }
      };
  
      _Tether.prototype.getTargetBounds = function() {
        var bounds, fitAdj, hasBottomScroll, height, out, scrollBottom, scrollPercentage, style, target;
        if (this.targetModifier != null) {
          switch (this.targetModifier) {
            case 'visible':
              if (this.target === document.body) {
                return {
                  top: pageYOffset,
                  left: pageXOffset,
                  height: innerHeight,
                  width: innerWidth
                };
              } else {
                bounds = getBounds(this.target);
                out = {
                  height: bounds.height,
                  width: bounds.width,
                  top: bounds.top,
                  left: bounds.left
                };
                out.height = Math.min(out.height, bounds.height - (pageYOffset - bounds.top));
                out.height = Math.min(out.height, bounds.height - ((bounds.top + bounds.height) - (pageYOffset + innerHeight)));
                out.height = Math.min(innerHeight, out.height);
                out.height -= 2;
                out.width = Math.min(out.width, bounds.width - (pageXOffset - bounds.left));
                out.width = Math.min(out.width, bounds.width - ((bounds.left + bounds.width) - (pageXOffset + innerWidth)));
                out.width = Math.min(innerWidth, out.width);
                out.width -= 2;
                if (out.top < pageYOffset) {
                  out.top = pageYOffset;
                }
                if (out.left < pageXOffset) {
                  out.left = pageXOffset;
                }
                return out;
              }
              break;
            case 'scroll-handle':
              target = this.target;
              if (target === document.body) {
                target = document.documentElement;
                bounds = {
                  left: pageXOffset,
                  top: pageYOffset,
                  height: innerHeight,
                  width: innerWidth
                };
              } else {
                bounds = getBounds(target);
              }
              style = getComputedStyle(target);
              hasBottomScroll = target.scrollWidth > target.clientWidth || 'scroll' === [style.overflow, style.overflowX] || this.target !== document.body;
              scrollBottom = 0;
              if (hasBottomScroll) {
                scrollBottom = 15;
              }
              height = bounds.height - parseFloat(style.borderTopWidth) - parseFloat(style.borderBottomWidth) - scrollBottom;
              out = {
                width: 15,
                height: height * 0.975 * (height / target.scrollHeight),
                left: bounds.left + bounds.width - parseFloat(style.borderLeftWidth) - 15
              };
              fitAdj = 0;
              if (height < 408 && this.target === document.body) {
                fitAdj = -0.00011 * Math.pow(height, 2) - 0.00727 * height + 22.58;
              }
              if (this.target !== document.body) {
                out.height = Math.max(out.height, 24);
              }
              scrollPercentage = this.target.scrollTop / (target.scrollHeight - height);
              out.top = scrollPercentage * (height - out.height - fitAdj) + bounds.top + parseFloat(style.borderTopWidth);
              if (this.target === document.body) {
                out.height = Math.max(out.height, 24);
              }
              return out;
          }
        } else {
          return getBounds(this.target);
        }
      };
  
      _Tether.prototype.clearCache = function() {
        return this._cache = {};
      };
  
      _Tether.prototype.cache = function(k, getter) {
        if (this._cache == null) {
          this._cache = {};
        }
        if (this._cache[k] == null) {
          this._cache[k] = getter.call(this);
        }
        return this._cache[k];
      };
  
      _Tether.prototype.enable = function(position) {
        if (position == null) {
          position = true;
        }
        addClass(this.target, this.getClass('enabled'));
        addClass(this.element, this.getClass('enabled'));
        this.enabled = true;
        if (this.scrollParent !== document) {
          this.scrollParent.addEventListener('scroll', this.position);
        }
        if (position) {
          return this.position();
        }
      };
  
      _Tether.prototype.disable = function() {
        removeClass(this.target, this.getClass('enabled'));
        removeClass(this.element, this.getClass('enabled'));
        this.enabled = false;
        if (this.scrollParent != null) {
          return this.scrollParent.removeEventListener('scroll', this.position);
        }
      };
  
      _Tether.prototype.destroy = function() {
        var i, tether, _i, _len, _results;
        this.disable();
        _results = [];
        for (i = _i = 0, _len = tethers.length; _i < _len; i = ++_i) {
          tether = tethers[i];
          if (tether === this) {
            tethers.splice(i, 1);
            break;
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };
  
      _Tether.prototype.updateAttachClasses = function(elementAttach, targetAttach) {
        var add, all, side, sides, _i, _j, _len, _len1, _ref1,
          _this = this;
        if (elementAttach == null) {
          elementAttach = this.attachment;
        }
        if (targetAttach == null) {
          targetAttach = this.targetAttachment;
        }
        sides = ['left', 'top', 'bottom', 'right', 'middle', 'center'];
        if ((_ref1 = this._addAttachClasses) != null ? _ref1.length : void 0) {
          this._addAttachClasses.splice(0, this._addAttachClasses.length);
        }
        add = this._addAttachClasses != null ? this._addAttachClasses : this._addAttachClasses = [];
        if (elementAttach.top) {
          add.push("" + (this.getClass('element-attached')) + "-" + elementAttach.top);
        }
        if (elementAttach.left) {
          add.push("" + (this.getClass('element-attached')) + "-" + elementAttach.left);
        }
        if (targetAttach.top) {
          add.push("" + (this.getClass('target-attached')) + "-" + targetAttach.top);
        }
        if (targetAttach.left) {
          add.push("" + (this.getClass('target-attached')) + "-" + targetAttach.left);
        }
        all = [];
        for (_i = 0, _len = sides.length; _i < _len; _i++) {
          side = sides[_i];
          all.push("" + (this.getClass('element-attached')) + "-" + side);
        }
        for (_j = 0, _len1 = sides.length; _j < _len1; _j++) {
          side = sides[_j];
          all.push("" + (this.getClass('target-attached')) + "-" + side);
        }
        return defer(function() {
          if (_this._addAttachClasses == null) {
            return;
          }
          updateClasses(_this.element, _this._addAttachClasses, all);
          updateClasses(_this.target, _this._addAttachClasses, all);
          return _this._addAttachClasses = void 0;
        });
      };
  
      _Tether.prototype.position = function(flushChanges) {
        var elementPos, elementStyle, height, left, manualOffset, manualTargetOffset, module, next, offset, offsetBorder, offsetParent, offsetParentSize, offsetParentStyle, offsetPosition, ret, scrollLeft, scrollTop, scrollbarSize, side, targetAttachment, targetOffset, targetPos, targetSize, top, width, _i, _j, _len, _len1, _ref1, _ref2, _ref3, _ref4, _ref5, _ref6,
          _this = this;
        if (flushChanges == null) {
          flushChanges = true;
        }
        if (!this.enabled) {
          return;
        }
        this.clearCache();
        targetAttachment = autoToFixedAttachment(this.targetAttachment, this.attachment);
        this.updateAttachClasses(this.attachment, targetAttachment);
        elementPos = this.cache('element-bounds', function() {
          return getBounds(_this.element);
        });
        width = elementPos.width, height = elementPos.height;
        if (width === 0 && height === 0 && (this.lastSize != null)) {
          _ref1 = this.lastSize, width = _ref1.width, height = _ref1.height;
        } else {
          this.lastSize = {
            width: width,
            height: height
          };
        }
        targetSize = targetPos = this.cache('target-bounds', function() {
          return _this.getTargetBounds();
        });
        offset = offsetToPx(attachmentToOffset(this.attachment), {
          width: width,
          height: height
        });
        targetOffset = offsetToPx(attachmentToOffset(targetAttachment), targetSize);
        manualOffset = offsetToPx(this.offset, {
          width: width,
          height: height
        });
        manualTargetOffset = offsetToPx(this.targetOffset, targetSize);
        offset = addOffset(offset, manualOffset);
        targetOffset = addOffset(targetOffset, manualTargetOffset);
        left = targetPos.left + targetOffset.left - offset.left;
        top = targetPos.top + targetOffset.top - offset.top;
        _ref2 = Tether.modules;
        for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
          module = _ref2[_i];
          ret = module.position.call(this, {
            left: left,
            top: top,
            targetAttachment: targetAttachment,
            targetPos: targetPos,
            attachment: this.attachment,
            elementPos: elementPos,
            offset: offset,
            targetOffset: targetOffset,
            manualOffset: manualOffset,
            manualTargetOffset: manualTargetOffset,
            scrollbarSize: scrollbarSize
          });
          if ((ret == null) || typeof ret !== 'object') {
            continue;
          } else if (ret === false) {
            return false;
          } else {
            top = ret.top, left = ret.left;
          }
        }
        next = {
          page: {
            top: top,
            left: left
          },
          viewport: {
            top: top - pageYOffset,
            bottom: pageYOffset - top - height + innerHeight,
            left: left - pageXOffset,
            right: pageXOffset - left - width + innerWidth
          }
        };
        if (document.body.scrollWidth > window.innerWidth) {
          scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
          next.viewport.bottom -= scrollbarSize.height;
        }
        if (document.body.scrollHeight > window.innerHeight) {
          scrollbarSize = this.cache('scrollbar-size', getScrollBarSize);
          next.viewport.right -= scrollbarSize.width;
        }
        if (((_ref3 = document.body.style.position) !== '' && _ref3 !== 'static') || ((_ref4 = document.body.parentElement.style.position) !== '' && _ref4 !== 'static')) {
          next.page.bottom = document.body.scrollHeight - top - height;
          next.page.right = document.body.scrollWidth - left - width;
        }
        if (((_ref5 = this.options.optimizations) != null ? _ref5.moveElement : void 0) !== false && (this.targetModifier == null)) {
          offsetParent = this.cache('target-offsetparent', function() {
            return getOffsetParent(_this.target);
          });
          offsetPosition = this.cache('target-offsetparent-bounds', function() {
            return getBounds(offsetParent);
          });
          offsetParentStyle = getComputedStyle(offsetParent);
          elementStyle = getComputedStyle(this.element);
          offsetParentSize = offsetPosition;
          offsetBorder = {};
          _ref6 = ['Top', 'Left', 'Bottom', 'Right'];
          for (_j = 0, _len1 = _ref6.length; _j < _len1; _j++) {
            side = _ref6[_j];
            offsetBorder[side.toLowerCase()] = parseFloat(offsetParentStyle["border" + side + "Width"]);
          }
          offsetPosition.right = document.body.scrollWidth - offsetPosition.left - offsetParentSize.width + offsetBorder.right;
          offsetPosition.bottom = document.body.scrollHeight - offsetPosition.top - offsetParentSize.height + offsetBorder.bottom;
          if (next.page.top >= (offsetPosition.top + offsetBorder.top) && next.page.bottom >= offsetPosition.bottom) {
            if (next.page.left >= (offsetPosition.left + offsetBorder.left) && next.page.right >= offsetPosition.right) {
              scrollTop = offsetParent.scrollTop;
              scrollLeft = offsetParent.scrollLeft;
              next.offset = {
                top: next.page.top - offsetPosition.top + scrollTop - offsetBorder.top,
                left: next.page.left - offsetPosition.left + scrollLeft - offsetBorder.left
              };
            }
          }
        }
        this.move(next);
        this.history.unshift(next);
        if (this.history.length > 3) {
          this.history.pop();
        }
        if (flushChanges) {
          flush();
        }
        return true;
      };
  
      _Tether.prototype.move = function(position) {
        var css, elVal, found, key, moved, offsetParent, point, same, transcribe, type, val, write, writeCSS, _i, _len, _ref1, _ref2,
          _this = this;
        if (this.element.parentNode == null) {
          return;
        }
        same = {};
        for (type in position) {
          same[type] = {};
          for (key in position[type]) {
            found = false;
            _ref1 = this.history;
            for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
              point = _ref1[_i];
              if (!within((_ref2 = point[type]) != null ? _ref2[key] : void 0, position[type][key])) {
                found = true;
                break;
              }
            }
            if (!found) {
              same[type][key] = true;
            }
          }
        }
        css = {
          top: '',
          left: '',
          right: '',
          bottom: ''
        };
        transcribe = function(same, pos) {
          var xPos, yPos, _ref3;
          if (((_ref3 = _this.options.optimizations) != null ? _ref3.gpu : void 0) !== false) {
            if (same.top) {
              css.top = 0;
              yPos = pos.top;
            } else {
              css.bottom = 0;
              yPos = -pos.bottom;
            }
            if (same.left) {
              css.left = 0;
              xPos = pos.left;
            } else {
              css.right = 0;
              xPos = -pos.right;
            }
            css[transformKey] = "translateX(" + (Math.round(xPos)) + "px) translateY(" + (Math.round(yPos)) + "px)";
            if (transformKey !== 'msTransform') {
              return css[transformKey] += " translateZ(0)";
            }
          } else {
            if (same.top) {
              css.top = "" + pos.top + "px";
            } else {
              css.bottom = "" + pos.bottom + "px";
            }
            if (same.left) {
              return css.left = "" + pos.left + "px";
            } else {
              return css.right = "" + pos.right + "px";
            }
          }
        };
        moved = false;
        if ((same.page.top || same.page.bottom) && (same.page.left || same.page.right)) {
          css.position = 'absolute';
          transcribe(same.page, position.page);
        } else if ((same.viewport.top || same.viewport.bottom) && (same.viewport.left || same.viewport.right)) {
          css.position = 'fixed';
          transcribe(same.viewport, position.viewport);
        } else if ((same.offset != null) && same.offset.top && same.offset.left) {
          css.position = 'absolute';
          offsetParent = this.cache('target-offsetparent', function() {
            return getOffsetParent(_this.target);
          });
          if (getOffsetParent(this.element) !== offsetParent) {
            defer(function() {
              _this.element.parentNode.removeChild(_this.element);
              return offsetParent.appendChild(_this.element);
            });
          }
          transcribe(same.offset, position.offset);
          moved = true;
        } else {
          css.position = 'absolute';
          transcribe({
            top: true,
            left: true
          }, position.page);
        }
        if (!moved && this.element.parentNode.tagName !== 'BODY') {
          this.element.parentNode.removeChild(this.element);
          document.body.appendChild(this.element);
        }
        writeCSS = {};
        write = false;
        for (key in css) {
          val = css[key];
          elVal = this.element.style[key];
          if (elVal !== '' && val !== '' && (key === 'top' || key === 'left' || key === 'bottom' || key === 'right')) {
            elVal = parseFloat(elVal);
            val = parseFloat(val);
          }
          if (elVal !== val) {
            write = true;
            writeCSS[key] = css[key];
          }
        }
        if (write) {
          return defer(function() {
            return extend(_this.element.style, writeCSS);
          });
        }
      };
  
      return _Tether;
  
    })();
  
    Tether.position = position;
  
    this.Tether = extend(_Tether, Tether);
  
  }).call(this);
  
  (function() {
    var BOUNDS_FORMAT, MIRROR_ATTACH, defer, extend, getBoundingRect, getBounds, getOuterSize, getSize, updateClasses, _ref,
      __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };
  
    _ref = this.Tether.Utils, getOuterSize = _ref.getOuterSize, getBounds = _ref.getBounds, getSize = _ref.getSize, extend = _ref.extend, updateClasses = _ref.updateClasses, defer = _ref.defer;
  
    MIRROR_ATTACH = {
      left: 'right',
      right: 'left',
      top: 'bottom',
      bottom: 'top',
      middle: 'middle'
    };
  
    BOUNDS_FORMAT = ['left', 'top', 'right', 'bottom'];
  
    getBoundingRect = function(tether, to) {
      var i, pos, side, size, style, _i, _len;
      if (to === 'scrollParent') {
        to = tether.scrollParent;
      } else if (to === 'window') {
        to = [pageXOffset, pageYOffset, innerWidth + pageXOffset, innerHeight + pageYOffset];
      }
      if (to === document) {
        to = to.documentElement;
      }
      if (to.nodeType != null) {
        pos = size = getBounds(to);
        style = getComputedStyle(to);
        to = [pos.left, pos.top, size.width + pos.left, size.height + pos.top];
        for (i = _i = 0, _len = BOUNDS_FORMAT.length; _i < _len; i = ++_i) {
          side = BOUNDS_FORMAT[i];
          side = side[0].toUpperCase() + side.substr(1);
          if (side === 'Top' || side === 'Left') {
            to[i] += parseFloat(style["border" + side + "Width"]);
          } else {
            to[i] -= parseFloat(style["border" + side + "Width"]);
          }
        }
      }
      return to;
    };
  
    this.Tether.modules.push({
      position: function(_arg) {
        var addClasses, allClasses, attachment, bounds, changeAttachX, changeAttachY, cls, constraint, eAttachment, height, left, oob, oobClass, p, pin, pinned, pinnedClass, removeClass, side, tAttachment, targetAttachment, targetHeight, targetSize, targetWidth, to, top, width, _i, _j, _k, _l, _len, _len1, _len2, _len3, _len4, _len5, _m, _n, _ref1, _ref2, _ref3, _ref4, _ref5, _ref6, _ref7, _ref8,
          _this = this;
        top = _arg.top, left = _arg.left, targetAttachment = _arg.targetAttachment;
        if (!this.options.constraints) {
          return true;
        }
        removeClass = function(prefix) {
          var side, _i, _len, _results;
          _this.removeClass(prefix);
          _results = [];
          for (_i = 0, _len = BOUNDS_FORMAT.length; _i < _len; _i++) {
            side = BOUNDS_FORMAT[_i];
            _results.push(_this.removeClass("" + prefix + "-" + side));
          }
          return _results;
        };
        _ref1 = this.cache('element-bounds', function() {
          return getBounds(_this.element);
        }), height = _ref1.height, width = _ref1.width;
        if (width === 0 && height === 0 && (this.lastSize != null)) {
          _ref2 = this.lastSize, width = _ref2.width, height = _ref2.height;
        }
        targetSize = this.cache('target-bounds', function() {
          return _this.getTargetBounds();
        });
        targetHeight = targetSize.height;
        targetWidth = targetSize.width;
        tAttachment = {};
        eAttachment = {};
        allClasses = [this.getClass('pinned'), this.getClass('out-of-bounds')];
        _ref3 = this.options.constraints;
        for (_i = 0, _len = _ref3.length; _i < _len; _i++) {
          constraint = _ref3[_i];
          if (constraint.outOfBoundsClass) {
            allClasses.push(constraint.outOfBoundsClass);
          }
          if (constraint.pinnedClass) {
            allClasses.push(constraint.pinnedClass);
          }
        }
        for (_j = 0, _len1 = allClasses.length; _j < _len1; _j++) {
          cls = allClasses[_j];
          _ref4 = ['left', 'top', 'right', 'bottom'];
          for (_k = 0, _len2 = _ref4.length; _k < _len2; _k++) {
            side = _ref4[_k];
            allClasses.push("" + cls + "-" + side);
          }
        }
        addClasses = [];
        tAttachment = extend({}, targetAttachment);
        eAttachment = extend({}, this.attachment);
        _ref5 = this.options.constraints;
        for (_l = 0, _len3 = _ref5.length; _l < _len3; _l++) {
          constraint = _ref5[_l];
          to = constraint.to, attachment = constraint.attachment, pin = constraint.pin;
          if (attachment == null) {
            attachment = '';
          }
          if (__indexOf.call(attachment, ' ') >= 0) {
            _ref6 = attachment.split(' '), changeAttachY = _ref6[0], changeAttachX = _ref6[1];
          } else {
            changeAttachX = changeAttachY = attachment;
          }
          bounds = getBoundingRect(this, to);
          if (changeAttachY === 'target' || changeAttachY === 'both') {
            if (top < bounds[1] && tAttachment.top === 'top') {
              top += targetHeight;
              tAttachment.top = 'bottom';
            }
            if (top + height > bounds[3] && tAttachment.top === 'bottom') {
              top -= targetHeight;
              tAttachment.top = 'top';
            }
          }
          if (changeAttachY === 'together') {
            if (top < bounds[1] && tAttachment.top === 'top') {
              if (eAttachment.top === 'bottom') {
                top += targetHeight;
                tAttachment.top = 'bottom';
                top += height;
                eAttachment.top = 'top';
              } else if (eAttachment.top === 'top') {
                top += targetHeight;
                tAttachment.top = 'bottom';
                top -= height;
                eAttachment.top = 'bottom';
              }
            }
            if (top + height > bounds[3] && tAttachment.top === 'bottom') {
              if (eAttachment.top === 'top') {
                top -= targetHeight;
                tAttachment.top = 'top';
                top -= height;
                eAttachment.top = 'bottom';
              } else if (eAttachment.top === 'bottom') {
                top -= targetHeight;
                tAttachment.top = 'top';
                top += height;
                eAttachment.top = 'top';
              }
            }
            if (tAttachment.top === 'middle') {
              if (top + height > bounds[3] && eAttachment.top === 'top') {
                top -= height;
                eAttachment.top = 'bottom';
              } else if (top < bounds[1] && eAttachment.top === 'bottom') {
                top += height;
                eAttachment.top = 'top';
              }
            }
          }
          if (changeAttachX === 'target' || changeAttachX === 'both') {
            if (left < bounds[0] && tAttachment.left === 'left') {
              left += targetWidth;
              tAttachment.left = 'right';
            }
            if (left + width > bounds[2] && tAttachment.left === 'right') {
              left -= targetWidth;
              tAttachment.left = 'left';
            }
          }
          if (changeAttachX === 'together') {
            if (left < bounds[0] && tAttachment.left === 'left') {
              if (eAttachment.left === 'right') {
                left += targetWidth;
                tAttachment.left = 'right';
                left += width;
                eAttachment.left = 'left';
              } else if (eAttachment.left === 'left') {
                left += targetWidth;
                tAttachment.left = 'right';
                left -= width;
                eAttachment.left = 'right';
              }
            } else if (left + width > bounds[2] && tAttachment.left === 'right') {
              if (eAttachment.left === 'left') {
                left -= targetWidth;
                tAttachment.left = 'left';
                left -= width;
                eAttachment.left = 'right';
              } else if (eAttachment.left === 'right') {
                left -= targetWidth;
                tAttachment.left = 'left';
                left += width;
                eAttachment.left = 'left';
              }
            } else if (tAttachment.left === 'center') {
              if (left + width > bounds[2] && eAttachment.left === 'left') {
                left -= width;
                eAttachment.left = 'right';
              } else if (left < bounds[0] && eAttachment.left === 'right') {
                left += width;
                eAttachment.left = 'left';
              }
            }
          }
          if (changeAttachY === 'element' || changeAttachY === 'both') {
            if (top < bounds[1] && eAttachment.top === 'bottom') {
              top += height;
              eAttachment.top = 'top';
            }
            if (top + height > bounds[3] && eAttachment.top === 'top') {
              top -= height;
              eAttachment.top = 'bottom';
            }
          }
          if (changeAttachX === 'element' || changeAttachX === 'both') {
            if (left < bounds[0] && eAttachment.left === 'right') {
              left += width;
              eAttachment.left = 'left';
            }
            if (left + width > bounds[2] && eAttachment.left === 'left') {
              left -= width;
              eAttachment.left = 'right';
            }
          }
          if (typeof pin === 'string') {
            pin = (function() {
              var _len4, _m, _ref7, _results;
              _ref7 = pin.split(',');
              _results = [];
              for (_m = 0, _len4 = _ref7.length; _m < _len4; _m++) {
                p = _ref7[_m];
                _results.push(p.trim());
              }
              return _results;
            })();
          } else if (pin === true) {
            pin = ['top', 'left', 'right', 'bottom'];
          }
          pin || (pin = []);
          pinned = [];
          oob = [];
          if (top < bounds[1]) {
            if (__indexOf.call(pin, 'top') >= 0) {
              top = bounds[1];
              pinned.push('top');
            } else {
              oob.push('top');
            }
          }
          if (top + height > bounds[3]) {
            if (__indexOf.call(pin, 'bottom') >= 0) {
              top = bounds[3] - height;
              pinned.push('bottom');
            } else {
              oob.push('bottom');
            }
          }
          if (left < bounds[0]) {
            if (__indexOf.call(pin, 'left') >= 0) {
              left = bounds[0];
              pinned.push('left');
            } else {
              oob.push('left');
            }
          }
          if (left + width > bounds[2]) {
            if (__indexOf.call(pin, 'right') >= 0) {
              left = bounds[2] - width;
              pinned.push('right');
            } else {
              oob.push('right');
            }
          }
          if (pinned.length) {
            pinnedClass = (_ref7 = this.options.pinnedClass) != null ? _ref7 : this.getClass('pinned');
            addClasses.push(pinnedClass);
            for (_m = 0, _len4 = pinned.length; _m < _len4; _m++) {
              side = pinned[_m];
              addClasses.push("" + pinnedClass + "-" + side);
            }
          }
          if (oob.length) {
            oobClass = (_ref8 = this.options.outOfBoundsClass) != null ? _ref8 : this.getClass('out-of-bounds');
            addClasses.push(oobClass);
            for (_n = 0, _len5 = oob.length; _n < _len5; _n++) {
              side = oob[_n];
              addClasses.push("" + oobClass + "-" + side);
            }
          }
          if (__indexOf.call(pinned, 'left') >= 0 || __indexOf.call(pinned, 'right') >= 0) {
            eAttachment.left = tAttachment.left = false;
          }
          if (__indexOf.call(pinned, 'top') >= 0 || __indexOf.call(pinned, 'bottom') >= 0) {
            eAttachment.top = tAttachment.top = false;
          }
          if (tAttachment.top !== targetAttachment.top || tAttachment.left !== targetAttachment.left || eAttachment.top !== this.attachment.top || eAttachment.left !== this.attachment.left) {
            this.updateAttachClasses(eAttachment, tAttachment);
          }
        }
        defer(function() {
          updateClasses(_this.target, addClasses, allClasses);
          return updateClasses(_this.element, addClasses, allClasses);
        });
        return {
          top: top,
          left: left
        };
      }
    });
  
  }).call(this);
  
  (function() {
    var defer, getBounds, updateClasses, _ref;
  
    _ref = this.Tether.Utils, getBounds = _ref.getBounds, updateClasses = _ref.updateClasses, defer = _ref.defer;
  
    this.Tether.modules.push({
      position: function(_arg) {
        var abutted, addClasses, allClasses, bottom, height, left, right, side, sides, targetPos, top, width, _i, _j, _k, _l, _len, _len1, _len2, _len3, _ref1, _ref2, _ref3, _ref4, _ref5,
          _this = this;
        top = _arg.top, left = _arg.left;
        _ref1 = this.cache('element-bounds', function() {
          return getBounds(_this.element);
        }), height = _ref1.height, width = _ref1.width;
        targetPos = this.getTargetBounds();
        bottom = top + height;
        right = left + width;
        abutted = [];
        if (top <= targetPos.bottom && bottom >= targetPos.top) {
          _ref2 = ['left', 'right'];
          for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
            side = _ref2[_i];
            if ((_ref3 = targetPos[side]) === left || _ref3 === right) {
              abutted.push(side);
            }
          }
        }
        if (left <= targetPos.right && right >= targetPos.left) {
          _ref4 = ['top', 'bottom'];
          for (_j = 0, _len1 = _ref4.length; _j < _len1; _j++) {
            side = _ref4[_j];
            if ((_ref5 = targetPos[side]) === top || _ref5 === bottom) {
              abutted.push(side);
            }
          }
        }
        allClasses = [];
        addClasses = [];
        sides = ['left', 'top', 'right', 'bottom'];
        allClasses.push(this.getClass('abutted'));
        for (_k = 0, _len2 = sides.length; _k < _len2; _k++) {
          side = sides[_k];
          allClasses.push("" + (this.getClass('abutted')) + "-" + side);
        }
        if (abutted.length) {
          addClasses.push(this.getClass('abutted'));
        }
        for (_l = 0, _len3 = abutted.length; _l < _len3; _l++) {
          side = abutted[_l];
          addClasses.push("" + (this.getClass('abutted')) + "-" + side);
        }
        defer(function() {
          updateClasses(_this.target, addClasses, allClasses);
          return updateClasses(_this.element, addClasses, allClasses);
        });
        return true;
      }
    });
  
  }).call(this);
  
  (function() {
    this.Tether.modules.push({
      position: function(_arg) {
        var left, result, shift, shiftLeft, shiftTop, top, _ref;
        top = _arg.top, left = _arg.left;
        if (!this.options.shift) {
          return;
        }
        result = function(val) {
          if (typeof val === 'function') {
            return val.call(this, {
              top: top,
              left: left
            });
          } else {
            return val;
          }
        };
        shift = result(this.options.shift);
        if (typeof shift === 'string') {
          shift = shift.split(' ');
          shift[1] || (shift[1] = shift[0]);
          shiftTop = shift[0], shiftLeft = shift[1];
          shiftTop = parseFloat(shiftTop, 10);
          shiftLeft = parseFloat(shiftLeft, 10);
        } else {
          _ref = [shift.top, shift.left], shiftTop = _ref[0], shiftLeft = _ref[1];
        }
        top += shiftTop;
        left += shiftLeft;
        return {
          top: top,
          left: left
        };
      }
    });
  
  }).call(this);
  
  
  	root.Tether = this.Tether;
  
  	if (typeof define === 'function') {
  		define([],function() {
  			return root.Tether;
  		});
  	} else if (typeof exports === 'object') {
  		module.exports = root.Tether;
  	}
  }(this));
  
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/alignment.js
(typeof window === 'undefined' ? global : window).__adf85a946ebc5f1648859d4c820fe0a2 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
  
  var _tether = __2c6af606fcf6072e609bc1ad585d898a;
  
  var _tether2 = _interopRequireDefault(_tether);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var ATTR_ALIGNMENT = 'alignment';
  var ATTR_ALIGNMENT_STATIC = 'alignment-static';
  var ATTR_CONTAINER = 'alignment-container';
  var CLASS_PREFIX_ALIGNMENT = 'aui-alignment';
  var CLASS_PREFIX_SIDE = 'aui-alignment-side-';
  var CLASS_PREFIX_SNAP = 'aui-alignment-snap-';
  var DEFAULT_ATTACHMENT = 'right middle';
  var attachmentMap = {
      'top left': { el: 'bottom left', target: 'top left' },
      'top center': { el: 'bottom center', target: 'top center' },
      'top right': { el: 'bottom right', target: 'top right' },
      'right top': { el: 'top left', target: 'top right' },
      'right middle': { el: 'middle left', target: 'middle right' },
      'right bottom': { el: 'bottom left', target: 'bottom right' },
      'bottom left': { el: 'top left', target: 'bottom left' },
      'bottom center': { el: 'top center', target: 'bottom center' },
      'bottom right': { el: 'top right', target: 'bottom right' },
      'left top': { el: 'top right', target: 'top left' },
      'left middle': { el: 'middle right', target: 'middle left' },
      'left bottom': { el: 'bottom right', target: 'bottom left' },
      'submenu left': { el: 'top left', target: 'top right' },
      'submenu right': { el: 'top right', target: 'top left' }
  };
  
  function hasClass(element, className) {
      return (' ' + element.className + ' ').indexOf(' ' + className + ' ') !== -1;
  }
  
  function addAlignmentClasses(element, side, snap) {
      var sideClass = CLASS_PREFIX_SIDE + side;
      var snapClass = CLASS_PREFIX_SNAP + snap;
  
      if (!hasClass(element, sideClass)) {
          element.className += ' ' + sideClass;
      }
  
      if (!hasClass(element, snapClass)) {
          element.className += ' ' + snapClass;
      }
  }
  
  function getAttribute(element, name) {
      return element.getAttribute(name) || element.getAttribute('data-aui-' + name);
  }
  
  function hasAttribute(element, name) {
      return element.hasAttribute(name) || element.hasAttribute('data-aui-' + name);
  }
  
  function getAlignment(element) {
      var _split = (getAttribute(element, ATTR_ALIGNMENT) || DEFAULT_ATTACHMENT).split(' ');
  
      var _split2 = _slicedToArray(_split, 2);
  
      var side = _split2[0];
      var snap = _split2[1];
  
      return {
          side: side,
          snap: snap
      };
  }
  
  function getContainer(element) {
      var container = getAttribute(element, ATTR_CONTAINER) || window;
  
      if (typeof container === 'string') {
          container = document.querySelector(container);
      }
  
      return container;
  }
  
  function calculateBestAlignmentSnap(target, container) {
      var snap = 'left';
  
      if (!container || container === window || container === document) {
          container = document.documentElement;
      }
  
      if (container && container.nodeType && container.nodeType === Node.ELEMENT_NODE) {
          var containerBounds = container.getBoundingClientRect();
          var targetBounds = target.getBoundingClientRect();
  
          if (targetBounds.left > containerBounds.right / 2) {
              snap = 'right';
          }
      }
  
      return snap;
  }
  
  function getAttachment(side, snap) {
      return attachmentMap[side + ' ' + snap] || attachmentMap[DEFAULT_ATTACHMENT];
  }
  
  function Alignment(element, target) {
      var container = getContainer(element);
      var alignment = getAlignment(element);
  
      if (!alignment.snap || alignment.snap === 'auto') {
          alignment.snap = calculateBestAlignmentSnap(target, container);
      }
  
      var attachment = getAttachment(alignment.side, alignment.snap);
      var isStaticallyAligned = hasAttribute(element, ATTR_ALIGNMENT_STATIC);
      var tether = new _tether2.default({
          enabled: false,
          element: element,
          target: target,
          attachment: attachment.el,
          targetAttachment: attachment.target,
          classPrefix: CLASS_PREFIX_ALIGNMENT,
          constraints: [{
              // Try and keep the element on page
              to: container === window ? 'window' : container,
              attachment: isStaticallyAligned === true ? 'none' : 'together'
          }]
      });
  
      addAlignmentClasses(element, alignment.side, alignment.snap);
  
      this._auiTether = tether;
  }
  
  Alignment.prototype = {
      /**
       * Stops aligning and cleans up.
       *
       * @returns {Alignment}
       */
      destroy: function destroy() {
          this._auiTether.destroy();
          return this;
      },
  
      /**
       * Disables alignment.
       *
       * @returns {Alignment}
       */
      disable: function disable() {
          this._auiTether.disable();
          return this;
      },
  
      /**
       * Enables alignment.
       *
       * @returns {Alignment}
       */
      enable: function enable() {
          this._auiTether.enable();
          return this;
      }
  };
  
  exports.default = Alignment;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/polyfills/custom-event.js
(typeof window === 'undefined' ? global : window).__97354d72dc34929750a9514fb0aad090 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  var CustomEvent = void 0;
  
  (function () {
      if (window.CustomEvent) {
          // Some browsers don't support constructable custom events yet.
          try {
              var ce = new window.CustomEvent('name', {
                  bubbles: false,
                  cancelable: true,
                  detail: {
                      x: 'y'
                  }
              });
              ce.preventDefault();
              if (ce.defaultPrevented !== true) {
                  throw new Error('Could not prevent default');
              }
              if (ce.type !== 'name') {
                  throw new Error('Could not set custom name');
              }
              if (ce.detail.x !== 'y') {
                  throw new Error('Could not set detail');
              }
  
              CustomEvent = window.CustomEvent;
              return;
          } catch (e) {
              // polyfill it
          }
      }
  
      /**
       * @type CustomEvent
       * @param {String} event - the name of the event.
       * @param {Object} [params] - optional configuration of the custom event.
       * @param {Boolean} [params.cancelable=false] - A boolean indicating whether the event is cancelable (i.e., can call preventDefault and set the defaultPrevented property).
       * @param {Boolean} [params.bubbles=false] - A boolean indicating whether the event bubbles up through the DOM or not.
       * @param {Boolean} [params.detail] - The data passed when initializing the event.
       * @extends Event
       * @returns {Event}
       * @constructor
       */
      CustomEvent = function CustomEvent(event, params) {
          params = params || { bubbles: false, cancelable: false, detail: undefined };
  
          var evt = document.createEvent('CustomEvent');
  
          evt.initCustomEvent(event, !!params.bubbles, !!params.cancelable, params.detail);
          var origPrevent = evt.preventDefault;
          evt.preventDefault = function () {
              origPrevent.call(this);
              try {
                  Object.defineProperty(this, 'defaultPrevented', {
                      get: function get() {
                          return true;
                      }
                  });
              } catch (e) {
                  this.defaultPrevented = true;
              }
          };
  
          return evt;
      };
  
      CustomEvent.prototype = window.Event.prototype;
  })();
  
  exports.default = CustomEvent;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/key-code.js
(typeof window === 'undefined' ? global : window).__5e00bdc0656ab7ab1098c8394e0010a9 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var keyCode = {
      ALT: 18,
      BACKSPACE: 8,
      CAPS_LOCK: 20,
      COMMA: 188,
      COMMAND: 91,
  
      // cmd
      COMMAND_LEFT: 91,
      COMMAND_RIGHT: 93,
      LEFT_SQUARE_BRACKET: 91, //This is 91 for keypress and 219 for keydown/keyup
      CONTROL: 17,
      DELETE: 46,
      DOWN: 40,
      END: 35,
      ENTER: 13,
      ESCAPE: 27,
      HOME: 36,
      INSERT: 45,
      LEFT: 37,
  
      // right cmd
      MENU: 93,
      NUMPAD_ADD: 107,
      NUMPAD_DECIMAL: 110,
      NUMPAD_DIVIDE: 111,
      NUMPAD_ENTER: 108,
      NUMPAD_MULTIPLY: 106,
      NUMPAD_SUBTRACT: 109,
      PAGE_DOWN: 34,
      PAGE_UP: 33,
      PERIOD: 190,
      RIGHT: 39,
      SHIFT: 16,
      SPACE: 32,
      TAB: 9,
      UP: 38,
  
      // cmd
      WINDOWS: 91
  };
  
  (0, _globalize2.default)('keyCode', keyCode);
  
  exports.default = keyCode;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/animation.js
(typeof window === 'undefined' ? global : window).__35d26072938719079cba2d8d4f73391d = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  /**
   * Force a re-compute of the style of an element.
   *
   * This is useful for CSS transitions and animations that need computed style changes to occur.
   * CSS transitions will fire when the computed value of the property they are transitioning changes.
   * This may not occur if the style changes get batched into one style change event by the browser.
   * We can force the browser to recognise the two different computed values by calling this function when we want it
   * to recompute the styles.
   *
   * For example, consider a transition on the opacity property.
   *
   * With recomputeStyle:
   * $parent.append($el); //opacity=0
   * recomputeStyle($el);
   * $el.addClass('visible'); //opacity=1
   * //Browser calculates value of opacity=0, and then transitions it to opacity=1
   *
   * Without recomputeStyle:
   * $parent.append($el); //opacity=0
   * $el.addClass('visible'); //opacity=1
   * //Browser calculates value of opacity=1 but no transition
   *
   * @param el The DOM or jQuery element for which style should be recomputed
   */
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  function recomputeStyle(el) {
      el = el.length ? el[0] : el;
      window.getComputedStyle(el, null).getPropertyValue('left');
  }
  
  exports.recomputeStyle = recomputeStyle;
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/blanket.js
(typeof window === 'undefined' ? global : window).__9631e458b05f1a70cf99301b5000456e = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.undim = exports.dim = undefined;
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  var _deprecation = __2d336d5b3a38711b48236d5af8c39b51;
  
  var _animation = __35d26072938719079cba2d8d4f73391d;
  
  var _createElement = __f22d2496fae72b889743e16dda60342b;
  
  var _createElement2 = _interopRequireDefault(_createElement);
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var $overflowEl;
  var _hiddenByAui = [];
  
  /**
   * Dims the screen using a blanket div
   * @param useShim deprecated, it is calculated by dim() now
   */
  function dim(useShim, zIndex) {
  
      //if we're blanketing the page it means we want to hide the whatever is under the blanket from the screen readers as well
      function hasAriaHidden(element) {
          return element.getAttribute('aria-hidden') ? true : false;
      }
  
      function isAuiLayer(element) {
          return element.className.match(/\baui-layer\b/) ? true : false;
      }
  
      _hiddenByAui = [];
      Array.prototype.forEach.call(document.body.children, function (element) {
          if (!hasAriaHidden(element) && !isAuiLayer(element)) {
              element.setAttribute('aria-hidden', 'true');
              _hiddenByAui.push(element);
          }
      });
  
      if (!$overflowEl) {
          $overflowEl = (0, _jquery2.default)(document.body);
      }
  
      if (useShim === true) {
          useShimDeprecationLogger();
      }
  
      var isBlanketShowing = !!dim.$dim && dim.$dim.attr('aria-hidden') === 'false';
  
      if (!!dim.$dim) {
          dim.$dim.remove();
          dim.$dim = null;
      }
  
      dim.$dim = (0, _createElement2.default)('div').addClass('aui-blanket');
      dim.$dim.attr('tabindex', '0'); //required, or the last element's focusout event will go to the browser
      dim.$dim.appendTo(document.body);
  
      if (!isBlanketShowing) {
          //recompute after insertion and before setting aria-hidden=false to ensure we calculate a difference in
          //computed styles
          (0, _animation.recomputeStyle)(dim.$dim);
          dim.cachedOverflow = $overflowEl.css('overflow');
          $overflowEl.css('overflow', 'hidden');
      }
  
      dim.$dim.attr('aria-hidden', 'false');
  
      if (zIndex) {
          dim.$dim.css({ zIndex: zIndex });
      }
  
      return dim.$dim;
  }
  
  /**
   * Removes semitransparent DIV
   * @see dim
   */
  function undim() {
      _hiddenByAui.forEach(function (element) {
          element.removeAttribute('aria-hidden');
      });
  
      if (dim.$dim) {
          dim.$dim.attr('aria-hidden', 'true');
  
          $overflowEl && $overflowEl.css('overflow', dim.cachedOverflow);
      }
  }
  
  var useShimDeprecationLogger = (0, _deprecation.getMessageLogger)('useShim', {
      extraInfo: 'useShim has no alternative as it is now calculated by dim().'
  });
  
  (0, _globalize2.default)('dim', dim);
  (0, _globalize2.default)('undim', undim);
  
  exports.dim = dim;
  exports.undim = undim;
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/focus-manager.js
(typeof window === 'undefined' ? global : window).__5893e4fe35fa01a87803695e8be8b479 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  (function initSelectors() {
      /*
      :tabbable and :focusable functions from jQuery UI v 1.10.4
      renamed to :aui-tabbable and :aui-focusable to not clash with jquery-ui if it's included.
      Code modified slightly to be compatible with jQuery < 1.8
      .addBack() replaced with .andSelf()
      $.curCSS() replaced with $.css()
       */
      function visible(element) {
          return _jquery2.default.css(element, 'visibility') === 'visible';
      }
  
      function focusable(element, isTabIndexNotNaN) {
          var nodeName = element.nodeName.toLowerCase();
  
          if (nodeName === 'aui-select') {
              return true;
          }
  
          if (nodeName === 'area') {
              var map = element.parentNode;
              var mapName = map.name;
              var imageMap = (0, _jquery2.default)('img[usemap=#' + mapName + ']').get();
  
              if (!element.href || !mapName || map.nodeName.toLowerCase() !== 'map') {
                  return false;
              }
              return imageMap && visible(imageMap);
          }
          var isFormElement = /input|select|textarea|button|object/.test(nodeName);
          var isAnchor = nodeName === 'a';
          var isAnchorTabbable = element.href || isTabIndexNotNaN;
  
          return (isFormElement ? !element.disabled : isAnchor ? isAnchorTabbable : isTabIndexNotNaN) && visible(element);
      }
  
      function tabbable(element) {
          var tabIndex = _jquery2.default.attr(element, 'tabindex'),
              isTabIndexNaN = isNaN(tabIndex);
          var hasTabIndex = isTabIndexNaN || tabIndex >= 0;
  
          return hasTabIndex && focusable(element, !isTabIndexNaN);
      }
  
      _jquery2.default.extend(_jquery2.default.expr[':'], {
          'aui-focusable': function auiFocusable(element) {
              return focusable(element, !isNaN(_jquery2.default.attr(element, 'tabindex')));
          },
  
          'aui-tabbable': tabbable
      });
  })();
  
  var RESTORE_FOCUS_DATA_KEY = '_aui-focus-restore';
  function FocusManager() {
      this._focusTrapStack = [];
      (0, _jquery2.default)(document).on('focusout', { focusTrapStack: this._focusTrapStack }, focusTrapHandler);
  }
  FocusManager.defaultFocusSelector = ':aui-tabbable';
  FocusManager.prototype.enter = function ($el) {
      // remember focus on old element
      $el.data(RESTORE_FOCUS_DATA_KEY, (0, _jquery2.default)(document.activeElement));
  
      // focus on new selector
      if ($el.attr('data-aui-focus') !== 'false') {
          var focusSelector = $el.attr('data-aui-focus-selector') || FocusManager.defaultFocusSelector;
          var $focusEl = $el.is(focusSelector) ? $el : $el.find(focusSelector);
          $focusEl.first().focus();
      }
  
      if (elementTrapsFocus($el)) {
          trapFocus($el, this._focusTrapStack);
      }
  };
  
  function trapFocus($el, focusTrapStack) {
      focusTrapStack.push($el);
  }
  
  function untrapFocus(focusTrapStack) {
      focusTrapStack.pop();
  }
  
  function elementTrapsFocus($el) {
      return $el.is('.aui-dialog2');
  }
  
  FocusManager.prototype.exit = function ($el) {
      if (elementTrapsFocus($el)) {
          untrapFocus(this._focusTrapStack);
      }
  
      // AUI-1059: remove focus from the active element when dialog is hidden
      var activeElement = document.activeElement;
      if ($el[0] === activeElement || $el.has(activeElement).length) {
          (0, _jquery2.default)(activeElement).blur();
      }
  
      var $restoreFocus = $el.data(RESTORE_FOCUS_DATA_KEY);
      if ($restoreFocus && $restoreFocus.length) {
          $el.removeData(RESTORE_FOCUS_DATA_KEY);
          $restoreFocus.focus();
      }
  };
  
  function focusTrapHandler(event) {
      var focusTrapStack = event.data.focusTrapStack;
  
      if (!event.relatedTarget) {
          //Does not work in firefox, see https://bugzilla.mozilla.org/show_bug.cgi?id=687787
          return;
      }
  
      if (focusTrapStack.length === 0) {
          return;
      }
  
      var $focusTrapElement = focusTrapStack[focusTrapStack.length - 1];
  
      var focusOrigin = event.target;
      var focusTo = event.relatedTarget;
  
      var $tabbableElements = $focusTrapElement.find(':aui-tabbable');
      var $firstTabbableElement = (0, _jquery2.default)($tabbableElements.first());
      var $lastTabbableElement = (0, _jquery2.default)($tabbableElements.last());
  
      var elementContainsOrigin = $focusTrapElement.has(focusTo).length === 0;
      var focusLeavingElement = elementContainsOrigin && focusTo;
      if (focusLeavingElement) {
          if ($firstTabbableElement.is(focusOrigin)) {
              $lastTabbableElement.focus();
          } else if ($lastTabbableElement.is(focusOrigin)) {
              $firstTabbableElement.focus();
          }
      }
  }
  
  FocusManager.global = new FocusManager();
  
  (0, _globalize2.default)('FocusManager', FocusManager);
  
  exports.default = FocusManager;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/widget.js
(typeof window === 'undefined' ? global : window).__74669bca74d178848b921b882a9fe9d5 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  exports.default = function (name, Ctor) {
      var dataAttr = '_aui-widget-' + name;
      return function (selectorOrOptions, maybeOptions) {
          var selector;
          var options;
          if (_jquery2.default.isPlainObject(selectorOrOptions)) {
              options = selectorOrOptions;
          } else {
              selector = selectorOrOptions;
              options = maybeOptions;
          }
  
          var $el = selector && (0, _jquery2.default)(selector);
  
          var widget;
          if (!$el || !$el.data(dataAttr)) {
              widget = new Ctor($el, options || {});
              $el = widget.$el;
              $el.data(dataAttr, widget);
          } else {
              widget = $el.data(dataAttr);
              // options are discarded if $el has already been constructed
          }
  
          return widget;
      };
  };
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  module.exports = exports['default'];
  
  /**
   * @param {string} name The name of the widget to use in any messaging.
   * @param {function(new:{ $el: jQuery }, ?jQuery, ?Object)} Ctor
   *     A constructor which will only ever be called with "new". It must take a JQuery object as the first
   *     parameter, or generate one if not provided. The second parameter will be a configuration object.
   *     The returned object must have an $el property and a setOptions function.
   * @constructor
   */
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/layer.js
(typeof window === 'undefined' ? global : window).__fb6eb7339db5263fd7887254de970b3b = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  var _blanket = __9631e458b05f1a70cf99301b5000456e;
  
  var _focusManager = __5893e4fe35fa01a87803695e8be8b479;
  
  var _focusManager2 = _interopRequireDefault(_focusManager);
  
  var _globalize = __ec4168b7e78952673964d3581cbee91b;
  
  var _globalize2 = _interopRequireDefault(_globalize);
  
  var _keyCode = __5e00bdc0656ab7ab1098c8394e0010a9;
  
  var _keyCode2 = _interopRequireDefault(_keyCode);
  
  var _widget = __74669bca74d178848b921b882a9fe9d5;
  
  var _widget2 = _interopRequireDefault(_widget);
  
  var _customEvent = __97354d72dc34929750a9514fb0aad090;
  
  var _customEvent2 = _interopRequireDefault(_customEvent);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var EVENT_PREFIX = '_aui-internal-layer-';
  var GLOBAL_EVENT_PREFIX = '_aui-internal-layer-global-';
  var LAYER_EVENT_PREFIX = 'aui-layer-';
  var AUI_EVENT_PREFIX = 'aui-';
  var $doc = (0, _jquery2.default)(document);
  
  // AUI-3708 - Abstracted to reflect code implemented upstream.
  function isTransitioning(el, prop) {
      var transition = window.getComputedStyle(el).transitionProperty;
      return transition ? transition.indexOf(prop) > -1 : false;
  }
  
  function onTransitionEnd(el, prop, func, once) {
      function handler(e) {
          if (prop !== e.propertyName) {
              return;
          }
  
          func.call(el);
  
          if (once) {
              el.removeEventListener('transitionend', handler);
          }
      }
  
      if (isTransitioning(el, prop)) {
          el.addEventListener('transitionend', handler);
      } else {
          func.call(el);
      }
  }
  
  function oneTransitionEnd(el, prop, func) {
      onTransitionEnd(el, prop, func, true);
  }
  // end AUI-3708
  
  function ariaHide($el) {
      $el.attr('aria-hidden', 'true');
  }
  
  function ariaShow($el) {
      $el.attr('aria-hidden', 'false');
  }
  
  /**
  * @return {bool} Returns false if at least one of the event handlers called .preventDefault(). Returns true otherwise.
  */
  function triggerEvent($el, deprecatedName, newNativeName) {
      var e1 = _jquery2.default.Event(EVENT_PREFIX + deprecatedName);
      var e2 = _jquery2.default.Event(GLOBAL_EVENT_PREFIX + deprecatedName);
      // TODO: Remove this 'aui-layer-' prefixed event once it is no longer used by inline dialog and dialog2.
      var nativeEvent = new _customEvent2.default(LAYER_EVENT_PREFIX + newNativeName, {
          bubbles: true,
          cancelable: true
      });
      var nativeEvent2 = new _customEvent2.default(AUI_EVENT_PREFIX + newNativeName, {
          bubbles: true,
          cancelable: true
      });
  
      $el.trigger(e1);
      $el.trigger(e2, [$el]);
      $el[0].dispatchEvent(nativeEvent);
      $el[0].dispatchEvent(nativeEvent2);
  
      return !e1.isDefaultPrevented() && !e2.isDefaultPrevented() && !nativeEvent.defaultPrevented && !nativeEvent2.defaultPrevented;
  }
  
  function Layer(selector) {
      this.$el = (0, _jquery2.default)(selector || '<div class="aui-layer" aria-hidden="true"></div>');
      this.$el.addClass('aui-layer');
  }
  
  Layer.prototype = {
      /**
       * Returns the layer below the current layer if it exists.
       *
       * @returns {jQuery | undefined}
       */
      below: function below() {
          return LayerManager.global.item(LayerManager.global.indexOf(this.$el) - 1);
      },
  
      /**
       * Returns the layer above the current layer if it exists.
       *
       * @returns {jQuery | undefined}
       */
      above: function above() {
          return LayerManager.global.item(LayerManager.global.indexOf(this.$el) + 1);
      },
  
      /**
       * Sets the width and height of the layer.
       *
       * @param {Integer} width The width to set.
       * @param {Integer} height The height to set.
       *
       * @returns {Layer}
       */
      changeSize: function changeSize(width, height) {
          this.$el.css('width', width);
          this.$el.css('height', height === 'content' ? '' : height);
          return this;
      },
  
      /**
       * Binds a layer event.
       *
       * @param {String} event The event name to listen to.
       * @param {Function} fn The event handler.
       *
       * @returns {Layer}
       */
      on: function on(event, fn) {
          this.$el.on(EVENT_PREFIX + event, fn);
          return this;
      },
  
      /**
       * Unbinds a layer event.
       *
       * @param {String} event The event name to unbind=.
       * @param {Function} fn Optional. The event handler.
       *
       * @returns {Layer}
       */
      off: function off(event, fn) {
          this.$el.off(EVENT_PREFIX + event, fn);
          return this;
      },
  
      /**
       * Shows the layer.
       *
       * @returns {Layer}
       */
      show: function show() {
          if (this.isVisible()) {
              ariaShow(this.$el);
              return this;
          }
  
          if (!triggerEvent(this.$el, 'beforeShow', 'show')) {
              return this;
          }
  
          // AUI-3708
          // Ensures that the display property is removed if it's been added
          // during hiding.
          if (this.$el.css('display') === 'none') {
              this.$el.css('display', '');
          }
  
          LayerManager.global.push(this.$el);
  
          return this;
      },
  
      /**
       * Hides the layer.
       *
       * @returns {Layer}
       */
      hide: function hide() {
          if (!this.isVisible()) {
              ariaHide(this.$el);
              return this;
          }
  
          if (!triggerEvent(this.$el, 'beforeHide', 'hide')) {
              return this;
          }
  
          // AUI-3708
          var thisLayer = this;
          oneTransitionEnd(this.$el.get(0), 'opacity', function () {
              if (!thisLayer.isVisible()) {
                  this.style.display = 'none';
              }
          });
  
          LayerManager.global.popUntil(this.$el);
  
          return this;
      },
  
      /**
       * Checks to see if the layer is visible.
       *
       * @returns {Boolean}
       */
      isVisible: function isVisible() {
          return this.$el.attr('aria-hidden') === 'false';
      },
  
      /**
       * Removes the layer and cleans up internal state.
       *
       * @returns {undefined}
       */
      remove: function remove() {
          this.hide();
          this.$el.remove();
          this.$el = null;
      },
  
      /**
       * Returns whether or not the layer is blanketed.
       *
       * @returns {Boolean}
       */
      isBlanketed: function isBlanketed() {
          return this.$el.attr('data-aui-blanketed') === 'true';
      },
  
      /**
       * Returns whether or not the layer is persistent.
       *
       * @returns {Boolean}
       */
      isPersistent: function isPersistent() {
          var modal = this.$el.attr('modal') || this.$el.attr('data-aui-modal');
          var isPersistent = this.$el[0].hasAttribute('persistent');
  
          return modal === 'true' || isPersistent;
      },
  
      _hideLayer: function _hideLayer(triggerBeforeEvents) {
          if (this.isPersistent() || this.isBlanketed()) {
              _focusManager2.default.global.exit(this.$el);
          }
  
          if (triggerBeforeEvents) {
              triggerEvent(this.$el, 'beforeHide', 'hide');
          }
  
          this.$el.attr('aria-hidden', 'true');
          this.$el.css('z-index', this.$el.data('_aui-layer-cached-z-index') || '');
          this.$el.data('_aui-layer-cached-z-index', '');
          this.$el.trigger(EVENT_PREFIX + 'hide');
          this.$el.trigger(GLOBAL_EVENT_PREFIX + 'hide', [this.$el]);
      },
  
      _showLayer: function _showLayer(zIndex) {
          if (!this.$el.parent().is('body')) {
              this.$el.appendTo(document.body);
          }
  
          this.$el.data('_aui-layer-cached-z-index', this.$el.css('z-index'));
          this.$el.css('z-index', zIndex);
          this.$el.attr('aria-hidden', 'false');
  
          if (this.isPersistent() || this.isBlanketed()) {
              _focusManager2.default.global.enter(this.$el);
          }
  
          this.$el.trigger(EVENT_PREFIX + 'show');
          this.$el.trigger(GLOBAL_EVENT_PREFIX + 'show', [this.$el]);
      }
  };
  
  var createLayer = (0, _widget2.default)('layer', Layer);
  
  createLayer.on = function (eventName, selector, fn) {
      $doc.on(GLOBAL_EVENT_PREFIX + eventName, selector, fn);
      return this;
  };
  
  createLayer.off = function (eventName, selector, fn) {
      $doc.off(GLOBAL_EVENT_PREFIX + eventName, selector, fn);
      return this;
  };
  
  // Layer Manager
  // -------------
  
  /**
   * Manages layers.
   *
   * There is a single global layer manager.
   * Additional instances can be created however this should generally only be used in tests.
   *
   * Layers are added by the push($el) method. Layers are removed by the
   * popUntil($el) method.
   *
   * popUntil's contract is that it pops all layers above & including the given
   * layer. This is used to support popping multiple layers.
   * Say we were showing a dropdown inside an inline dialog inside a dialog - we
   * have a stack of dialog layer, inline dialog layer, then dropdown layer. Calling
   * popUntil(dialog.$el) would hide all layers above & including the dialog.
   */
  
  function getTrigger($layer) {
      return (0, _jquery2.default)('[aria-controls="' + $layer.attr('id') + '"]');
  }
  
  function hasTrigger($layer) {
      return getTrigger($layer).length > 0;
  }
  
  function topIndexWhere(layerArr, fn) {
      var i = layerArr.length;
  
      while (i--) {
          if (fn(layerArr[i])) {
              return i;
          }
      }
  
      return -1;
  }
  
  function layerIndex(layerArr, $el) {
      return topIndexWhere(layerArr, function ($layer) {
          return $layer[0] === $el[0];
      });
  }
  
  function topBlanketedIndex(layerArr) {
      return topIndexWhere(layerArr, function ($layer) {
          return createLayer($layer).isBlanketed();
      });
  }
  
  function nextZIndex(layerArr) {
      var _nextZIndex;
  
      if (layerArr.length) {
          var $topEl = layerArr[layerArr.length - 1];
          var zIndex = parseInt($topEl.css('z-index'), 10);
          _nextZIndex = (isNaN(zIndex) ? 0 : zIndex) + 100;
      } else {
          _nextZIndex = 0;
      }
  
      return Math.max(3000, _nextZIndex);
  }
  
  function updateBlanket(stack, oldBlanketIndex) {
      var newTopBlanketedIndex = topBlanketedIndex(stack);
  
      if (oldBlanketIndex !== newTopBlanketedIndex) {
          if (newTopBlanketedIndex > -1) {
              (0, _blanket.dim)(false, stack[newTopBlanketedIndex].css('z-index') - 20);
          } else {
              (0, _blanket.undim)();
          }
      }
  }
  
  function popLayers(stack, stopIndex, forceClosePersistent) {
      if (stopIndex < 0) {
          return;
      }
  
      for (var a = stack.length - 1; a >= stopIndex; a--) {
          var $layer = stack[a];
          var layer = createLayer($layer);
  
          if (forceClosePersistent || !layer.isPersistent()) {
              layer._hideLayer(true);
              stack.splice(a, 1);
          }
      }
  }
  
  function getParentLayer($childLayer) {
      var $layerTrigger = getTrigger($childLayer);
  
      if ($layerTrigger.length > 0) {
          return $layerTrigger.closest('.aui-layer');
      }
  }
  
  function LayerManager() {
      this._stack = [];
  }
  
  LayerManager.prototype = {
      /**
      * Pushes a layer onto the stack. The same element cannot be opened as a layer multiple times - if the given
      * element is already an open layer, this method throws an exception.
      *
      * @param {HTMLElement | String | jQuery} element  The element to push onto the stack.
      *
      * @returns {LayerManager}
      */
      push: function push(element) {
          var $el = element instanceof _jquery2.default ? element : (0, _jquery2.default)(element);
          if (layerIndex(this._stack, $el) >= 0) {
              throw new Error('The given element is already an active layer.');
          }
  
          this.popLayersBeside($el);
  
          var layer = createLayer($el);
          var zIndex = nextZIndex(this._stack);
  
          layer._showLayer(zIndex);
  
          if (layer.isBlanketed()) {
              (0, _blanket.dim)(false, zIndex - 20);
          }
  
          this._stack.push($el);
  
          return this;
      },
  
      popLayersBeside: function popLayersBeside(element) {
          var $layer = element instanceof _jquery2.default ? element : (0, _jquery2.default)(element);
          if (!hasTrigger($layer)) {
              // We can't find this layer's trigger, we will pop all non-persistent until a blanket or the document
              var blanketedIndex = topBlanketedIndex(this._stack);
              popLayers(this._stack, ++blanketedIndex, false);
              return;
          }
  
          var $parentLayer = getParentLayer($layer);
          if ($parentLayer) {
              var parentIndex = this.indexOf($parentLayer);
              popLayers(this._stack, ++parentIndex, false);
          } else {
              popLayers(this._stack, 0, false);
          }
      },
  
      /**
      * Returns the index of the specified layer in the layer stack.
      *
      * @param {HTMLElement | String | jQuery} element  The element to find in the stack.
      *
      * @returns {Number} the (zero-based) index of the element, or -1 if not in the stack.
      */
      indexOf: function indexOf(element) {
          return layerIndex(this._stack, (0, _jquery2.default)(element));
      },
  
      /**
      * Returns the item at the particular index or false.
      *
      * @param {Number} index The index of the element to get.
      *
      * @returns {jQuery | Boolean}
      */
      item: function item(index) {
          return this._stack[index];
      },
  
      /**
      * Hides all layers in the stack.
      *
      * @returns {LayerManager}
      */
      hideAll: function hideAll() {
          this._stack.reverse().forEach(function (element) {
              var layer = createLayer(element);
              if (layer.isBlanketed() || layer.isPersistent()) {
                  return;
              }
              layer.hide();
          });
  
          return this;
      },
  
      /**
      * Gets the previous layer below the given layer, which is non modal and non persistent. If it finds a blanketed layer on the way
      * it returns it regardless if it is modal or not
      *
      * @param {HTMLElement | String | jQuery} element layer to start the search from.
      *
      * @returns {jQuery | null} the next matching layer or null if none found.
      */
      getNextLowerNonPersistentOrBlanketedLayer: function getNextLowerNonPersistentOrBlanketedLayer(element) {
          var $el = element instanceof _jquery2.default ? element : (0, _jquery2.default)(element);
          var index = layerIndex(this._stack, $el);
  
          if (index < 0) {
              return null;
          }
  
          var $nextEl;
          index--;
          while (index >= 0) {
              $nextEl = this._stack[index];
              var layer = createLayer($nextEl);
  
              if (!layer.isPersistent() || layer.isBlanketed()) {
                  return $nextEl;
              }
              index--;
          }
  
          return null;
      },
  
      /**
      * Gets the next layer which is neither modal or blanketed, from the given layer.
      *
      * @param {HTMLElement | String | jQuery} element layer to start the search from.
      *
      * @returns {jQuery | null} the next non modal non blanketed layer or null if none found.
      */
      getNextHigherNonPeristentAndNonBlanketedLayer: function getNextHigherNonPeristentAndNonBlanketedLayer(element) {
          var $el = element instanceof _jquery2.default ? element : (0, _jquery2.default)(element);
          var index = layerIndex(this._stack, $el);
  
          if (index < 0) {
              return null;
          }
  
          var $nextEl;
          index++;
          while (index < this._stack.length) {
              $nextEl = this._stack[index];
              var layer = createLayer($nextEl);
  
              if (!(layer.isPersistent() || layer.isBlanketed())) {
                  return $nextEl;
              }
              index++;
          }
  
          return null;
      },
  
      /**
      * Removes all non-modal layers above & including the given element. If the given element is not an active layer, this method
      * is a no-op. The given element will be removed regardless of whether or not it is modal.
      *
      * @param {HTMLElement | String | jQuery} element layer to pop.
      *
      * @returns {jQuery} The last layer that was popped, or null if no layer matching the given $el was found.
      */
      popUntil: function popUntil(element) {
          var $el = element instanceof _jquery2.default ? element : (0, _jquery2.default)(element);
          var index = layerIndex(this._stack, $el);
  
          if (index === -1) {
              return null;
          }
  
          var oldTopBlanketedIndex = topBlanketedIndex(this._stack);
  
          // Removes all layers above the current one.
          popLayers(this._stack, index + 1, createLayer($el).isBlanketed());
  
          // Removes the current layer.
          createLayer($el)._hideLayer();
          this._stack.splice(index, 1);
  
          updateBlanket(this._stack, oldTopBlanketedIndex);
  
          return $el;
      },
  
      /**
      * Gets the top layer, if it exists.
      *
      * @returns The layer on top of the stack, if it exists, otherwise null.
      */
      getTopLayer: function getTopLayer() {
          if (!this._stack.length) {
              return null;
          }
  
          var $topLayer = this._stack[this._stack.length - 1];
  
          return $topLayer;
      },
  
      /**
      * Pops the top layer, if it exists and it is non modal and non persistent.
      *
      * @returns The layer that was popped, if it was popped.
      */
      popTopIfNonPersistent: function popTopIfNonPersistent() {
          var $topLayer = this.getTopLayer();
          var layer = createLayer($topLayer);
  
          if (!$topLayer || layer.isPersistent()) {
              return null;
          }
  
          return this.popUntil($topLayer);
      },
  
      /**
      * Pops all layers above and including the top blanketed layer. If layers exist but none are blanketed, this method
      * does nothing.
      *
      * @returns The blanketed layer that was popped, if it exists, otherwise null.
      */
      popUntilTopBlanketed: function popUntilTopBlanketed() {
          var i = topBlanketedIndex(this._stack);
  
          if (i < 0) {
              return null;
          }
  
          var $topBlanketedLayer = this._stack[i];
          var layer = createLayer($topBlanketedLayer);
  
          if (layer.isPersistent()) {
              // We can't pop the blanketed layer, only the things ontop
              var $next = this.getNextHigherNonPeristentAndNonBlanketedLayer($topBlanketedLayer);
              if ($next) {
                  var stopIndex = layerIndex(this._stack, $next);
                  popLayers(this._stack, stopIndex, true);
                  return $next;
              }
              return null;
          }
  
          popLayers(this._stack, i, true);
          updateBlanket(this._stack, i);
          return $topBlanketedLayer;
      },
  
      /**
      * Pops all layers above and including the top persistent layer. If layers exist but none are persistent, this method
      * does nothing.
      */
      popUntilTopPersistent: function popUntilTopPersistent() {
          var $toPop = LayerManager.global.getTopLayer();
          if (!$toPop) {
              return;
          }
  
          var stopIndex;
          var oldTopBlanketedIndex = topBlanketedIndex(this._stack);
  
          var toPop = createLayer($toPop);
          if (toPop.isPersistent()) {
              if (toPop.isBlanketed()) {
                  return;
              } else {
                  // Get the closest non modal layer below, stop at the first blanketed layer though, we don't want to pop below that
                  $toPop = LayerManager.global.getNextLowerNonPersistentOrBlanketedLayer($toPop);
                  toPop = createLayer($toPop);
  
                  if ($toPop && !toPop.isPersistent()) {
                      stopIndex = layerIndex(this._stack, $toPop);
                      popLayers(this._stack, stopIndex, true);
                      updateBlanket(this._stack, oldTopBlanketedIndex);
                  } else {
                      // Here we have a blanketed persistent layer
                      return;
                  }
              }
          } else {
              stopIndex = layerIndex(this._stack, $toPop);
              popLayers(this._stack, stopIndex, true);
              updateBlanket(this._stack, oldTopBlanketedIndex);
          }
      }
  };
  
  // LayerManager.global
  // -------------------
  
  function initCloseLayerOnEscPress() {
      $doc.on('keydown', function (e) {
          if (e.keyCode === _keyCode2.default.ESCAPE) {
              LayerManager.global.popUntilTopPersistent();
              e.preventDefault();
          }
      });
  }
  
  function initCloseLayerOnBlanketClick() {
      $doc.on('click', '.aui-blanket', function (e) {
          if (LayerManager.global.popUntilTopBlanketed()) {
              e.preventDefault();
          }
      });
  }
  
  function hasLayer($trigger) {
      if (!$trigger.length) {
          return false;
      }
  
      var layer = document.getElementById($trigger.attr('aria-controls'));
      return LayerManager.global.indexOf(layer) > -1;
  }
  
  // If it's a click on a trigger, do nothing.
  // If it's a click on a layer, close all layers above.
  // Otherwise, close all layers.
  function initCloseLayerOnOuterClick() {
      $doc.on('click', function (e) {
          var $target = (0, _jquery2.default)(e.target);
          if ($target.closest('.aui-blanket').length) {
              return;
          }
  
          var $trigger = $target.closest('[aria-controls]');
          var $layer = $target.closest('.aui-layer');
          if (!$layer.length && !hasLayer($trigger)) {
              LayerManager.global.hideAll();
              return;
          }
  
          // Triggers take precedence over layers
          if (hasLayer($trigger)) {
              return;
          }
  
          if ($layer.length) {
              // We dont want to explicitly call close on a modal dialog if it happens to be next.
              // All blanketed layers should be below us, as otherwise the blanket should have caught the click.
              // We make sure we dont close a blanketed one explicitly as a hack, this is to fix the problem arising
              // from dialog2 triggers inside dialog2's having no aria controls, where the dialog2 that was just
              // opened would be closed instantly
              var $next = LayerManager.global.getNextHigherNonPeristentAndNonBlanketedLayer($layer);
  
              if ($next) {
                  createLayer($next).hide();
              }
          }
      });
  }
  
  initCloseLayerOnEscPress();
  initCloseLayerOnBlanketClick();
  initCloseLayerOnOuterClick();
  
  LayerManager.global = new LayerManager();
  createLayer.Manager = LayerManager;
  
  (0, _globalize2.default)('layer', createLayer);
  
  exports.default = createLayer;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/state.js
(typeof window === 'undefined' ? global : window).__214d27a9777ad806977cd604056460e5 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  function state(element) {
      return {
          /**
           * sets an internal state on a component element
           * @param element the element to which the state will be added
           * @param stateName the name of the state
           * @param stateValue the value that the state will be changed to
           */
          set: function set(stateName, stateValue) {
              if (element._state === undefined) {
                  element._state = {};
              }
  
              element._state[stateName] = stateValue;
          },
  
          /**
           * gets an internal state on a component element
           * @param element the element to which the state will be added
           * @param stateName the name of the state
           */
          get: function get(stateName) {
              if (element._state) {
                  return element._state[stateName];
              }
          }
      };
  }
  
  exports.default = state;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/constants.js
(typeof window === 'undefined' ? global : window).__a7b0062609fe760fb31266997d736e07 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__a7b0062609fe760fb31266997d736e07");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports"], factory);
    } else if (typeof exports !== "undefined") {
      factory(exports);
    }
  })(function (exports) {
  
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
  
    var ATTR_IGNORE = "data-skate-ignore";
    exports.ATTR_IGNORE = ATTR_IGNORE;
    var TYPE_ATTRIBUTE = "a";
    exports.TYPE_ATTRIBUTE = TYPE_ATTRIBUTE;
    var TYPE_CLASSNAME = "c";
    exports.TYPE_CLASSNAME = TYPE_CLASSNAME;
    var TYPE_ELEMENT = "t";
    exports.TYPE_ELEMENT = TYPE_ELEMENT;
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/version.js
(typeof window === 'undefined' ? global : window).__3f4bcf0b1224f1e2aec2d67adb81d7da = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__3f4bcf0b1224f1e2aec2d67adb81d7da");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "module"], factory);
    } else if (typeof exports !== "undefined" && typeof module !== "undefined") {
      factory(exports, module);
    }
  })(function (exports, module) {
  
    module.exports = "0.13.16";
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/globals.js
(typeof window === 'undefined' ? global : window).__c4a6b20f5235e3eec22c66f352171e2d = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports,
    "./version": __3f4bcf0b1224f1e2aec2d67adb81d7da,
    "./version": __3f4bcf0b1224f1e2aec2d67adb81d7da
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__c4a6b20f5235e3eec22c66f352171e2d");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "module", "./version"], factory);
    } else if (typeof exports !== "undefined" && typeof module !== "undefined") {
      factory(exports, module, __3f4bcf0b1224f1e2aec2d67adb81d7da);
    }
  })(function (exports, module, _version) {
  
    var _interopRequire = function _interopRequire(obj) {
      return obj && obj.__esModule ? obj["default"] : obj;
    };
  
    var version = _interopRequire(_version);
  
    var VERSION = "__skate_" + version.replace(/[^\w]/g, "_");
  
    if (!window[VERSION]) {
      window[VERSION] = {
        observer: undefined,
        registry: {}
      };
    }
  
    module.exports = window[VERSION];
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/data.js
(typeof window === 'undefined' ? global : window).__8898ec9cf00d9d75ddaabec66b7c9624 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__8898ec9cf00d9d75ddaabec66b7c9624");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "module"], factory);
    } else if (typeof exports !== "undefined" && typeof module !== "undefined") {
      factory(exports, module);
    }
  })(function (exports, module) {
  
    module.exports = function (element) {
      var namespace = arguments[1] === undefined ? "" : arguments[1];
  
      var data = element.__SKATE_DATA || (element.__SKATE_DATA = {});
      return namespace && (data[namespace] || (data[namespace] = {})) || data;
    };
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/mutation-observer.js
(typeof window === 'undefined' ? global : window).__bbc1d2b662a76620d0af1c87a6f18011 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__bbc1d2b662a76620d0af1c87a6f18011");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports"], factory);
    } else if (typeof exports !== "undefined") {
      factory(exports);
    }
  })(function (exports) {
  
    (function (self) {
      // Atlassian: added IIFE
      /**
       * @license
       * Copyright (c) 2014 The Polymer Project Authors. All rights reserved.
       * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
       * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
       * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
       * Code distributed by Google as part of the polymer project is also
       * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
       */
      // @version 0.7.15
      if (typeof WeakMap === "undefined") {
        (function () {
          var defineProperty = Object.defineProperty;
          var counter = Date.now() % 1000000000;
          var WeakMap = function WeakMap() {
            this.name = "__st" + (Math.random() * 1000000000 >>> 0) + (counter++ + "__");
          };
          WeakMap.prototype = {
            set: function set(key, value) {
              var entry = key[this.name];
              if (entry && entry[0] === key) entry[1] = value;else defineProperty(key, this.name, {
                value: [key, value],
                writable: true
              });
              return this;
            },
            get: function get(key) {
              var entry;
              return (entry = key[this.name]) && entry[0] === key ? entry[1] : undefined;
            },
            "delete": function _delete(key) {
              var entry = key[this.name];
              if (!entry || entry[0] !== key) {
                return false;
              }entry[0] = entry[1] = undefined;
              return true;
            },
            has: function has(key) {
              var entry = key[this.name];
              if (!entry) {
                return false;
              }return entry[0] === key;
            }
          };
          window.WeakMap = WeakMap;
        })();
      }
  
      (function (global) {
        if (global.JsMutationObserver) {
          return;
        }
        var registrationsTable = new WeakMap();
        var setImmediate;
        if (/Trident|Edge/.test(navigator.userAgent)) {
          setImmediate = setTimeout;
        } else if (window.setImmediate) {
          setImmediate = window.setImmediate;
        } else {
          var setImmediateQueue = [];
          var sentinel = String(Math.random());
          window.addEventListener("message", function (e) {
            if (e.data === sentinel) {
              var queue = setImmediateQueue;
              setImmediateQueue = [];
              queue.forEach(function (func) {
                func();
              });
            }
          });
          setImmediate = function setImmediate(func) {
            setImmediateQueue.push(func);
            window.postMessage(sentinel, "*");
          };
        }
        var isScheduled = false;
        var scheduledObservers = [];
        function scheduleCallback(observer) {
          scheduledObservers.push(observer);
          if (!isScheduled) {
            isScheduled = true;
            setImmediate(dispatchCallbacks);
          }
        }
        function wrapIfNeeded(node) {
          return window.ShadowDOMPolyfill && window.ShadowDOMPolyfill.wrapIfNeeded(node) || node;
        }
        function dispatchCallbacks() {
          isScheduled = false;
          var observers = scheduledObservers;
          scheduledObservers = [];
          observers.sort(function (o1, o2) {
            return o1.uid_ - o2.uid_;
          });
          var anyNonEmpty = false;
          observers.forEach(function (observer) {
            var queue = observer.takeRecords();
            removeTransientObserversFor(observer);
            if (queue.length) {
              observer.callback_(queue, observer);
              anyNonEmpty = true;
            }
          });
          if (anyNonEmpty) dispatchCallbacks();
        }
        function removeTransientObserversFor(observer) {
          observer.nodes_.forEach(function (node) {
            var registrations = registrationsTable.get(node);
            if (!registrations) return;
            registrations.forEach(function (registration) {
              if (registration.observer === observer) registration.removeTransientObservers();
            });
          });
        }
        function forEachAncestorAndObserverEnqueueRecord(target, callback) {
          for (var node = target; node; node = node.parentNode) {
            var registrations = registrationsTable.get(node);
            if (registrations) {
              for (var j = 0; j < registrations.length; j++) {
                var registration = registrations[j];
                var options = registration.options;
                if (node !== target && !options.subtree) continue;
                var record = callback(options);
                if (record) registration.enqueue(record);
              }
            }
          }
        }
        var uidCounter = 0;
        function JsMutationObserver(callback) {
          this.callback_ = callback;
          this.nodes_ = [];
          this.records_ = [];
          this.uid_ = ++uidCounter;
        }
        JsMutationObserver.prototype = {
          observe: function observe(target, options) {
            target = wrapIfNeeded(target);
            if (!options.childList && !options.attributes && !options.characterData || options.attributeOldValue && !options.attributes || options.attributeFilter && options.attributeFilter.length && !options.attributes || options.characterDataOldValue && !options.characterData) {
              throw new SyntaxError();
            }
            var registrations = registrationsTable.get(target);
            if (!registrations) registrationsTable.set(target, registrations = []);
            var registration;
            for (var i = 0; i < registrations.length; i++) {
              if (registrations[i].observer === this) {
                registration = registrations[i];
                registration.removeListeners();
                registration.options = options;
                break;
              }
            }
            if (!registration) {
              registration = new Registration(this, target, options);
              registrations.push(registration);
              this.nodes_.push(target);
            }
            registration.addListeners();
          },
          disconnect: function disconnect() {
            this.nodes_.forEach(function (node) {
              var registrations = registrationsTable.get(node);
              for (var i = 0; i < registrations.length; i++) {
                var registration = registrations[i];
                if (registration.observer === this) {
                  registration.removeListeners();
                  registrations.splice(i, 1);
                  break;
                }
              }
            }, this);
            this.records_ = [];
          },
          takeRecords: function takeRecords() {
            var copyOfRecords = this.records_;
            this.records_ = [];
            return copyOfRecords;
          }
        };
        function MutationRecord(type, target) {
          this.type = type;
          this.target = target;
          this.addedNodes = [];
          this.removedNodes = [];
          this.previousSibling = null;
          this.nextSibling = null;
          this.attributeName = null;
          this.attributeNamespace = null;
          this.oldValue = null;
        }
        function copyMutationRecord(original) {
          var record = new MutationRecord(original.type, original.target);
          record.addedNodes = original.addedNodes.slice();
          record.removedNodes = original.removedNodes.slice();
          record.previousSibling = original.previousSibling;
          record.nextSibling = original.nextSibling;
          record.attributeName = original.attributeName;
          record.attributeNamespace = original.attributeNamespace;
          record.oldValue = original.oldValue;
          return record;
        }
        var currentRecord, recordWithOldValue;
        function getRecord(type, target) {
          return currentRecord = new MutationRecord(type, target);
        }
        function getRecordWithOldValue(oldValue) {
          if (recordWithOldValue) {
            return recordWithOldValue;
          }recordWithOldValue = copyMutationRecord(currentRecord);
          recordWithOldValue.oldValue = oldValue;
          return recordWithOldValue;
        }
        function clearRecords() {
          currentRecord = recordWithOldValue = undefined;
        }
        function recordRepresentsCurrentMutation(record) {
          return record === recordWithOldValue || record === currentRecord;
        }
        function selectRecord(lastRecord, newRecord) {
          if (lastRecord === newRecord) {
            return lastRecord;
          }if (recordWithOldValue && recordRepresentsCurrentMutation(lastRecord)) {
            return recordWithOldValue;
          }return null;
        }
        function Registration(observer, target, options) {
          this.observer = observer;
          this.target = target;
          this.options = options;
          this.transientObservedNodes = [];
        }
        Registration.prototype = {
          enqueue: function enqueue(record) {
            var records = this.observer.records_;
            var length = records.length;
            if (records.length > 0) {
              var lastRecord = records[length - 1];
              var recordToReplaceLast = selectRecord(lastRecord, record);
              if (recordToReplaceLast) {
                records[length - 1] = recordToReplaceLast;
                return;
              }
            } else {
              scheduleCallback(this.observer);
            }
            records[length] = record;
          },
          addListeners: function addListeners() {
            this.addListeners_(this.target);
          },
          addListeners_: function addListeners_(node) {
            var options = this.options;
            if (options.attributes) node.addEventListener("DOMAttrModified", this, true);
            if (options.characterData) node.addEventListener("DOMCharacterDataModified", this, true);
            if (options.childList) node.addEventListener("DOMNodeInserted", this, true);
            if (options.childList || options.subtree) node.addEventListener("DOMNodeRemoved", this, true);
          },
          removeListeners: function removeListeners() {
            this.removeListeners_(this.target);
          },
          removeListeners_: function removeListeners_(node) {
            var options = this.options;
            if (options.attributes) node.removeEventListener("DOMAttrModified", this, true);
            if (options.characterData) node.removeEventListener("DOMCharacterDataModified", this, true);
            if (options.childList) node.removeEventListener("DOMNodeInserted", this, true);
            if (options.childList || options.subtree) node.removeEventListener("DOMNodeRemoved", this, true);
          },
          addTransientObserver: function addTransientObserver(node) {
            if (node === this.target) {
              return;
            }this.addListeners_(node);
            this.transientObservedNodes.push(node);
            var registrations = registrationsTable.get(node);
            if (!registrations) registrationsTable.set(node, registrations = []);
            registrations.push(this);
          },
          removeTransientObservers: function removeTransientObservers() {
            var transientObservedNodes = this.transientObservedNodes;
            this.transientObservedNodes = [];
            transientObservedNodes.forEach(function (node) {
              this.removeListeners_(node);
              var registrations = registrationsTable.get(node);
              for (var i = 0; i < registrations.length; i++) {
                if (registrations[i] === this) {
                  registrations.splice(i, 1);
                  break;
                }
              }
            }, this);
          },
          handleEvent: function handleEvent(e) {
            e.stopImmediatePropagation();
            switch (e.type) {
              case "DOMAttrModified":
                var name = e.attrName;
                var namespace = e.relatedNode.namespaceURI;
                var target = e.target;
                var record = new getRecord("attributes", target);
                record.attributeName = name;
                record.attributeNamespace = namespace;
                var oldValue = e.attrChange === MutationEvent.ADDITION ? null : e.prevValue;
                forEachAncestorAndObserverEnqueueRecord(target, function (options) {
                  if (!options.attributes) return;
                  if (options.attributeFilter && options.attributeFilter.length && options.attributeFilter.indexOf(name) === -1 && options.attributeFilter.indexOf(namespace) === -1) {
                    return;
                  }
                  if (options.attributeOldValue) return getRecordWithOldValue(oldValue);
                  return record;
                });
                break;
  
              case "DOMCharacterDataModified":
                var target = e.target;
                var record = getRecord("characterData", target);
                var oldValue = e.prevValue;
                forEachAncestorAndObserverEnqueueRecord(target, function (options) {
                  if (!options.characterData) return;
                  if (options.characterDataOldValue) return getRecordWithOldValue(oldValue);
                  return record;
                });
                break;
  
              case "DOMNodeRemoved":
                this.addTransientObserver(e.target);
  
              case "DOMNodeInserted":
                var changedNode = e.target;
                var addedNodes, removedNodes;
                if (e.type === "DOMNodeInserted") {
                  addedNodes = [changedNode];
                  removedNodes = [];
                } else {
                  addedNodes = [];
                  removedNodes = [changedNode];
                }
                var previousSibling = changedNode.previousSibling;
                var nextSibling = changedNode.nextSibling;
                var record = getRecord("childList", e.target.parentNode);
                record.addedNodes = addedNodes;
                record.removedNodes = removedNodes;
                record.previousSibling = previousSibling;
                record.nextSibling = nextSibling;
                forEachAncestorAndObserverEnqueueRecord(e.relatedNode, function (options) {
                  if (!options.childList) return;
                  return record;
                });
            }
            clearRecords();
          }
        };
        global.JsMutationObserver = JsMutationObserver;
        if (!global.MutationObserver) {
          global.MutationObserver = JsMutationObserver;
          JsMutationObserver._isPolyfilled = true;
        }
      })(self);
    })(window); // Atlassian: added IIFE
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/utils.js
(typeof window === 'undefined' ? global : window).__4040a82bae2969ea3a1a790da8b2fc9c = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports,
    "./constants": __a7b0062609fe760fb31266997d736e07,
    "./constants": __a7b0062609fe760fb31266997d736e07
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__4040a82bae2969ea3a1a790da8b2fc9c");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "./constants"], factory);
    } else if (typeof exports !== "undefined") {
      factory(exports, __a7b0062609fe760fb31266997d736e07);
    }
  })(function (exports, _constants) {
  
    /**
     * Checks {}.hasOwnProperty in a safe way.
     *
     * @param {Object} obj The object the property is on.
     * @param {String} key The object key to check.
     *
     * @returns {Boolean}
     */
  
    exports.hasOwn = hasOwn;
  
    /**
     * Camel-cases the specified string.
     *
     * @param {String} str The string to camel-case.
     *
     * @returns {String}
     */
    exports.camelCase = camelCase;
  
    /**
     * Returns whether or not the source element contains the target element.
     * This is for browsers that don't support Element.prototype.contains on an
     * HTMLUnknownElement.
     *
     * @param {HTMLElement} source The source element.
     * @param {HTMLElement} target The target element.
     *
     * @returns {Boolean}
     */
    exports.elementContains = elementContains;
  
    /**
     * Returns a function that will prevent more than one call in a single clock
     * tick.
     *
     * @param {Function} fn The function to call.
     *
     * @returns {Function}
     */
    exports.debounce = debounce;
  
    /**
     * Returns whether or not the specified element has been selectively ignored.
     *
     * @param {Element} element The element to check and traverse up from.
     *
     * @returns {Boolean}
     */
    exports.getClosestIgnoredElement = getClosestIgnoredElement;
  
    /**
     * Merges the second argument into the first.
     *
     * @param {Object} child The object to merge into.
     * @param {Object} parent The object to merge from.
     * @param {Boolean} overwrite Whether or not to overwrite properties on the child.
     *
     * @returns {Object} Returns the child object.
     */
    exports.inherit = inherit;
  
    /**
     * Traverses an object checking hasOwnProperty.
     *
     * @param {Object} obj The object to traverse.
     * @param {Function} fn The function to call for each item in the object.
     *
     * @returns {undefined}
     */
    exports.objEach = objEach;
    exports.supportsNativeCustomElements = supportsNativeCustomElements;
    exports.isValidNativeCustomElementName = isValidNativeCustomElementName;
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
  
    var ATTR_IGNORE = _constants.ATTR_IGNORE;
    var elementPrototype = window.HTMLElement.prototype;
    exports.elementPrototype = elementPrototype;
    var elementPrototypeContains = elementPrototype.contains;
    function hasOwn(obj, key) {
      return Object.prototype.hasOwnProperty.call(obj, key);
    }
  
    function camelCase(str) {
      return str.split(/-/g).map(function (str, index) {
        return index === 0 ? str : str[0].toUpperCase() + str.substring(1);
      }).join("");
    }
  
    function elementContains(source, target) {
      // The document element does not have the contains method in IE.
      if (source === document && !source.contains) {
        return document.head.contains(target) || document.body.contains(target);
      }
  
      return source.contains ? source.contains(target) : elementPrototypeContains.call(source, target);
    }
  
    function debounce(fn) {
      var called = false;
  
      return function () {
        if (!called) {
          called = true;
          setTimeout(function () {
            called = false;
            fn();
          }, 1);
        }
      };
    }
  
    function getClosestIgnoredElement(element) {
      var parent = element;
  
      // e.g. document doesn't have a function hasAttribute; no need to go further up
      while (parent instanceof Element) {
        if (parent.hasAttribute(ATTR_IGNORE)) {
          return parent;
        }
  
        parent = parent.parentNode;
      }
    }
  
    function inherit(child, parent, overwrite) {
      var names = Object.getOwnPropertyNames(parent);
      var namesLen = names.length;
  
      for (var a = 0; a < namesLen; a++) {
        var name = names[a];
  
        if (overwrite || child[name] === undefined) {
          var desc = Object.getOwnPropertyDescriptor(parent, name);
          var shouldDefineProps = desc.get || desc.set || !desc.writable || !desc.enumerable || !desc.configurable;
  
          if (shouldDefineProps) {
            Object.defineProperty(child, name, desc);
          } else {
            child[name] = parent[name];
          }
        }
      }
  
      return child;
    }
  
    function objEach(obj, fn) {
      for (var a in obj) {
        if (hasOwn(obj, a)) {
          fn(obj[a], a);
        }
      }
    }
  
    function supportsNativeCustomElements() {
      return typeof document.registerElement === "function";
    }
  
    function isValidNativeCustomElementName(name) {
      return name.indexOf("-") > 0;
    }
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/registry.js
(typeof window === 'undefined' ? global : window).__7730a833b569959f139e5fa88a9ba6f7 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports,
    "./constants": __a7b0062609fe760fb31266997d736e07,
    "./globals": __c4a6b20f5235e3eec22c66f352171e2d,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c,
    "./constants": __a7b0062609fe760fb31266997d736e07,
    "./globals": __c4a6b20f5235e3eec22c66f352171e2d,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__7730a833b569959f139e5fa88a9ba6f7");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "module", "./constants", "./globals", "./utils"], factory);
    } else if (typeof exports !== "undefined" && typeof module !== "undefined") {
      factory(exports, module, __a7b0062609fe760fb31266997d736e07, __c4a6b20f5235e3eec22c66f352171e2d, __4040a82bae2969ea3a1a790da8b2fc9c);
    }
  })(function (exports, module, _constants, _globals, _utils) {
  
    var _interopRequire = function _interopRequire(obj) {
      return obj && obj.__esModule ? obj["default"] : obj;
    };
  
    var TYPE_ATTRIBUTE = _constants.TYPE_ATTRIBUTE;
    var TYPE_CLASSNAME = _constants.TYPE_CLASSNAME;
    var TYPE_ELEMENT = _constants.TYPE_ELEMENT;
  
    var globals = _interopRequire(_globals);
  
    var hasOwn = _utils.hasOwn;
    var isValidNativeCustomElementName = _utils.isValidNativeCustomElementName;
    var supportsNativeCustomElements = _utils.supportsNativeCustomElements;
  
    /**
     * Returns the class list for the specified element.
     *
     * @param {Element} element The element to get the class list for.
     *
     * @returns {ClassList | Array}
     */
    function getClassList(element) {
      var classList = element.classList;
  
      if (classList) {
        return classList;
      }
  
      var attrs = element.attributes;
  
      return attrs["class"] && attrs["class"].nodeValue.split(/\s+/) || [];
    }
  
    module.exports = {
      clear: function clear() {
        globals.registry = {};
        return this;
      },
  
      get: function get(id) {
        return hasOwn(globals.registry, id) && globals.registry[id];
      },
  
      getForElement: function getForElement(element) {
        var attrs = element.attributes;
        var attrsLen = attrs.length;
        var definitions = [];
        var isAttr = attrs.is;
        var isAttrValue = isAttr && (isAttr.value || isAttr.nodeValue);
  
        // Using localName as fallback for edge cases when processing <object> tag that is used
        // as inteface to NPAPI plugin.
        var tag = (element.tagName || element.localName).toLowerCase();
        var isAttrOrTag = isAttrValue || tag;
        var definition;
        var tagToExtend;
  
        if (this.isType(isAttrOrTag, TYPE_ELEMENT)) {
          definition = globals.registry[isAttrOrTag];
          tagToExtend = definition["extends"];
  
          if (isAttrValue) {
            if (tag === tagToExtend) {
              definitions.push(definition);
            }
          } else if (!tagToExtend) {
            definitions.push(definition);
          }
        }
  
        for (var a = 0; a < attrsLen; a++) {
          var attr = attrs[a].nodeName;
  
          if (this.isType(attr, TYPE_ATTRIBUTE)) {
            definition = globals.registry[attr];
            tagToExtend = definition["extends"];
  
            if (!tagToExtend || tag === tagToExtend) {
              definitions.push(definition);
            }
          }
        }
  
        var classList = getClassList(element);
        var classListLen = classList.length;
  
        for (var b = 0; b < classListLen; b++) {
          var className = classList[b];
  
          if (this.isType(className, TYPE_CLASSNAME)) {
            definition = globals.registry[className];
            tagToExtend = definition["extends"];
  
            if (!tagToExtend || tag === tagToExtend) {
              definitions.push(definition);
            }
          }
        }
  
        return definitions;
      },
  
      isType: function isType(id, type) {
        var def = this.get(id);
        return def && def.type === type;
      },
  
      isNativeCustomElement: function isNativeCustomElement(id) {
        return supportsNativeCustomElements() && this.isType(id, TYPE_ELEMENT) && isValidNativeCustomElementName(id);
      },
  
      set: function set(id, definition) {
        if (hasOwn(globals.registry, id)) {
          throw new Error("A component definition of type \"" + definition.type + "\" with the ID of \"" + id + "\" already exists.");
        }
  
        globals.registry[id] = definition;
  
        return this;
      }
    };
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/lifecycle.js
(typeof window === 'undefined' ? global : window).__48b29e539678e044870636b94df11349 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports,
    "./constants": __a7b0062609fe760fb31266997d736e07,
    "./data": __8898ec9cf00d9d75ddaabec66b7c9624,
    "./mutation-observer": __bbc1d2b662a76620d0af1c87a6f18011,
    "./registry": __7730a833b569959f139e5fa88a9ba6f7,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c,
    "./constants": __a7b0062609fe760fb31266997d736e07,
    "./data": __8898ec9cf00d9d75ddaabec66b7c9624,
    "./mutation-observer": __bbc1d2b662a76620d0af1c87a6f18011,
    "./registry": __7730a833b569959f139e5fa88a9ba6f7,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__48b29e539678e044870636b94df11349");
  define.amd = true;
  
  "use strict";
  
  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "./constants", "./data", "./mutation-observer", "./registry", "./utils"], factory);
    } else if (typeof exports !== "undefined") {
      factory(exports, __a7b0062609fe760fb31266997d736e07, __8898ec9cf00d9d75ddaabec66b7c9624, __bbc1d2b662a76620d0af1c87a6f18011, __7730a833b569959f139e5fa88a9ba6f7, __4040a82bae2969ea3a1a790da8b2fc9c);
    }
  })(function (exports, _constants, _data, _mutationObserver, _registry, _utils) {
  
    var _interopRequire = function _interopRequire(obj) {
      return obj && obj.__esModule ? obj["default"] : obj;
    };
  
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
  
    var ATTR_IGNORE = _constants.ATTR_IGNORE;
  
    var data = _interopRequire(_data);
  
    var registry = _interopRequire(_registry);
  
    var camelCase = _utils.camelCase;
    var elementContains = _utils.elementContains;
    var hasOwn = _utils.hasOwn;
    var inherit = _utils.inherit;
    var objEach = _utils.objEach;
    var Node = window.Node;
    //jshint ignore:line
    var elProto = window.HTMLElement.prototype;
    var nativeMatchesSelector = elProto.matches || elProto.msMatchesSelector || elProto.webkitMatchesSelector || elProto.mozMatchesSelector || elProto.oMatchesSelector;
    // Only IE9 has this msMatchesSelector bug, but best to detect it.
    var hasNativeMatchesSelectorDetattachedBug = !nativeMatchesSelector.call(document.createElement("div"), "div");
    var matchesSelector = function matchesSelector(element, selector) {
      if (hasNativeMatchesSelectorDetattachedBug) {
        var clone = element.cloneNode();
        document.createElement("div").appendChild(clone);
        return nativeMatchesSelector.call(clone, selector);
      }
      return nativeMatchesSelector.call(element, selector);
    };
  
    /**
     * Parses an event definition and returns information about it.
     *
     * @param {String} e The event to parse.
     *
     * @returns {Object]}
     */
    function parseEvent(e) {
      var parts = e.split(" ");
      return {
        name: parts.shift(),
        delegate: parts.join(" ")
      };
    }
  
    /**
     * Sets the defined attributes to their default values, if specified.
     *
     * @param {Element} target The web component element.
     * @param {Object} component The web component definition.
     *
     * @returns {undefined}
     */
    function initAttributes(target, component) {
      var componentAttributes = component.attributes;
  
      if ((typeof componentAttributes === "undefined" ? "undefined" : _typeof(componentAttributes)) !== "object") {
        return;
      }
  
      for (var attribute in componentAttributes) {
        if (hasOwn(componentAttributes, attribute) && hasOwn(componentAttributes[attribute], "value") && !target.hasAttribute(attribute)) {
          var value = componentAttributes[attribute].value;
          value = typeof value === "function" ? value(target) : value;
          target.setAttribute(attribute, value);
        }
      }
    }
  
    /**
     * Defines a property that proxies the specified attribute.
     *
     * @param {Element} target The web component element.
     * @param {String} attribute The attribute name to proxy.
     *
     * @returns {undefined}
     */
    function defineAttributeProperty(target, attribute, property) {
      Object.defineProperty(target, property, {
        get: function get() {
          return this.getAttribute(attribute);
        },
        set: function set(value) {
          if (value === undefined) {
            this.removeAttribute(attribute);
          } else {
            this.setAttribute(attribute, value);
          }
        }
      });
    }
  
    /**
     * Adds links from attributes to properties.
     *
     * @param {Element} target The web component element.
     * @param {Object} component The web component definition.
     *
     * @returns {undefined}
     */
    function addAttributeToPropertyLinks(target, component) {
      var componentAttributes = component.attributes;
  
      if ((typeof componentAttributes === "undefined" ? "undefined" : _typeof(componentAttributes)) !== "object") {
        return;
      }
  
      for (var attribute in componentAttributes) {
        var property = camelCase(attribute);
        if (hasOwn(componentAttributes, attribute) && !hasOwn(target, property)) {
          defineAttributeProperty(target, attribute, property);
        }
      }
    }
  
    function triggerAttributeChanged(target, component, data) {
      var callback;
      var type;
      var name = data.name;
      var newValue = data.newValue;
      var oldValue = data.oldValue;
      var newValueIsString = typeof newValue === "string";
      var oldValueIsString = typeof oldValue === "string";
      var attrs = component.attributes;
      var specific = attrs && attrs[name];
  
      if (!oldValueIsString && newValueIsString) {
        type = "created";
      } else if (oldValueIsString && newValueIsString) {
        type = "updated";
      } else if (oldValueIsString && !newValueIsString) {
        type = "removed";
      }
  
      if (specific && typeof specific[type] === "function") {
        callback = specific[type];
      } else if (specific && typeof specific.fallback === "function") {
        callback = specific.fallback;
      } else if (typeof specific === "function") {
        callback = specific;
      } else if (typeof attrs === "function") {
        callback = attrs;
      }
  
      // Ensure values are null if undefined.
      newValue = newValue === undefined ? null : newValue;
      oldValue = oldValue === undefined ? null : oldValue;
  
      // There may still not be a callback.
      if (callback) {
        callback(target, {
          type: type,
          name: name,
          newValue: newValue,
          oldValue: oldValue
        });
      }
    }
  
    function triggerAttributesCreated(target, component) {
      var a;
      var attrs = target.attributes;
      var attrsCopy = [];
      var attrsLen = attrs.length;
  
      for (a = 0; a < attrsLen; a++) {
        attrsCopy.push(attrs[a]);
      }
  
      // In default web components, attribute changes aren't triggered for
      // attributes that already exist on an element when it is bound. This sucks
      // when you want to reuse and separate code for attributes away from your
      // lifecycle callbacks. Skate will initialise each attribute by calling the
      // created callback for the attributes that already exist on the element.
      for (a = 0; a < attrsLen; a++) {
        var attr = attrsCopy[a];
        triggerAttributeChanged(target, component, {
          name: attr.nodeName,
          newValue: attr.value || attr.nodeValue
        });
      }
    }
  
    function addAttributeListeners(target, component) {
      var attrs = target.attributes;
  
      if (!component.attributes || registry.isNativeCustomElement(component.id)) {
        return;
      }
  
      var observer = new window.MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
          var name = mutation.attributeName;
          var attr = attrs[name];
  
          triggerAttributeChanged(target, component, {
            name: name,
            newValue: attr && (attr.value || attr.nodeValue),
            oldValue: mutation.oldValue
          });
        });
      });
  
      observer.observe(target, {
        attributes: true,
        attributeOldValue: true
      });
    }
  
    /**
     * Binds event listeners for the specified event handlers.
     *
     * @param {Element} target The component element.
     * @param {Object} component The component data.
     *
     * @returns {undefined}
     */
    function addEventListeners(target, component) {
      if (_typeof(component.events) !== "object") {
        return;
      }
  
      function makeHandler(handler, delegate) {
        return function (e) {
          // If we're not delegating, trigger directly on the component element.
          if (!delegate) {
            return handler(target, e, target);
          }
  
          // If we're delegating, but the target doesn't match, then we've have
          // to go up the tree until we find a matching ancestor or stop at the
          // component element, or document. If a matching ancestor is found, the
          // handler is triggered on it.
          var current = e.target;
  
          while (current && current !== document && current !== target.parentNode) {
            if (matchesSelector(current, delegate)) {
              return handler(target, e, current);
            }
  
            current = current.parentNode;
          }
        };
      }
  
      objEach(component.events, function (handler, name) {
        var evt = parseEvent(name);
        var useCapture = !!evt.delegate && (evt.name === "blur" || evt.name === "focus");
        target.addEventListener(evt.name, makeHandler(handler, evt.delegate), useCapture);
      });
    }
  
    /**
     * Triggers the created lifecycle callback.
     *
     * @param {Element} target The component element.
     * @param {Object} component The component data.
     *
     * @returns {undefined}
     */
    function triggerCreated(target, component) {
      var targetData = data(target, component.id);
  
      if (targetData.created) {
        return;
      }
  
      targetData.created = true;
  
      // TODO: This doesn't need to happen if using native.
      inherit(target, component.prototype, true);
  
      // We use the unresolved / resolved attributes to flag whether or not the
      // element has been templated or not.
      if (component.template && !target.hasAttribute(component.resolvedAttribute)) {
        component.template(target);
      }
  
      target.removeAttribute(component.unresolvedAttribute);
      target.setAttribute(component.resolvedAttribute, "");
      addEventListeners(target, component);
      addAttributeListeners(target, component);
      addAttributeToPropertyLinks(target, component);
      initAttributes(target, component);
      triggerAttributesCreated(target, component);
  
      if (component.created) {
        component.created(target);
      }
    }
  
    /**
     * Triggers the attached lifecycle callback.
     *
     * @param {Element} target The component element.
     * @param {Object} component The component data.
     *
     * @returns {undefined}
     */
    function triggerAttached(target, component) {
      var targetData = data(target, component.id);
  
      if (targetData.attached) {
        return;
      }
  
      if (!elementContains(document, target)) {
        return;
      }
  
      targetData.attached = true;
  
      if (component.attached) {
        component.attached(target);
      }
  
      targetData.detached = false;
    }
  
    /**
     * Triggers the detached lifecycle callback.
     *
     * @param {Element} target The component element.
     * @param {Object} component The component data.
     *
     * @returns {undefined}
     */
    function triggerDetached(target, component) {
      var targetData = data(target, component.id);
  
      if (targetData.detached) {
        return;
      }
  
      targetData.detached = true;
  
      if (component.detached) {
        component.detached(target);
      }
  
      targetData.attached = false;
    }
  
    /**
     * Triggers the entire element lifecycle if it's not being ignored.
     *
     * @param {Element} target The component element.
     * @param {Object} component The component data.
     *
     * @returns {undefined}
     */
    function triggerLifecycle(target, component) {
      triggerCreated(target, component);
      triggerAttached(target, component);
    }
  
    /**
     * Initialises a set of elements.
     *
     * @param {DOMNodeList | Array} elements A traversable set of elements.
     *
     * @returns {undefined}
     */
    function initElements(elements) {
      // [CATION] Don't cache elements length! Components initialization could append nodes
      // as siblings (see label's element behaviour for example) and this could lead to problems with
      // components placed at the end of processing childNodes because they will change they index
      // position and get out of cached value range.
      for (var a = 0; a < elements.length; a++) {
        var element = elements[a];
  
        if (element.nodeType !== Node.ELEMENT_NODE || element.attributes[ATTR_IGNORE]) {
          continue;
        }
  
        var currentNodeDefinitions = registry.getForElement(element);
        var currentNodeDefinitionsLength = currentNodeDefinitions.length;
  
        for (var b = 0; b < currentNodeDefinitionsLength; b++) {
          triggerLifecycle(element, currentNodeDefinitions[b]);
        }
  
        // When <object> tag is used to expose NPAPI api to js may have different behaviour then other
        // tags. One of those differences is that it's childNodes can be undefined.
        var elementChildNodes = element.childNodes || [];
        var elementChildNodesLen = elementChildNodes.length;
  
        if (elementChildNodesLen) {
          initElements(elementChildNodes);
        }
      }
    }
  
    /**
     * Triggers the remove lifecycle callback on all of the elements.
     *
     * @param {DOMNodeList} elements The elements to trigger the remove lifecycle
     * callback on.
     *
     * @returns {undefined}
     */
    function removeElements(elements) {
      // Don't cache `childNodes` length. For more info see description in `initElements` function.
      for (var a = 0; a < elements.length; a++) {
        var element = elements[a];
  
        if (element.nodeType !== Node.ELEMENT_NODE) {
          continue;
        }
  
        removeElements(element.childNodes);
  
        var definitions = registry.getForElement(element);
        var definitionsLen = definitions.length;
  
        for (var b = 0; b < definitionsLen; b++) {
          triggerDetached(element, definitions[b]);
        }
      }
    }
  
    exports.initElements = initElements;
    exports.removeElements = removeElements;
    exports.triggerAttached = triggerAttached;
    exports.triggerAttributeChanged = triggerAttributeChanged;
    exports.triggerCreated = triggerCreated;
    exports.triggerDetached = triggerDetached;
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/fix-ie-innerhtml.js
(typeof window === 'undefined' ? global : window).__28009422ccf8e1078fbf7e6e0ff766c7 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__28009422ccf8e1078fbf7e6e0ff766c7");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports"], factory);
    } else if (typeof exports !== "undefined") {
      factory(exports);
    }
  })(function (exports) {
  
    var isIeUntil10 = /MSIE/.test(navigator.userAgent);
    var isIe11 = /Trident/.test(navigator.userAgent);
    var isIe = isIeUntil10 || isIe11;
    var elementPrototype = window.HTMLElement.prototype;
  
    // ! This walkTree method differs from the implementation in ../../utils/walk-tree
    // It invokes the callback only for the children, not the passed node and the second parameter to the callback is the parent node
    function walkTree(node, cb) {
      var childNodes = node.childNodes;
  
      if (!childNodes) {
        return;
      }
  
      var childNodesLen = childNodes.length;
  
      for (var a = 0; a < childNodesLen; a++) {
        var childNode = childNodes[a];
        cb(childNode, node);
        walkTree(childNode, cb);
      }
    }
  
    function fixInnerHTML() {
      var originalInnerHTML = Object.getOwnPropertyDescriptor(elementPrototype, "innerHTML");
  
      var get = function get() {
        return originalInnerHTML.get.call(this);
      };
      get._hasBeenEnhanced = true;
  
      // This redefines the innerHTML property so that we can ensure that events
      // are properly triggered.
      Object.defineProperty(elementPrototype, "innerHTML", {
        get: get,
        set: function set(html) {
          walkTree(this, function (node, parentNode) {
            var mutationEvent = document.createEvent("MutationEvent");
            mutationEvent.initMutationEvent("DOMNodeRemoved", true, false, parentNode, null, null, null, null);
            node.dispatchEvent(mutationEvent);
          });
          originalInnerHTML.set.call(this, html);
        }
      });
    }
  
    if (isIe) {
      // IE 9-11
      var propertyDescriptor = Object.getOwnPropertyDescriptor(elementPrototype, "innerHTML");
      var hasBeenEnhanced = !!propertyDescriptor && propertyDescriptor.get._hasBeenEnhanced;
  
      if (!hasBeenEnhanced) {
        if (isIe11) {
          // IE11's native MutationObserver needs some help as well :()
          window.MutationObserver = window.JsMutationObserver || window.MutationObserver;
        }
  
        fixInnerHTML();
      }
    }
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/document-observer.js
(typeof window === 'undefined' ? global : window).__749d65f38d01d6608733ead63f4918cd = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports,
    "./globals": __c4a6b20f5235e3eec22c66f352171e2d,
    "./lifecycle": __48b29e539678e044870636b94df11349,
    "./mutation-observer": __bbc1d2b662a76620d0af1c87a6f18011,
    "./fix-ie-innerhtml": __28009422ccf8e1078fbf7e6e0ff766c7,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c,
    "./globals": __c4a6b20f5235e3eec22c66f352171e2d,
    "./lifecycle": __48b29e539678e044870636b94df11349,
    "./mutation-observer": __bbc1d2b662a76620d0af1c87a6f18011,
    "./fix-ie-innerhtml": __28009422ccf8e1078fbf7e6e0ff766c7,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__749d65f38d01d6608733ead63f4918cd");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "module", "./globals", "./lifecycle", "./mutation-observer", "./fix-ie-innerhtml", "./utils"], factory);
    } else if (typeof exports !== "undefined" && typeof module !== "undefined") {
      factory(exports, module, __c4a6b20f5235e3eec22c66f352171e2d, __48b29e539678e044870636b94df11349, __bbc1d2b662a76620d0af1c87a6f18011, __28009422ccf8e1078fbf7e6e0ff766c7, __4040a82bae2969ea3a1a790da8b2fc9c);
    }
  })(function (exports, module, _globals, _lifecycle, _mutationObserver, _fixIeInnerhtml, _utils) {
  
    var _interopRequire = function _interopRequire(obj) {
      return obj && obj.__esModule ? obj["default"] : obj;
    };
  
    var globals = _interopRequire(_globals);
  
    var initElements = _lifecycle.initElements;
    var removeElements = _lifecycle.removeElements;
    var getClosestIgnoredElement = _utils.getClosestIgnoredElement;
  
    /**
     * The document observer handler.
     *
     * @param {Array} mutations The mutations to handle.
     *
     * @returns {undefined}
     */
    function documentObserverHandler(mutations) {
      var mutationsLen = mutations.length;
  
      for (var a = 0; a < mutationsLen; a++) {
        var mutation = mutations[a];
        var addedNodes = mutation.addedNodes;
        var removedNodes = mutation.removedNodes;
  
        // Since siblings are batched together, we check the first node's parent
        // node to see if it is ignored. If it is then we don't process any added
        // nodes. This prevents having to check every node.
        if (addedNodes && addedNodes.length && !getClosestIgnoredElement(addedNodes[0].parentNode)) {
          initElements(addedNodes);
        }
  
        // We can't check batched nodes here because they won't have a parent node.
        if (removedNodes && removedNodes.length) {
          removeElements(removedNodes);
        }
      }
    }
  
    /**
     * Creates a new mutation observer for listening to Skate definitions for the
     * document.
     *
     * @param {Element} root The element to observe.
     *
     * @returns {MutationObserver}
     */
    function createDocumentObserver() {
      var observer = new window.MutationObserver(documentObserverHandler);
  
      // Observe after the DOM content has loaded.
      observer.observe(document, {
        childList: true,
        subtree: true
      });
  
      return observer;
    }
  
    module.exports = {
      register: function register(fixIe) {
        // IE has issues with reporting removedNodes correctly. See the polyfill for
        // details. If we fix IE, we must also re-define the document observer.
        if (fixIe) {
          this.unregister();
        }
  
        if (!globals.observer) {
          globals.observer = createDocumentObserver();
        }
  
        return this;
      },
  
      unregister: function unregister() {
        if (globals.observer) {
          globals.observer.disconnect();
          globals.observer = undefined;
        }
  
        return this;
      }
    };
  });
  
  return module.exports;
}).call(this);
// node_modules/skatejs/lib/skate.js
(typeof window === 'undefined' ? global : window).__b4ef81b87a2bc633ac16746f902d0fc3 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports,
    "./constants": __a7b0062609fe760fb31266997d736e07,
    "./document-observer": __749d65f38d01d6608733ead63f4918cd,
    "./lifecycle": __48b29e539678e044870636b94df11349,
    "./registry": __7730a833b569959f139e5fa88a9ba6f7,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c,
    "./version": __3f4bcf0b1224f1e2aec2d67adb81d7da,
    "./constants": __a7b0062609fe760fb31266997d736e07,
    "./document-observer": __749d65f38d01d6608733ead63f4918cd,
    "./lifecycle": __48b29e539678e044870636b94df11349,
    "./registry": __7730a833b569959f139e5fa88a9ba6f7,
    "./utils": __4040a82bae2969ea3a1a790da8b2fc9c,
    "./version": __3f4bcf0b1224f1e2aec2d67adb81d7da
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__b4ef81b87a2bc633ac16746f902d0fc3");
  define.amd = true;
  
  "use strict";
  
  (function (factory) {
    if (typeof define === "function" && define.amd) {
      define(["exports", "module", "./constants", "./document-observer", "./lifecycle", "./registry", "./utils", "./version"], factory);
    } else if (typeof exports !== "undefined" && typeof module !== "undefined") {
      factory(exports, module, __a7b0062609fe760fb31266997d736e07, __749d65f38d01d6608733ead63f4918cd, __48b29e539678e044870636b94df11349, __7730a833b569959f139e5fa88a9ba6f7, __4040a82bae2969ea3a1a790da8b2fc9c, __3f4bcf0b1224f1e2aec2d67adb81d7da);
    }
  })(function (exports, module, _constants, _documentObserver, _lifecycle, _registry, _utils, _version) {
  
    var _interopRequire = function _interopRequire(obj) {
      return obj && obj.__esModule ? obj["default"] : obj;
    };
  
    var TYPE_ATTRIBUTE = _constants.TYPE_ATTRIBUTE;
    var TYPE_CLASSNAME = _constants.TYPE_CLASSNAME;
    var TYPE_ELEMENT = _constants.TYPE_ELEMENT;
  
    var documentObserver = _interopRequire(_documentObserver);
  
    var triggerCreated = _lifecycle.triggerCreated;
    var triggerAttached = _lifecycle.triggerAttached;
    var triggerDetached = _lifecycle.triggerDetached;
    var triggerAttributeChanged = _lifecycle.triggerAttributeChanged;
    var initElements = _lifecycle.initElements;
  
    var registry = _interopRequire(_registry);
  
    var debounce = _utils.debounce;
    var inherit = _utils.inherit;
  
    var version = _interopRequire(_version);
  
    var HTMLElement = window.HTMLElement; //jshint ignore:line
  
    // IE <= 10 can fire "interactive" too early (#243).
    var isOldIE = !!document.attachEvent; // attachEvent was removed in IE11.
  
    function isReady() {
      if (isOldIE) {
        return document.readyState === "complete";
      } else {
        return document.readyState === "interactive" || document.readyState === "complete";
      }
    }
  
    /**
     * Initialises all valid elements in the document. Ensures that it does not
     * happen more than once in the same execution, and that it happens after the DOM is ready.
     *
     * @returns {undefined}
     */
    var initDocument = debounce(function () {
      var initialiseSkateElementsOnDomLoad = function initialiseSkateElementsOnDomLoad() {
        initElements(document.documentElement.childNodes);
      };
      if (isReady()) {
        initialiseSkateElementsOnDomLoad();
      } else {
        if (isOldIE) {
          window.addEventListener("load", initialiseSkateElementsOnDomLoad);
        } else {
          document.addEventListener("DOMContentLoaded", initialiseSkateElementsOnDomLoad);
        }
      }
    });
  
    /**
     * Creates a constructor for the specified definition.
     *
     * @param {Object} definition The definition information to use for generating the constructor.
     *
     * @returns {Function} The element constructor.
     */
    function makeElementConstructor(definition) {
      function CustomElement() {
        var element;
        var tagToExtend = definition["extends"];
        var definitionId = definition.id;
  
        if (tagToExtend) {
          element = document.createElement(tagToExtend);
          element.setAttribute("is", definitionId);
        } else {
          element = document.createElement(definitionId);
        }
  
        // Ensure the definition prototype is up to date with the element's
        // prototype. This ensures that overwriting the element prototype still
        // works.
        definition.prototype = CustomElement.prototype;
  
        // If they use the constructor we don't have to wait until it's attached.
        triggerCreated(element, definition);
  
        return element;
      }
  
      // This allows modifications to the element prototype propagate to the
      // definition prototype.
      CustomElement.prototype = definition.prototype;
  
      return CustomElement;
    }
  
    // Public API
    // ----------
  
    /**
     * Creates a listener for the specified definition.
     *
     * @param {String} id The ID of the definition.
     * @param {Object | Function} definition The definition definition.
     *
     * @returns {Function} Constructor that returns a custom element.
     */
    function skate(id, definition) {
      // Just in case the definition is shared, we duplicate it so that internal
      // modifications to the original aren't shared.
      definition = inherit({}, definition);
      definition = inherit(definition, skate.defaults);
      definition.id = id;
  
      registry.set(id, definition);
  
      if (registry.isNativeCustomElement(id)) {
        var elementPrototype = definition["extends"] ? document.createElement(definition["extends"]).constructor.prototype : HTMLElement.prototype;
  
        if (!elementPrototype.isPrototypeOf(definition.prototype)) {
          definition.prototype = inherit(Object.create(elementPrototype), definition.prototype, true);
        }
  
        var options = {
          prototype: inherit(definition.prototype, {
            createdCallback: function createdCallback() {
              triggerCreated(this, definition);
            },
            attachedCallback: function attachedCallback() {
              triggerAttached(this, definition);
            },
            detachedCallback: function detachedCallback() {
              triggerDetached(this, definition);
            },
            attributeChangedCallback: function attributeChangedCallback(name, oldValue, newValue) {
              triggerAttributeChanged(this, definition, {
                name: name,
                oldValue: oldValue,
                newValue: newValue
              });
            }
          })
        };
  
        if (definition["extends"]) {
          options["extends"] = definition["extends"];
        }
  
        return document.registerElement(id, options);
      }
  
      initDocument();
      documentObserver.register(!!definition.detached);
  
      if (registry.isType(id, TYPE_ELEMENT)) {
        return makeElementConstructor(definition);
      }
    }
  
    /**
     * Synchronously initialises the specified element or elements and descendants.
     *
     * @param {Mixed} nodes The node, or nodes to initialise. Can be anything:
     *                      jQuery, DOMNodeList, DOMNode, selector etc.
     *
     * @returns {skate}
     */
    skate.init = function (nodes) {
      var nodesToUse = nodes;
  
      if (!nodes) {
        return nodes;
      }
  
      if (typeof nodes === "string") {
        nodesToUse = nodes = document.querySelectorAll(nodes);
      } else if (nodes instanceof HTMLElement) {
        nodesToUse = [nodes];
      }
  
      initElements(nodesToUse);
  
      return nodes;
    };
  
    // Restriction type constants.
    skate.type = {
      ATTRIBUTE: TYPE_ATTRIBUTE,
      CLASSNAME: TYPE_CLASSNAME,
      ELEMENT: TYPE_ELEMENT
    };
  
    // Makes checking the version easy when debugging.
    skate.version = version;
  
    /**
     * The default options for a definition.
     *
     * @var {Object}
     */
    skate.defaults = {
      // Attribute lifecycle callback or callbacks.
      attributes: undefined,
  
      // The events to manage the binding and unbinding of during the definition's
      // lifecycle.
      events: undefined,
  
      // Restricts a particular definition to binding explicitly to an element with
      // a tag name that matches the specified value.
      "extends": undefined,
  
      // The ID of the definition. This is automatically set in the `skate()`
      // function.
      id: "",
  
      // Properties and methods to add to each element.
      prototype: {},
  
      // The attribute name to add after calling the created() callback.
      resolvedAttribute: "resolved",
  
      // The template to replace the content of the element with.
      template: undefined,
  
      // The type of bindings to allow.
      type: TYPE_ELEMENT,
  
      // The attribute name to remove after calling the created() callback.
      unresolvedAttribute: "unresolved"
    };
  
    // Exporting
    // ---------
  
    var previousSkate = window.skate;
    skate.noConflict = function () {
      window.skate = previousSkate;
      return skate;
    };
  
    // Global
    window.skate = skate;
  
    // ES6
    module.exports = skate;
  });
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/skate.js
(typeof window === 'undefined' ? global : window).__77840d725b87e770c115b18925208796 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  var _skatejs = __b4ef81b87a2bc633ac16746f902d0fc3;
  
  var _skatejs2 = _interopRequireDefault(_skatejs);
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  var auiSkate = _skatejs2.default.noConflict();
  
  exports.default = auiSkate;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/dropdown2.js
(typeof window === 'undefined' ? global : window).__fee2fb03e0889c96059239d94019b7c0 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  __686ed4df314a83846f5a3834d1c7497d;
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  var _skatejsTemplateHtml = __13ac5d5912c1c2250ae1f594e62da187;
  
  var _skatejsTemplateHtml2 = _interopRequireDefault(_skatejsTemplateHtml);
  
  var _deprecation = __2d336d5b3a38711b48236d5af8c39b51;
  
  var deprecate = _interopRequireWildcard(_deprecation);
  
  var _log = __3db7d70f11d6400b2fe84fc4d957c72d;
  
  var logger = _interopRequireWildcard(_log);
  
  var _debounce = __b6c2d68b946981b653c1247d4bef7c4c;
  
  var _browser = __8cdce9eb1fdffc13125d51ff33f17b88;
  
  var _alignment = __adf85a946ebc5f1648859d4c820fe0a2;
  
  var _alignment2 = _interopRequireDefault(_alignment);
  
  var _customEvent = __97354d72dc34929750a9514fb0aad090;
  
  var _customEvent2 = _interopRequireDefault(_customEvent);
  
  var _keyCode = __5e00bdc0656ab7ab1098c8394e0010a9;
  
  var _keyCode2 = _interopRequireDefault(_keyCode);
  
  var _layer = __fb6eb7339db5263fd7887254de970b3b;
  
  var _layer2 = _interopRequireDefault(_layer);
  
  var _state = __214d27a9777ad806977cd604056460e5;
  
  var _state2 = _interopRequireDefault(_state);
  
  var _skate = __77840d725b87e770c115b18925208796;
  
  var _skate2 = _interopRequireDefault(_skate);
  
  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function isChecked(el) {
      return (0, _jquery2.default)(el).is('.checked, .aui-dropdown2-checked, [aria-checked="true"]');
  }
  
  function getTrigger(control) {
      return (0, _jquery2.default)('[aria-controls="' + control.id + '"]')[0];
  }
  
  function doIfTrigger(triggerable, callback) {
      var trigger = getTrigger(triggerable);
  
      if (trigger) {
          callback(trigger);
      }
  }
  
  function setDropdownTriggerActiveState(trigger, isActive) {
      var $trigger = (0, _jquery2.default)(trigger);
  
      if (isActive) {
          $trigger.attr('aria-expanded', 'true');
          $trigger.addClass('active aui-dropdown2-active');
      } else {
          $trigger.attr('aria-expanded', 'false');
          $trigger.removeClass('active aui-dropdown2-active');
      }
  }
  
  // LOADING STATES
  var UNLOADED = 'unloaded';
  var LOADING = 'loading';
  var ERROR = 'error';
  var SUCCESS = 'success';
  
  // ASYNC DROPDOWN FUNCTIONS
  
  function makeAsyncDropdownContents(json) {
      var dropdownContents = json.map(function makeSection(sectionData) {
          var sectionItemsHtml = sectionData.items.map(function makeSectionItem(itemData) {
              function makeBooleanAttribute(attr) {
                  return itemData[attr] ? attr + ' ="true"' : '';
              }
  
              function makeAttribute(attr) {
                  return itemData[attr] ? attr + '="' + itemData[attr] + '"' : '';
              }
  
              var tagName = 'aui-item-' + itemData.type;
              var itemHtml = '\n                <' + tagName + ' ' + makeAttribute('for') + ' ' + makeAttribute('href') + ' ' + makeBooleanAttribute('interactive') + '\n                    ' + makeBooleanAttribute('checked') + ' ' + makeBooleanAttribute('disabled') + ' ' + makeBooleanAttribute('hidden') + '>\n                    ' + itemData.content + '\n                </' + tagName + '>';
  
              return itemHtml;
          }).join('');
  
          var sectionAttributes = sectionData.label ? 'label="' + sectionData.label + '"' : '';
          var sectionHtml = '\n            <aui-section ' + sectionAttributes + '>\n                ' + sectionItemsHtml + '\n            </aui-section>';
  
          return sectionHtml;
      }).join('\n');
  
      return dropdownContents;
  }
  
  function setDropdownContents(dropdown, json) {
      (0, _state2.default)(dropdown).set('loading-state', SUCCESS);
      _skatejsTemplateHtml2.default.wrap(dropdown).innerHTML = makeAsyncDropdownContents(json);
      _skate2.default.init(dropdown);
  }
  
  function setDropdownErrorState(dropdown) {
      (0, _state2.default)(dropdown).set('loading-state', ERROR);
      (0, _state2.default)(dropdown).set('hasErrorBeenShown', dropdown.isVisible());
      _skatejsTemplateHtml2.default.wrap(dropdown).innerHTML = '\n        <div class="aui-message aui-message-error aui-dropdown-error">\n            <p>' + AJS.I18n.getText('aui.dropdown.async.error') + '</p>\n        </div>\n    ';
  }
  
  function setDropdownLoadingState(dropdown) {
      (0, _state2.default)(dropdown).set('loading-state', LOADING);
      (0, _state2.default)(dropdown).set('hasErrorBeenShown', false);
  
      doIfTrigger(dropdown, function (trigger) {
          trigger.setAttribute('aria-busy', 'true');
      });
  
      _skatejsTemplateHtml2.default.wrap(dropdown).innerHTML = '\n        <div class="aui-dropdown-loading">\n            <span class="spinner"></span> ' + AJS.I18n.getText('aui.dropdown.async.loading') + '\n        </div>\n    ';
      (0, _jquery2.default)(dropdown).find('.spinner').spin();
  }
  
  function setDropdownLoaded(dropdown) {
      doIfTrigger(dropdown, function (trigger) {
          trigger.setAttribute('aria-busy', 'false');
      });
  }
  
  function loadContentsIfAsync(dropdown) {
      if (!dropdown.src || (0, _state2.default)(dropdown).get('loading-state') === LOADING) {
          return;
      }
  
      setDropdownLoadingState(dropdown);
  
      _jquery2.default.ajax(dropdown.src).done(function (json, status, xhr) {
          var isValidStatus = xhr.status === 200;
          if (isValidStatus) {
              setDropdownContents(dropdown, json);
          } else {
              setDropdownErrorState(dropdown);
          }
      }).fail(function () {
          setDropdownErrorState(dropdown);
      }).always(function () {
          setDropdownLoaded(dropdown);
      });
  }
  
  function loadContentWhenMouseEnterTrigger(dropdown) {
      var isDropdownUnloaded = (0, _state2.default)(dropdown).get('loading-state') === UNLOADED;
      var hasCurrentErrorBeenShown = (0, _state2.default)(dropdown).get('hasErrorBeenShown');
      if (isDropdownUnloaded || hasCurrentErrorBeenShown && !dropdown.isVisible()) {
          loadContentsIfAsync(dropdown);
      }
  }
  
  function loadContentWhenMenuShown(dropdown) {
      var isDropdownUnloaded = (0, _state2.default)(dropdown).get('loading-state') === UNLOADED;
      var hasCurrentErrorBeenShown = (0, _state2.default)(dropdown).get('hasErrorBeenShown');
      if (isDropdownUnloaded || hasCurrentErrorBeenShown) {
          loadContentsIfAsync(dropdown);
      }
      if ((0, _state2.default)(dropdown).get('loading-state') === ERROR) {
          (0, _state2.default)(dropdown).set('hasErrorBeenShown', true);
      }
  }
  
  // The dropdown's trigger
  // ----------------------
  
  function triggerCreated(trigger) {
      var dropdownID = trigger.getAttribute('aria-controls');
  
      if (!dropdownID) {
          dropdownID = trigger.getAttribute('aria-owns');
  
          if (!dropdownID) {
              logger.error('Dropdown triggers need either a "aria-owns" or "aria-controls" attribute');
          } else {
              trigger.removeAttribute('aria-owns');
              trigger.setAttribute('aria-controls', dropdownID);
          }
      }
  
      trigger.setAttribute('aria-haspopup', true);
      trigger.setAttribute('aria-expanded', false);
      trigger.setAttribute('href', '#');
  
      function handleIt(e) {
          e.preventDefault();
  
          if (!trigger.isEnabled()) {
              return;
          }
  
          var dropdown = document.getElementById(dropdownID);
          dropdown.toggle();
          dropdown.isSubmenu = trigger.hasSubmenu();
  
          return dropdown;
      }
  
      function handleMouseEnter(e) {
          e.preventDefault();
  
          if (!trigger.isEnabled()) {
              return;
          }
  
          var dropdown = document.getElementById(dropdownID);
          loadContentWhenMouseEnterTrigger(dropdown);
  
          if (trigger.hasSubmenu()) {
              dropdown.show();
              dropdown.isSubmenu = trigger.hasSubmenu();
          }
  
          return dropdown;
      }
  
      function handleKeydown(e) {
          var normalInvoke = e.keyCode === _keyCode2.default.ENTER || e.keyCode === _keyCode2.default.SPACE;
          var submenuInvoke = e.keyCode === _keyCode2.default.RIGHT && trigger.hasSubmenu();
          var rootMenuInvoke = (e.keyCode === _keyCode2.default.UP || e.keyCode === _keyCode2.default.DOWN) && !trigger.hasSubmenu();
  
          if (normalInvoke || submenuInvoke || rootMenuInvoke) {
              var dropdown = handleIt(e);
  
              if (dropdown) {
                  dropdown.focusItem(0);
              }
          }
      }
  
      (0, _jquery2.default)(trigger).on('aui-button-invoke', handleIt).on('click', handleIt).on('keydown', handleKeydown).on('mouseenter', handleMouseEnter);
  }
  
  var triggerPrototype = {
      disable: function disable() {
          this.setAttribute('aria-disabled', 'true');
      },
  
      enable: function enable() {
          this.setAttribute('aria-disabled', 'false');
      },
  
      isEnabled: function isEnabled() {
          return this.getAttribute('aria-disabled') !== 'true';
      },
  
      hasSubmenu: function hasSubmenu() {
          var triggerClasses = (this.className || '').split(/\s+/);
          return triggerClasses.indexOf('aui-dropdown2-sub-trigger') !== -1;
      }
  };
  
  (0, _skate2.default)('aui-dropdown2-trigger', {
      type: _skate2.default.type.CLASSNAME,
      created: triggerCreated,
      prototype: triggerPrototype
  });
  
  //To remove at a later date. Some dropdown triggers initialise lazily, so we need to listen for mousedown
  //and synchronously init before the click event is fired.
  //TODO: delete in AUI 6.0.0, see AUI-2868
  function bindLazyTriggerInitialisation() {
      (0, _jquery2.default)(document).on('mousedown', '.aui-dropdown2-trigger', function () {
          var isElementSkated = this.hasAttribute('resolved');
          if (!isElementSkated) {
              _skate2.default.init(this);
              var lazyDeprecate = deprecate.getMessageLogger('Dropdown2 lazy initialisation', {
                  removeInVersion: '6.0.0',
                  alternativeName: 'initialisation on DOM insertion',
                  sinceVersion: '5.8.0',
                  extraInfo: 'Dropdown2 triggers should have all necessary attributes on DOM insertion',
                  deprecationType: 'JS'
              });
              lazyDeprecate();
          }
      });
  }
  
  bindLazyTriggerInitialisation();
  
  (0, _skate2.default)('aui-dropdown2-sub-trigger', {
      type: _skate2.default.type.CLASSNAME,
      created: function created(trigger) {
          trigger.className += ' aui-dropdown2-trigger';
          _skate2.default.init(trigger);
      }
  });
  
  // Dropdown trigger groups
  // -----------------------
  
  (0, _jquery2.default)(document).on('mouseenter', '.aui-dropdown2-trigger-group a, .aui-dropdown2-trigger-group button', function (e) {
      var $item = (0, _jquery2.default)(e.currentTarget);
  
      if ($item.is('.aui-dropdown2-active')) {
          return; // No point doing anything if we're hovering over the already-active item trigger.
      }
  
      if ($item.closest('.aui-dropdown2').size()) {
          return; // We don't want to deal with dropdown items, just the potential triggers in the group.
      }
  
      var $triggerGroup = $item.closest('.aui-dropdown2-trigger-group');
  
      var $groupActiveTrigger = $triggerGroup.find('.aui-dropdown2-active');
      if ($groupActiveTrigger.size() && $item.is('.aui-dropdown2-trigger')) {
          $groupActiveTrigger.blur(); // Remove focus from the previously opened menu.
          $item.trigger('aui-button-invoke'); // Open this trigger's menu.
          e.preventDefault();
      }
  
      var $groupFocusedTrigger = $triggerGroup.find(':focus');
      if ($groupFocusedTrigger.size() && $item.is('.aui-dropdown2-trigger')) {
          $groupFocusedTrigger.blur();
      }
  });
  
  // Dropdown items
  // --------------
  
  function getDropdownItems(dropdown, filter) {
      return (0, _jquery2.default)(dropdown).find([
      // Legacy markup.
      '> ul > li', '> .aui-dropdown2-section > ul > li',
      // Accessible markup.
      '> div[role] > .aui-dropdown2-section > div[role="group"] > ul[role] > li[role]',
      // Web component.
      'aui-item-link', 'aui-item-checkbox', 'aui-item-radio'].join(', ')).filter(filter).children('a, button, [role="checkbox"], [role="menuitemcheckbox"], [role="radio"], [role="menuitemradio"]');
  }
  
  function getAllDropdownItems(dropdown) {
      return getDropdownItems(dropdown, function () {
          return true;
      });
  }
  
  function getVisibleDropdownItems(dropdown) {
      return getDropdownItems(dropdown, function () {
          return this.className.indexOf('hidden') === -1 && !this.hasAttribute('hidden');
      });
  }
  
  function amendDropdownItem(item) {
      var $item = (0, _jquery2.default)(item);
  
      $item.attr('tabindex', '-1');
  
      /**
       * Honouring the documentation.
       * @link https://docs.atlassian.com/aui/latest/docs/dropdown2.html
       */
      if ($item.hasClass('aui-dropdown2-disabled') || $item.parent().hasClass('aui-dropdown2-hidden')) {
          $item.attr('aria-disabled', true);
      }
  }
  
  function amendDropdownContent(dropdown) {
      // Add assistive semantics to each dropdown item
      getAllDropdownItems(dropdown).each(function () {
          amendDropdownItem(this);
      });
  }
  
  /**
   * Honours behaviour for code written using only the legacy class names.
   * To maintain old behaviour (i.e., remove the 'hidden' class and the item will become un-hidden)
   * whilst allowing our code to only depend on the new classes, we need to
   * keep the state of the DOM in sync with legacy classes.
   *
   * Calling this function will add the new namespaced classes to elements with legacy names.
   * @returns {Function} a function to remove the new namespaced classes, only from the elements they were added to.
   */
  function migrateAndSyncLegacyClassNames(dropdown) {
      var $dropdown = (0, _jquery2.default)(dropdown);
  
      // Migrate away from legacy class names
      var $hiddens = $dropdown.find('.hidden').addClass('aui-dropdown2-hidden');
      var $disableds = $dropdown.find('.disabled').addClass('aui-dropdown2-disabled');
      var $interactives = $dropdown.find('.interactive').addClass('aui-dropdown2-interactive');
  
      return function revertToOriginalMarkup() {
          $hiddens.removeClass('aui-dropdown2-hidden');
          $disableds.removeClass('aui-dropdown2-disabled');
          $interactives.removeClass('aui-dropdown2-interactive');
      };
  }
  
  // The Dropdown itself
  // -------------------
  
  function setLayerAlignment(dropdown, trigger) {
      var hasSubmenu = trigger && trigger.hasSubmenu && trigger.hasSubmenu();
      var hasSubmenuAlignment = dropdown.getAttribute('data-aui-alignment') === 'submenu auto';
  
      if (!hasSubmenu && hasSubmenuAlignment) {
          restorePreviousAlignment(dropdown);
      }
      var hasAnyAlignment = dropdown.hasAttribute('data-aui-alignment');
  
      if (hasSubmenu && !hasSubmenuAlignment) {
          saveCurrentAlignment(dropdown);
          dropdown.setAttribute('data-aui-alignment', 'submenu auto');
          dropdown.setAttribute('data-aui-alignment-static', true);
      } else if (!hasAnyAlignment) {
          dropdown.setAttribute('data-aui-alignment', 'bottom auto');
          dropdown.setAttribute('data-aui-alignment-static', true);
      }
  
      if (dropdown._auiAlignment) {
          dropdown._auiAlignment.destroy();
      }
  
      dropdown._auiAlignment = new _alignment2.default(dropdown, trigger);
  
      dropdown._auiAlignment.enable();
  }
  
  function saveCurrentAlignment(dropdown) {
      var $dropdown = (0, _jquery2.default)(dropdown);
      if (dropdown.hasAttribute('data-aui-alignment')) {
          $dropdown.data('previous-data-aui-alignment', dropdown.getAttribute('data-aui-alignment'));
      }
      $dropdown.data('had-data-aui-alignment-static', dropdown.hasAttribute('data-aui-alignment-static'));
  }
  
  function restorePreviousAlignment(dropdown) {
      var $dropdown = (0, _jquery2.default)(dropdown);
  
      var previousAlignment = $dropdown.data('previous-data-aui-alignment');
      if (previousAlignment) {
          dropdown.setAttribute('data-aui-alignment', previousAlignment);
      } else {
          dropdown.removeAttribute('data-aui-alignment');
      }
      $dropdown.removeData('previous-data-aui-alignment');
  
      if (!$dropdown.data('had-data-aui-alignment-static')) {
          dropdown.removeAttribute('data-aui-alignment-static');
      }
      $dropdown.removeData('had-data-aui-alignment-static');
  }
  
  function getDropdownHideLocation(dropdown, trigger) {
      var possibleHome = trigger.getAttribute('data-dropdown2-hide-location');
      return document.getElementById(possibleHome) || dropdown.parentNode;
  }
  
  var keyboardClose = false;
  function keyboardCloseDetected() {
      keyboardClose = true;
  }
  
  function wasProbablyClosedViaKeyboard() {
      var result = keyboardClose === true;
      keyboardClose = false;
      return result;
  }
  
  function bindDropdownBehaviourToLayer(dropdown) {
      (0, _layer2.default)(dropdown);
  
      dropdown.addEventListener('aui-layer-show', function () {
          (0, _jquery2.default)(dropdown).trigger('aui-dropdown2-show');
  
          dropdown._syncClasses = migrateAndSyncLegacyClassNames(dropdown);
  
          amendDropdownContent(this);
  
          doIfTrigger(dropdown, function (trigger) {
              setDropdownTriggerActiveState(trigger, true);
              dropdown._returnTo = getDropdownHideLocation(dropdown, trigger);
          });
      });
  
      dropdown.addEventListener('aui-layer-hide', function () {
          (0, _jquery2.default)(dropdown).trigger('aui-dropdown2-hide');
  
          if (dropdown._syncClasses) {
              dropdown._syncClasses();
              delete dropdown._syncClasses;
          }
  
          if (dropdown._auiAlignment) {
              dropdown._auiAlignment.disable();
              dropdown._auiAlignment.destroy();
          }
  
          if (dropdown._returnTo) {
              if (dropdown.parentNode && dropdown.parentNode !== dropdown._returnTo) {
                  dropdown.parentNode.removeChild(dropdown);
              }
              dropdown._returnTo.appendChild(dropdown);
          }
  
          getVisibleDropdownItems(dropdown).removeClass('active aui-dropdown2-active');
  
          doIfTrigger(dropdown, function (trigger) {
              if (wasProbablyClosedViaKeyboard()) {
                  trigger.focus();
                  setDropdownTriggerActiveState(trigger, trigger.hasSubmenu && trigger.hasSubmenu());
              } else {
                  setDropdownTriggerActiveState(trigger, false);
              }
          });
  
          // Gets set by submenu trigger invocation. Bad coupling point?
          delete dropdown.isSubmenu;
      });
  }
  
  function bindItemInteractionBehaviourToDropdown(dropdown) {
      var $dropdown = (0, _jquery2.default)(dropdown);
  
      $dropdown.on('keydown', function (e) {
          if (e.keyCode === _keyCode2.default.DOWN) {
              dropdown.focusNext();
              e.preventDefault();
          } else if (e.keyCode === _keyCode2.default.UP) {
              dropdown.focusPrevious();
              e.preventDefault();
          } else if (e.keyCode === _keyCode2.default.LEFT) {
              if (dropdown.isSubmenu) {
                  keyboardCloseDetected();
                  dropdown.hide();
                  e.preventDefault();
              }
          } else if (e.keyCode === _keyCode2.default.ESCAPE) {
              // The closing will be handled by the LayerManager!
              keyboardCloseDetected();
          } else if (e.keyCode === _keyCode2.default.TAB) {
              keyboardCloseDetected();
              dropdown.hide();
          }
      });
  
      // close the menu when activating elements which aren't "interactive"
      $dropdown.on('click keydown', 'a, button, [role="menuitem"], [role="menuitemcheckbox"], [role="checkbox"], [role="menuitemradio"], [role="radio"]', function (e) {
          if (e.type === 'click' || e.keyCode === _keyCode2.default.ENTER || e.keyCode === _keyCode2.default.SPACE) {
              var $item = (0, _jquery2.default)(e.currentTarget);
  
              if ($item.attr('aria-disabled') === 'true') {
                  e.preventDefault();
              }
  
              if (!e.isDefaultPrevented() && !$item.is('.aui-dropdown2-interactive')) {
                  var theMenu = dropdown;
                  do {
                      var dd = (0, _layer2.default)(theMenu);
                      theMenu = (0, _layer2.default)(theMenu).below();
                      if (dd.$el.is('.aui-dropdown2')) {
                          dd.hide();
                      }
                  } while (theMenu);
              }
          }
      });
  
      // close a submenus when the mouse moves over items other than its trigger
      $dropdown.on('mouseenter', 'a, button, [role="menuitem"], [role="menuitemcheckbox"], [role="checkbox"], [role="menuitemradio"], [role="radio"]', function (e) {
          var item = e.currentTarget;
          var hasSubmenu = item.hasSubmenu && item.hasSubmenu();
  
          if (!e.isDefaultPrevented() && !hasSubmenu) {
              var maybeALayer = (0, _layer2.default)(dropdown).above();
  
              if (maybeALayer) {
                  (0, _layer2.default)(maybeALayer).hide();
              }
          }
      });
  }
  
  (0, _jquery2.default)(window).on('resize', (0, _debounce.debounceImmediate)(function () {
      (0, _jquery2.default)('.aui-dropdown2').each(function (index, dropdown) {
          _skate2.default.init(dropdown);
          if (dropdown.isVisible()) {
              dropdown.hide();
          }
      });
  }, 1000));
  
  // Dropdowns
  // ---------
  
  function dropdownCreated(dropdown) {
      var $dropdown = (0, _jquery2.default)(dropdown);
  
      $dropdown.addClass('aui-dropdown2');
  
      // swap the inner div to presentation as application is only needed for Windows
      if ((0, _browser.supportsVoiceOver)()) {
          $dropdown.find('> div[role="application"]').attr('role', 'presentation');
      }
  
      if (dropdown.hasAttribute('data-container')) {
          $dropdown.attr('data-aui-alignment-container', $dropdown.attr('data-container'));
          $dropdown.removeAttr('data-container');
      }
  
      bindDropdownBehaviourToLayer(dropdown);
      bindItemInteractionBehaviourToDropdown(dropdown);
      dropdown.hide();
  
      (0, _jquery2.default)(dropdown).delegate('.aui-dropdown2-checkbox:not(.disabled):not(.aui-dropdown2-disabled)', 'click keydown', function (e) {
          if (e.type === 'click' || e.keyCode === _keyCode2.default.ENTER || e.keyCode === _keyCode2.default.SPACE) {
              var checkbox = this;
              if (e.isDefaultPrevented()) {
                  return;
              }
              if (checkbox.isInteractive()) {
                  e.preventDefault();
              }
              if (checkbox.isEnabled()) {
                  // toggle the checked state
                  if (checkbox.isChecked()) {
                      checkbox.uncheck();
                  } else {
                      checkbox.check();
                  }
              }
          }
      });
  
      (0, _jquery2.default)(dropdown).delegate('.aui-dropdown2-radio:not(.checked):not(.aui-dropdown2-checked):not(.disabled):not(.aui-dropdown2-disabled)', 'click keydown', function (e) {
          if (e.type === 'click' || e.keyCode === _keyCode2.default.ENTER || e.keyCode === _keyCode2.default.SPACE) {
              var radio = this;
              if (e.isDefaultPrevented()) {
                  return;
              }
              if (radio.isInteractive()) {
                  e.preventDefault();
              }
  
              var $radio = (0, _jquery2.default)(this);
              if (this.isEnabled() && this.isChecked() === false) {
                  // toggle the checked state
                  $radio.closest('ul,[role=group]').find('.aui-dropdown2-checked').not(this).each(function () {
                      this.uncheck();
                  });
                  radio.check();
              }
          }
      });
  }
  
  var dropdownPrototype = {
      /**
       * Toggles the visibility of the dropdown menu
       */
      toggle: function toggle() {
          if (this.isVisible()) {
              this.hide();
          } else {
              this.show();
          }
      },
  
      /**
       * Explicitly shows the menu
       *
       * @returns {HTMLElement}
       */
      show: function show() {
          (0, _layer2.default)(this).show();
  
          var dropdown = this;
          doIfTrigger(dropdown, function (trigger) {
              setLayerAlignment(dropdown, trigger);
          });
  
          return this;
      },
  
      /**
       * Explicitly hides the menu
       *
       * @returns {HTMLElement}
       */
      hide: function hide() {
          (0, _layer2.default)(this).hide();
          return this;
      },
  
      /**
       * Shifts explicit focus to the next available item in the menu
       *
       * @returns {undefined}
       */
      focusNext: function focusNext() {
          var $items = getVisibleDropdownItems(this);
          var selected = document.activeElement;
          var idx;
  
          if ($items.last()[0] !== selected) {
              idx = $items.toArray().indexOf(selected);
              this.focusItem($items.get(idx + 1));
          }
      },
  
      /**
       * Shifts explicit focus to the previous available item in the menu
       *
       * @returns {undefined}
       */
      focusPrevious: function focusPrevious() {
          var $items = getVisibleDropdownItems(this);
          var selected = document.activeElement;
          var idx;
  
          if ($items.first()[0] !== selected) {
              idx = $items.toArray().indexOf(selected);
              this.focusItem($items.get(idx - 1));
          }
      },
  
      /**
       * Shifts explicit focus to the menu item matching the index param
       */
      focusItem: function focusItem(item) {
          var $items = getVisibleDropdownItems(this);
          var $item;
          if (typeof item === 'number') {
              item = $items.get(item);
          }
          $item = (0, _jquery2.default)(item);
          $item.focus();
          $items.removeClass('active aui-dropdown2-active');
          $item.addClass('active aui-dropdown2-active');
      },
  
      /**
       * Checks whether or not the menu is currently displayed
       *
       * @returns {Boolean}
       */
      isVisible: function isVisible() {
          return (0, _layer2.default)(this).isVisible();
      }
  };
  
  // Web component API for dropdowns
  // -------------------------------
  
  var disabledAttributeHandler = {
      created: function created(element) {
          var a = element.children[0];
          a.setAttribute('aria-disabled', 'true');
          a.className += ' aui-dropdown2-disabled';
      },
      removed: function removed(element) {
          var a = element.children[0];
          a.setAttribute('aria-disabled', 'false');
          (0, _jquery2.default)(a).removeClass('aui-dropdown2-disabled');
      }
  };
  
  var interactiveAttributeHandler = {
      created: function created(element) {
          var a = element.children[0];
          a.className += ' aui-dropdown2-interactive';
      },
      removed: function removed(element) {
          var a = element.children[0];
          (0, _jquery2.default)(a).removeClass('aui-dropdown2-interactive');
      }
  };
  
  var checkedAttributeHandler = {
      created: function created(element) {
          var a = element.children[0];
          (0, _jquery2.default)(a).addClass('checked aui-dropdown2-checked');
          a.setAttribute('aria-checked', true);
          element.dispatchEvent(new _customEvent2.default('change', { bubbles: true }));
      },
      removed: function removed(element) {
          var a = element.children[0];
          (0, _jquery2.default)(a).removeClass('checked aui-dropdown2-checked');
          a.setAttribute('aria-checked', false);
          element.dispatchEvent(new _customEvent2.default('change', { bubbles: true }));
      }
  };
  
  var hiddenAttributeHandler = {
      created: function created(element) {
          disabledAttributeHandler.created(element);
      },
      removed: function removed(element) {
          disabledAttributeHandler.removed(element);
      }
  };
  
  (0, _skate2.default)('aui-item-link', {
      template: (0, _skatejsTemplateHtml2.default)('<a role="menuitem" tabindex="-1"><content></content></a>'),
      attributes: {
          disabled: disabledAttributeHandler,
          interactive: interactiveAttributeHandler,
          hidden: hiddenAttributeHandler,
          href: {
              created: function created(element, change) {
                  var a = element.children[0];
                  a.setAttribute('href', change.newValue);
              },
              updated: function updated(element, change) {
                  var a = element.children[0];
                  a.setAttribute('href', change.newValue);
              },
              removed: function removed(element) {
                  var a = element.children[0];
                  a.removeAttribute('href');
              }
          },
          for: {
              created: function created(element) {
                  var anchor = element.children[0];
                  anchor.setAttribute('aria-controls', element.getAttribute('for'));
                  (0, _jquery2.default)(anchor).addClass('aui-dropdown2-sub-trigger');
              },
              updated: function updated(element) {
                  var anchor = element.children[0];
                  anchor.setAttribute('aria-controls', element.getAttribute('for'));
              },
              removed: function removed(element) {
                  var anchor = element.children[0];
                  anchor.removeAttribute('aria-controls');
                  (0, _jquery2.default)(anchor).removeClass('aui-dropdown2-sub-trigger');
              }
          }
      }
  });
  
  (0, _skate2.default)('aui-item-checkbox', {
      template: (0, _skatejsTemplateHtml2.default)('<span role="checkbox" class="aui-dropdown2-checkbox" tabindex="-1"><content></content></span>'),
      attributes: {
          disabled: disabledAttributeHandler,
          interactive: interactiveAttributeHandler,
          checked: checkedAttributeHandler,
          hidden: hiddenAttributeHandler
      }
  });
  
  (0, _skate2.default)('aui-item-radio', {
      template: (0, _skatejsTemplateHtml2.default)('<span role="radio" class="aui-dropdown2-radio" tabindex="-1"><content></content></span>'),
      attributes: {
          disabled: disabledAttributeHandler,
          interactive: interactiveAttributeHandler,
          checked: checkedAttributeHandler,
          hidden: hiddenAttributeHandler
      }
  });
  
  (0, _skate2.default)('aui-section', {
      template: (0, _skatejsTemplateHtml2.default)('\n        <strong aria-role="presentation" class="aui-dropdown2-heading"></strong>\n        <div role="group">\n            <content></content>\n        </div>\n    '),
      attributes: {
          label: function label(element, data) {
              var headingElement = element.children[0];
              var groupElement = element.children[1];
              headingElement.textContent = data.newValue;
              groupElement.setAttribute('aria-label', data.newValue);
          }
      },
      created: function created(element) {
          element.className += ' aui-dropdown2-section';
          element.setAttribute('role', 'presentation');
      }
  });
  
  (0, _skate2.default)('aui-dropdown-menu', {
      template: (0, _skatejsTemplateHtml2.default)('\n        <div role="application">\n            <content></content>\n        </div>\n    '),
      created: function created(dropdown) {
          dropdown.setAttribute('role', 'menu');
          dropdown.className = 'aui-dropdown2 aui-style-default aui-layer';
          (0, _state2.default)(dropdown).set('loading-state', UNLOADED);
          // Now skate the .aui-dropdown2 behaviour.
          _skate2.default.init(dropdown);
      },
      attributes: {
          src: {}
      },
      prototype: dropdownPrototype,
      events: {
          'aui-layer-show': loadContentWhenMenuShown
      }
  });
  
  // Legacy dropdown inits
  // ---------------------
  
  (0, _skate2.default)('aui-dropdown2', {
      type: _skate2.default.type.CLASSNAME,
      created: dropdownCreated,
      prototype: dropdownPrototype
  });
  
  (0, _skate2.default)('data-aui-dropdown2', {
      type: _skate2.default.type.ATTRIBUTE,
      created: dropdownCreated,
      prototype: dropdownPrototype
  });
  
  // Checkboxes and radios
  // ---------------------
  
  (0, _skate2.default)('aui-dropdown2-checkbox', {
      type: _skate2.default.type.CLASSNAME,
  
      created: function created(checkbox) {
          var checked = isChecked(checkbox);
          if (checked) {
              (0, _jquery2.default)(checkbox).addClass('checked aui-dropdown2-checked');
          }
          checkbox.setAttribute('aria-checked', checked);
          checkbox.setAttribute('tabindex', '0');
  
          // swap from menuitemcheckbox to just plain checkbox for VoiceOver
          if ((0, _browser.supportsVoiceOver)()) {
              checkbox.setAttribute('role', 'checkbox');
          }
      },
  
      prototype: {
          isEnabled: function isEnabled() {
              return !(this.getAttribute('aria-disabled') !== null && this.getAttribute('aria-disabled') === 'true');
          },
  
          isChecked: function isChecked() {
              return this.getAttribute('aria-checked') !== null && this.getAttribute('aria-checked') === 'true';
          },
  
          isInteractive: function isInteractive() {
              return (0, _jquery2.default)(this).hasClass('aui-dropdown2-interactive');
          },
  
          uncheck: function uncheck() {
              if (this.parentNode.tagName.toLowerCase() === 'aui-item-checkbox') {
                  this.parentNode.removeAttribute('checked');
              }
              this.setAttribute('aria-checked', 'false');
              (0, _jquery2.default)(this).removeClass('checked aui-dropdown2-checked');
              (0, _jquery2.default)(this).trigger('aui-dropdown2-item-uncheck');
          },
  
          check: function check() {
              if (this.parentNode.tagName.toLowerCase() === 'aui-item-checkbox') {
                  this.parentNode.setAttribute('checked', '');
              }
              this.setAttribute('aria-checked', 'true');
              (0, _jquery2.default)(this).addClass('checked aui-dropdown2-checked');
              (0, _jquery2.default)(this).trigger('aui-dropdown2-item-check');
          }
      }
  });
  
  (0, _skate2.default)('aui-dropdown2-radio', {
      type: _skate2.default.type.CLASSNAME,
  
      created: function created(radio) {
          // add a dash of ARIA
          var checked = isChecked(radio);
          if (checked) {
              (0, _jquery2.default)(radio).addClass('checked aui-dropdown2-checked');
          }
          radio.setAttribute('aria-checked', checked);
          radio.setAttribute('tabindex', '0');
  
          // swap from menuitemradio to just plain radio for VoiceOver
          if ((0, _browser.supportsVoiceOver)()) {
              radio.setAttribute('role', 'radio');
          }
      },
  
      prototype: {
          isEnabled: function isEnabled() {
              return !(this.getAttribute('aria-disabled') !== null && this.getAttribute('aria-disabled') === 'true');
          },
  
          isChecked: function isChecked() {
              return this.getAttribute('aria-checked') !== null && this.getAttribute('aria-checked') === 'true';
          },
  
          isInteractive: function isInteractive() {
              return (0, _jquery2.default)(this).hasClass('aui-dropdown2-interactive');
          },
  
          uncheck: function uncheck() {
              if (this.parentNode.tagName.toLowerCase() === 'aui-item-radio') {
                  this.parentNode.removeAttribute('checked');
              }
              this.setAttribute('aria-checked', 'false');
              (0, _jquery2.default)(this).removeClass('checked aui-dropdown2-checked');
              (0, _jquery2.default)(this).trigger('aui-dropdown2-item-uncheck');
          },
  
          check: function check() {
              if (this.parentNode.tagName.toLowerCase() === 'aui-item-radio') {
                  this.parentNode.setAttribute('checked', '');
              }
              this.setAttribute('aria-checked', 'true');
              (0, _jquery2.default)(this).addClass('checked aui-dropdown2-checked');
              (0, _jquery2.default)(this).trigger('aui-dropdown2-item-check');
          }
      }
  });
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js-vendor/jquery/plugins/jquery.select2.js
(typeof window === 'undefined' ? global : window).__cb40cd044a911820c6668078d4113d99 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  /*
   Copyright 2012 Igor Vaynberg
  
   Version: 3.4.5 Timestamp: Mon Nov  4 08:22:42 PST 2013
  
   This software is licensed under the Apache License, Version 2.0 (the "Apache License") or the GNU
   General Public License version 2 (the "GPL License"). You may choose either license to govern your
   use of this software only upon the condition that you accept all of the terms of either the Apache
   License or the GPL License.
  
   You may obtain a copy of the Apache License and the GPL License at:
  
   http://www.apache.org/licenses/LICENSE-2.0
   http://www.gnu.org/licenses/gpl-2.0.html
  
   Unless required by applicable law or agreed to in writing, software distributed under the
   Apache License or the GPL Licesnse is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied. See the Apache License and the GPL License for
   the specific language governing permissions and limitations under the Apache License and the GPL License.
   */
  (function ($) {
      if(typeof $.fn.each2 == "undefined") {
          $.extend($.fn, {
              /*
               * 4-10 times faster .each replacement
               * use it carefully, as it overrides jQuery context of element on each iteration
               */
              each2 : function (c) {
                  var j = $([0]), i = -1, l = this.length;
                  while (
                      ++i < l
                          && (j.context = j[0] = this[i])
                          && c.call(j[0], i, j) !== false //"this"=DOM, i=index, j=jQuery object
                      );
                  return this;
              }
          });
      }
  })(jQuery);
  
  (function ($, undefined) {
      /*global document, window, jQuery, console */
  
      if (window.Select2 !== undefined) {
          return;
      }
  
      var KEY, AbstractSelect2, SingleSelect2, MultiSelect2, nextUid, sizer,
          lastMousePosition={x:0,y:0}, $document, scrollBarDimensions,
  
          KEY = {
              TAB: 9,
              ENTER: 13,
              ESC: 27,
              SPACE: 32,
              LEFT: 37,
              UP: 38,
              RIGHT: 39,
              DOWN: 40,
              SHIFT: 16,
              CTRL: 17,
              ALT: 18,
              PAGE_UP: 33,
              PAGE_DOWN: 34,
              HOME: 36,
              END: 35,
              BACKSPACE: 8,
              DELETE: 46,
              isArrow: function (k) {
                  k = k.which ? k.which : k;
                  switch (k) {
                      case KEY.LEFT:
                      case KEY.RIGHT:
                      case KEY.UP:
                      case KEY.DOWN:
                          return true;
                  }
                  return false;
              },
              isControl: function (e) {
                  var k = e.which;
                  switch (k) {
                      case KEY.SHIFT:
                      case KEY.CTRL:
                      case KEY.ALT:
                          return true;
                  }
  
                  if (e.metaKey) return true;
  
                  return false;
              },
              isFunctionKey: function (k) {
                  k = k.which ? k.which : k;
                  return k >= 112 && k <= 123;
              }
          },
          MEASURE_SCROLLBAR_TEMPLATE = "<div class='select2-measure-scrollbar'></div>",
  
          DIACRITICS = {"\u24B6":"A","\uFF21":"A","\u00C0":"A","\u00C1":"A","\u00C2":"A","\u1EA6":"A","\u1EA4":"A","\u1EAA":"A","\u1EA8":"A","\u00C3":"A","\u0100":"A","\u0102":"A","\u1EB0":"A","\u1EAE":"A","\u1EB4":"A","\u1EB2":"A","\u0226":"A","\u01E0":"A","\u00C4":"A","\u01DE":"A","\u1EA2":"A","\u00C5":"A","\u01FA":"A","\u01CD":"A","\u0200":"A","\u0202":"A","\u1EA0":"A","\u1EAC":"A","\u1EB6":"A","\u1E00":"A","\u0104":"A","\u023A":"A","\u2C6F":"A","\uA732":"AA","\u00C6":"AE","\u01FC":"AE","\u01E2":"AE","\uA734":"AO","\uA736":"AU","\uA738":"AV","\uA73A":"AV","\uA73C":"AY","\u24B7":"B","\uFF22":"B","\u1E02":"B","\u1E04":"B","\u1E06":"B","\u0243":"B","\u0182":"B","\u0181":"B","\u24B8":"C","\uFF23":"C","\u0106":"C","\u0108":"C","\u010A":"C","\u010C":"C","\u00C7":"C","\u1E08":"C","\u0187":"C","\u023B":"C","\uA73E":"C","\u24B9":"D","\uFF24":"D","\u1E0A":"D","\u010E":"D","\u1E0C":"D","\u1E10":"D","\u1E12":"D","\u1E0E":"D","\u0110":"D","\u018B":"D","\u018A":"D","\u0189":"D","\uA779":"D","\u01F1":"DZ","\u01C4":"DZ","\u01F2":"Dz","\u01C5":"Dz","\u24BA":"E","\uFF25":"E","\u00C8":"E","\u00C9":"E","\u00CA":"E","\u1EC0":"E","\u1EBE":"E","\u1EC4":"E","\u1EC2":"E","\u1EBC":"E","\u0112":"E","\u1E14":"E","\u1E16":"E","\u0114":"E","\u0116":"E","\u00CB":"E","\u1EBA":"E","\u011A":"E","\u0204":"E","\u0206":"E","\u1EB8":"E","\u1EC6":"E","\u0228":"E","\u1E1C":"E","\u0118":"E","\u1E18":"E","\u1E1A":"E","\u0190":"E","\u018E":"E","\u24BB":"F","\uFF26":"F","\u1E1E":"F","\u0191":"F","\uA77B":"F","\u24BC":"G","\uFF27":"G","\u01F4":"G","\u011C":"G","\u1E20":"G","\u011E":"G","\u0120":"G","\u01E6":"G","\u0122":"G","\u01E4":"G","\u0193":"G","\uA7A0":"G","\uA77D":"G","\uA77E":"G","\u24BD":"H","\uFF28":"H","\u0124":"H","\u1E22":"H","\u1E26":"H","\u021E":"H","\u1E24":"H","\u1E28":"H","\u1E2A":"H","\u0126":"H","\u2C67":"H","\u2C75":"H","\uA78D":"H","\u24BE":"I","\uFF29":"I","\u00CC":"I","\u00CD":"I","\u00CE":"I","\u0128":"I","\u012A":"I","\u012C":"I","\u0130":"I","\u00CF":"I","\u1E2E":"I","\u1EC8":"I","\u01CF":"I","\u0208":"I","\u020A":"I","\u1ECA":"I","\u012E":"I","\u1E2C":"I","\u0197":"I","\u24BF":"J","\uFF2A":"J","\u0134":"J","\u0248":"J","\u24C0":"K","\uFF2B":"K","\u1E30":"K","\u01E8":"K","\u1E32":"K","\u0136":"K","\u1E34":"K","\u0198":"K","\u2C69":"K","\uA740":"K","\uA742":"K","\uA744":"K","\uA7A2":"K","\u24C1":"L","\uFF2C":"L","\u013F":"L","\u0139":"L","\u013D":"L","\u1E36":"L","\u1E38":"L","\u013B":"L","\u1E3C":"L","\u1E3A":"L","\u0141":"L","\u023D":"L","\u2C62":"L","\u2C60":"L","\uA748":"L","\uA746":"L","\uA780":"L","\u01C7":"LJ","\u01C8":"Lj","\u24C2":"M","\uFF2D":"M","\u1E3E":"M","\u1E40":"M","\u1E42":"M","\u2C6E":"M","\u019C":"M","\u24C3":"N","\uFF2E":"N","\u01F8":"N","\u0143":"N","\u00D1":"N","\u1E44":"N","\u0147":"N","\u1E46":"N","\u0145":"N","\u1E4A":"N","\u1E48":"N","\u0220":"N","\u019D":"N","\uA790":"N","\uA7A4":"N","\u01CA":"NJ","\u01CB":"Nj","\u24C4":"O","\uFF2F":"O","\u00D2":"O","\u00D3":"O","\u00D4":"O","\u1ED2":"O","\u1ED0":"O","\u1ED6":"O","\u1ED4":"O","\u00D5":"O","\u1E4C":"O","\u022C":"O","\u1E4E":"O","\u014C":"O","\u1E50":"O","\u1E52":"O","\u014E":"O","\u022E":"O","\u0230":"O","\u00D6":"O","\u022A":"O","\u1ECE":"O","\u0150":"O","\u01D1":"O","\u020C":"O","\u020E":"O","\u01A0":"O","\u1EDC":"O","\u1EDA":"O","\u1EE0":"O","\u1EDE":"O","\u1EE2":"O","\u1ECC":"O","\u1ED8":"O","\u01EA":"O","\u01EC":"O","\u00D8":"O","\u01FE":"O","\u0186":"O","\u019F":"O","\uA74A":"O","\uA74C":"O","\u01A2":"OI","\uA74E":"OO","\u0222":"OU","\u24C5":"P","\uFF30":"P","\u1E54":"P","\u1E56":"P","\u01A4":"P","\u2C63":"P","\uA750":"P","\uA752":"P","\uA754":"P","\u24C6":"Q","\uFF31":"Q","\uA756":"Q","\uA758":"Q","\u024A":"Q","\u24C7":"R","\uFF32":"R","\u0154":"R","\u1E58":"R","\u0158":"R","\u0210":"R","\u0212":"R","\u1E5A":"R","\u1E5C":"R","\u0156":"R","\u1E5E":"R","\u024C":"R","\u2C64":"R","\uA75A":"R","\uA7A6":"R","\uA782":"R","\u24C8":"S","\uFF33":"S","\u1E9E":"S","\u015A":"S","\u1E64":"S","\u015C":"S","\u1E60":"S","\u0160":"S","\u1E66":"S","\u1E62":"S","\u1E68":"S","\u0218":"S","\u015E":"S","\u2C7E":"S","\uA7A8":"S","\uA784":"S","\u24C9":"T","\uFF34":"T","\u1E6A":"T","\u0164":"T","\u1E6C":"T","\u021A":"T","\u0162":"T","\u1E70":"T","\u1E6E":"T","\u0166":"T","\u01AC":"T","\u01AE":"T","\u023E":"T","\uA786":"T","\uA728":"TZ","\u24CA":"U","\uFF35":"U","\u00D9":"U","\u00DA":"U","\u00DB":"U","\u0168":"U","\u1E78":"U","\u016A":"U","\u1E7A":"U","\u016C":"U","\u00DC":"U","\u01DB":"U","\u01D7":"U","\u01D5":"U","\u01D9":"U","\u1EE6":"U","\u016E":"U","\u0170":"U","\u01D3":"U","\u0214":"U","\u0216":"U","\u01AF":"U","\u1EEA":"U","\u1EE8":"U","\u1EEE":"U","\u1EEC":"U","\u1EF0":"U","\u1EE4":"U","\u1E72":"U","\u0172":"U","\u1E76":"U","\u1E74":"U","\u0244":"U","\u24CB":"V","\uFF36":"V","\u1E7C":"V","\u1E7E":"V","\u01B2":"V","\uA75E":"V","\u0245":"V","\uA760":"VY","\u24CC":"W","\uFF37":"W","\u1E80":"W","\u1E82":"W","\u0174":"W","\u1E86":"W","\u1E84":"W","\u1E88":"W","\u2C72":"W","\u24CD":"X","\uFF38":"X","\u1E8A":"X","\u1E8C":"X","\u24CE":"Y","\uFF39":"Y","\u1EF2":"Y","\u00DD":"Y","\u0176":"Y","\u1EF8":"Y","\u0232":"Y","\u1E8E":"Y","\u0178":"Y","\u1EF6":"Y","\u1EF4":"Y","\u01B3":"Y","\u024E":"Y","\u1EFE":"Y","\u24CF":"Z","\uFF3A":"Z","\u0179":"Z","\u1E90":"Z","\u017B":"Z","\u017D":"Z","\u1E92":"Z","\u1E94":"Z","\u01B5":"Z","\u0224":"Z","\u2C7F":"Z","\u2C6B":"Z","\uA762":"Z","\u24D0":"a","\uFF41":"a","\u1E9A":"a","\u00E0":"a","\u00E1":"a","\u00E2":"a","\u1EA7":"a","\u1EA5":"a","\u1EAB":"a","\u1EA9":"a","\u00E3":"a","\u0101":"a","\u0103":"a","\u1EB1":"a","\u1EAF":"a","\u1EB5":"a","\u1EB3":"a","\u0227":"a","\u01E1":"a","\u00E4":"a","\u01DF":"a","\u1EA3":"a","\u00E5":"a","\u01FB":"a","\u01CE":"a","\u0201":"a","\u0203":"a","\u1EA1":"a","\u1EAD":"a","\u1EB7":"a","\u1E01":"a","\u0105":"a","\u2C65":"a","\u0250":"a","\uA733":"aa","\u00E6":"ae","\u01FD":"ae","\u01E3":"ae","\uA735":"ao","\uA737":"au","\uA739":"av","\uA73B":"av","\uA73D":"ay","\u24D1":"b","\uFF42":"b","\u1E03":"b","\u1E05":"b","\u1E07":"b","\u0180":"b","\u0183":"b","\u0253":"b","\u24D2":"c","\uFF43":"c","\u0107":"c","\u0109":"c","\u010B":"c","\u010D":"c","\u00E7":"c","\u1E09":"c","\u0188":"c","\u023C":"c","\uA73F":"c","\u2184":"c","\u24D3":"d","\uFF44":"d","\u1E0B":"d","\u010F":"d","\u1E0D":"d","\u1E11":"d","\u1E13":"d","\u1E0F":"d","\u0111":"d","\u018C":"d","\u0256":"d","\u0257":"d","\uA77A":"d","\u01F3":"dz","\u01C6":"dz","\u24D4":"e","\uFF45":"e","\u00E8":"e","\u00E9":"e","\u00EA":"e","\u1EC1":"e","\u1EBF":"e","\u1EC5":"e","\u1EC3":"e","\u1EBD":"e","\u0113":"e","\u1E15":"e","\u1E17":"e","\u0115":"e","\u0117":"e","\u00EB":"e","\u1EBB":"e","\u011B":"e","\u0205":"e","\u0207":"e","\u1EB9":"e","\u1EC7":"e","\u0229":"e","\u1E1D":"e","\u0119":"e","\u1E19":"e","\u1E1B":"e","\u0247":"e","\u025B":"e","\u01DD":"e","\u24D5":"f","\uFF46":"f","\u1E1F":"f","\u0192":"f","\uA77C":"f","\u24D6":"g","\uFF47":"g","\u01F5":"g","\u011D":"g","\u1E21":"g","\u011F":"g","\u0121":"g","\u01E7":"g","\u0123":"g","\u01E5":"g","\u0260":"g","\uA7A1":"g","\u1D79":"g","\uA77F":"g","\u24D7":"h","\uFF48":"h","\u0125":"h","\u1E23":"h","\u1E27":"h","\u021F":"h","\u1E25":"h","\u1E29":"h","\u1E2B":"h","\u1E96":"h","\u0127":"h","\u2C68":"h","\u2C76":"h","\u0265":"h","\u0195":"hv","\u24D8":"i","\uFF49":"i","\u00EC":"i","\u00ED":"i","\u00EE":"i","\u0129":"i","\u012B":"i","\u012D":"i","\u00EF":"i","\u1E2F":"i","\u1EC9":"i","\u01D0":"i","\u0209":"i","\u020B":"i","\u1ECB":"i","\u012F":"i","\u1E2D":"i","\u0268":"i","\u0131":"i","\u24D9":"j","\uFF4A":"j","\u0135":"j","\u01F0":"j","\u0249":"j","\u24DA":"k","\uFF4B":"k","\u1E31":"k","\u01E9":"k","\u1E33":"k","\u0137":"k","\u1E35":"k","\u0199":"k","\u2C6A":"k","\uA741":"k","\uA743":"k","\uA745":"k","\uA7A3":"k","\u24DB":"l","\uFF4C":"l","\u0140":"l","\u013A":"l","\u013E":"l","\u1E37":"l","\u1E39":"l","\u013C":"l","\u1E3D":"l","\u1E3B":"l","\u017F":"l","\u0142":"l","\u019A":"l","\u026B":"l","\u2C61":"l","\uA749":"l","\uA781":"l","\uA747":"l","\u01C9":"lj","\u24DC":"m","\uFF4D":"m","\u1E3F":"m","\u1E41":"m","\u1E43":"m","\u0271":"m","\u026F":"m","\u24DD":"n","\uFF4E":"n","\u01F9":"n","\u0144":"n","\u00F1":"n","\u1E45":"n","\u0148":"n","\u1E47":"n","\u0146":"n","\u1E4B":"n","\u1E49":"n","\u019E":"n","\u0272":"n","\u0149":"n","\uA791":"n","\uA7A5":"n","\u01CC":"nj","\u24DE":"o","\uFF4F":"o","\u00F2":"o","\u00F3":"o","\u00F4":"o","\u1ED3":"o","\u1ED1":"o","\u1ED7":"o","\u1ED5":"o","\u00F5":"o","\u1E4D":"o","\u022D":"o","\u1E4F":"o","\u014D":"o","\u1E51":"o","\u1E53":"o","\u014F":"o","\u022F":"o","\u0231":"o","\u00F6":"o","\u022B":"o","\u1ECF":"o","\u0151":"o","\u01D2":"o","\u020D":"o","\u020F":"o","\u01A1":"o","\u1EDD":"o","\u1EDB":"o","\u1EE1":"o","\u1EDF":"o","\u1EE3":"o","\u1ECD":"o","\u1ED9":"o","\u01EB":"o","\u01ED":"o","\u00F8":"o","\u01FF":"o","\u0254":"o","\uA74B":"o","\uA74D":"o","\u0275":"o","\u01A3":"oi","\u0223":"ou","\uA74F":"oo","\u24DF":"p","\uFF50":"p","\u1E55":"p","\u1E57":"p","\u01A5":"p","\u1D7D":"p","\uA751":"p","\uA753":"p","\uA755":"p","\u24E0":"q","\uFF51":"q","\u024B":"q","\uA757":"q","\uA759":"q","\u24E1":"r","\uFF52":"r","\u0155":"r","\u1E59":"r","\u0159":"r","\u0211":"r","\u0213":"r","\u1E5B":"r","\u1E5D":"r","\u0157":"r","\u1E5F":"r","\u024D":"r","\u027D":"r","\uA75B":"r","\uA7A7":"r","\uA783":"r","\u24E2":"s","\uFF53":"s","\u00DF":"s","\u015B":"s","\u1E65":"s","\u015D":"s","\u1E61":"s","\u0161":"s","\u1E67":"s","\u1E63":"s","\u1E69":"s","\u0219":"s","\u015F":"s","\u023F":"s","\uA7A9":"s","\uA785":"s","\u1E9B":"s","\u24E3":"t","\uFF54":"t","\u1E6B":"t","\u1E97":"t","\u0165":"t","\u1E6D":"t","\u021B":"t","\u0163":"t","\u1E71":"t","\u1E6F":"t","\u0167":"t","\u01AD":"t","\u0288":"t","\u2C66":"t","\uA787":"t","\uA729":"tz","\u24E4":"u","\uFF55":"u","\u00F9":"u","\u00FA":"u","\u00FB":"u","\u0169":"u","\u1E79":"u","\u016B":"u","\u1E7B":"u","\u016D":"u","\u00FC":"u","\u01DC":"u","\u01D8":"u","\u01D6":"u","\u01DA":"u","\u1EE7":"u","\u016F":"u","\u0171":"u","\u01D4":"u","\u0215":"u","\u0217":"u","\u01B0":"u","\u1EEB":"u","\u1EE9":"u","\u1EEF":"u","\u1EED":"u","\u1EF1":"u","\u1EE5":"u","\u1E73":"u","\u0173":"u","\u1E77":"u","\u1E75":"u","\u0289":"u","\u24E5":"v","\uFF56":"v","\u1E7D":"v","\u1E7F":"v","\u028B":"v","\uA75F":"v","\u028C":"v","\uA761":"vy","\u24E6":"w","\uFF57":"w","\u1E81":"w","\u1E83":"w","\u0175":"w","\u1E87":"w","\u1E85":"w","\u1E98":"w","\u1E89":"w","\u2C73":"w","\u24E7":"x","\uFF58":"x","\u1E8B":"x","\u1E8D":"x","\u24E8":"y","\uFF59":"y","\u1EF3":"y","\u00FD":"y","\u0177":"y","\u1EF9":"y","\u0233":"y","\u1E8F":"y","\u00FF":"y","\u1EF7":"y","\u1E99":"y","\u1EF5":"y","\u01B4":"y","\u024F":"y","\u1EFF":"y","\u24E9":"z","\uFF5A":"z","\u017A":"z","\u1E91":"z","\u017C":"z","\u017E":"z","\u1E93":"z","\u1E95":"z","\u01B6":"z","\u0225":"z","\u0240":"z","\u2C6C":"z","\uA763":"z"};
  
      $document = $(document);
  
      nextUid=(function() { var counter=1; return function() { return counter++; }; }());
  
  
      function stripDiacritics(str) {
          var ret, i, l, c;
  
          if (!str || str.length < 1) return str;
  
          ret = "";
          for (i = 0, l = str.length; i < l; i++) {
              c = str.charAt(i);
              ret += DIACRITICS[c] || c;
          }
          return ret;
      }
  
      function indexOf(value, array) {
          var i = 0, l = array.length;
          for (; i < l; i = i + 1) {
              if (equal(value, array[i])) return i;
          }
          return -1;
      }
  
      function measureScrollbar () {
          var $template = $( MEASURE_SCROLLBAR_TEMPLATE );
          $template.appendTo('body');
  
          var dim = {
              width: $template.width() - $template[0].clientWidth,
              height: $template.height() - $template[0].clientHeight
          };
          $template.remove();
  
          return dim;
      }
  
      /**
       * Compares equality of a and b
       * @param a
       * @param b
       */
      function equal(a, b) {
          if (a === b) return true;
          if (a === undefined || b === undefined) return false;
          if (a === null || b === null) return false;
          // Check whether 'a' or 'b' is a string (primitive or object).
          // The concatenation of an empty string (+'') converts its argument to a string's primitive.
          if (a.constructor === String) return a+'' === b+''; // a+'' - in case 'a' is a String object
          if (b.constructor === String) return b+'' === a+''; // b+'' - in case 'b' is a String object
          return false;
      }
  
      /**
       * Splits the string into an array of values, trimming each value. An empty array is returned for nulls or empty
       * strings
       * @param string
       * @param separator
       */
      function splitVal(string, separator) {
          var val, i, l;
          if (string === null || string.length < 1) return [];
          val = string.split(separator);
          for (i = 0, l = val.length; i < l; i = i + 1) val[i] = $.trim(val[i]);
          return val;
      }
  
      function getSideBorderPadding(element) {
          return element.outerWidth(false) - element.width();
      }
  
      function installKeyUpChangeEvent(element) {
          var key="keyup-change-value";
          element.on("keydown", function () {
              if ($.data(element, key) === undefined) {
                  $.data(element, key, element.val());
              }
          });
          element.on("keyup", function () {
              var val= $.data(element, key);
              if (val !== undefined && element.val() !== val) {
                  $.removeData(element, key);
                  element.trigger("keyup-change");
              }
          });
      }
  
      $document.on("mousemove", function (e) {
          lastMousePosition.x = e.pageX;
          lastMousePosition.y = e.pageY;
      });
  
      /**
       * filters mouse events so an event is fired only if the mouse moved.
       *
       * filters out mouse events that occur when mouse is stationary but
       * the elements under the pointer are scrolled.
       */
      function installFilteredMouseMove(element) {
          element.on("mousemove", function (e) {
              var lastpos = lastMousePosition;
              if (lastpos === undefined || lastpos.x !== e.pageX || lastpos.y !== e.pageY) {
                  $(e.target).trigger("mousemove-filtered", e);
              }
          });
      }
  
      /**
       * Debounces a function. Returns a function that calls the original fn function only if no invocations have been made
       * within the last quietMillis milliseconds.
       *
       * @param quietMillis number of milliseconds to wait before invoking fn
       * @param fn function to be debounced
       * @param ctx object to be used as this reference within fn
       * @return debounced version of fn
       */
      function debounce(quietMillis, fn, ctx) {
          ctx = ctx || undefined;
          var timeout;
          return function () {
              var args = arguments;
              window.clearTimeout(timeout);
              timeout = window.setTimeout(function() {
                  fn.apply(ctx, args);
              }, quietMillis);
          };
      }
  
      /**
       * A simple implementation of a thunk
       * @param formula function used to lazily initialize the thunk
       * @return {Function}
       */
      function thunk(formula) {
          var evaluated = false,
              value;
          return function() {
              if (evaluated === false) { value = formula(); evaluated = true; }
              return value;
          };
      };
  
      function installDebouncedScroll(threshold, element) {
          var notify = debounce(threshold, function (e) { element.trigger("scroll-debounced", e);});
          element.on("scroll", function (e) {
              if (indexOf(e.target, element.get()) >= 0) notify(e);
          });
      }
  
      function focus($el) {
          if ($el[0] === document.activeElement) return;
  
          /* set the focus in a 0 timeout - that way the focus is set after the processing
           of the current event has finished - which seems like the only reliable way
           to set focus */
          window.setTimeout(function() {
              var el=$el[0], pos=$el.val().length, range;
  
              $el.focus();
  
              /* make sure el received focus so we do not error out when trying to manipulate the caret.
               sometimes modals or others listeners may steal it after its set */
              if ($el.is(":visible") && el === document.activeElement) {
  
                  /* after the focus is set move the caret to the end, necessary when we val()
                   just before setting focus */
                  if(el.setSelectionRange)
                  {
                      el.setSelectionRange(pos, pos);
                  }
                  else if (el.createTextRange) {
                      range = el.createTextRange();
                      range.collapse(false);
                      range.select();
                  }
              }
          }, 0);
      }
  
      function getCursorInfo(el) {
          el = $(el)[0];
          var offset = 0;
          var length = 0;
          if ('selectionStart' in el) {
              offset = el.selectionStart;
              length = el.selectionEnd - offset;
          } else if ('selection' in document) {
              el.focus();
              var sel = document.selection.createRange();
              length = document.selection.createRange().text.length;
              sel.moveStart('character', -el.value.length);
              offset = sel.text.length - length;
          }
          return { offset: offset, length: length };
      }
  
      function killEvent(event) {
          event.preventDefault();
          event.stopPropagation();
      }
      function killEventImmediately(event) {
          event.preventDefault();
          event.stopImmediatePropagation();
      }
  
      function measureTextWidth(e) {
          if (!sizer){
              var style = e[0].currentStyle || window.getComputedStyle(e[0], null);
              sizer = $(document.createElement("div")).css({
                  position: "absolute",
                  left: "-10000px",
                  top: "-10000px",
                  display: "none",
                  fontSize: style.fontSize,
                  fontFamily: style.fontFamily,
                  fontStyle: style.fontStyle,
                  fontWeight: style.fontWeight,
                  letterSpacing: style.letterSpacing,
                  textTransform: style.textTransform,
                  whiteSpace: "nowrap"
              });
              sizer.attr("class","select2-sizer");
              $("body").append(sizer);
          }
          sizer.text(e.val());
          return sizer.width();
      }
  
      function syncCssClasses(dest, src, adapter) {
          var classes, replacements = [], adapted;
  
          classes = dest.attr("class");
          if (classes) {
              classes = '' + classes; // for IE which returns object
              $(classes.split(" ")).each2(function() {
                  if (this.indexOf("select2-") === 0) {
                      replacements.push(this);
                  }
              });
          }
          classes = src.attr("class");
          if (classes) {
              classes = '' + classes; // for IE which returns object
              $(classes.split(" ")).each2(function() {
                  if (this.indexOf("select2-") !== 0) {
                      adapted = adapter(this);
                      if (adapted) {
                          replacements.push(adapted);
                      }
                  }
              });
          }
          dest.attr("class", replacements.join(" "));
      }
  
  
      function markMatch(text, term, markup, escapeMarkup) {
          var match=stripDiacritics(text.toUpperCase()).indexOf(stripDiacritics(term.toUpperCase())),
              tl=term.length;
  
          if (match<0) {
              markup.push(escapeMarkup(text));
              return;
          }
  
          markup.push(escapeMarkup(text.substring(0, match)));
          markup.push("<span class='select2-match'>");
          markup.push(escapeMarkup(text.substring(match, match + tl)));
          markup.push("</span>");
          markup.push(escapeMarkup(text.substring(match + tl, text.length)));
      }
  
      function defaultEscapeMarkup(markup) {
          var replace_map = {
              '\\': '&#92;',
              '&': '&amp;',
              '<': '&lt;',
              '>': '&gt;',
              '"': '&quot;',
              "'": '&#39;',
              "/": '&#47;'
          };
  
          return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
              return replace_map[match];
          });
      }
  
      /**
       * Produces an ajax-based query function
       *
       * @param options object containing configuration paramters
       * @param options.params parameter map for the transport ajax call, can contain such options as cache, jsonpCallback, etc. see $.ajax
       * @param options.transport function that will be used to execute the ajax request. must be compatible with parameters supported by $.ajax
       * @param options.url url for the data
       * @param options.data a function(searchTerm, pageNumber, context) that should return an object containing query string parameters for the above url.
       * @param options.dataType request data type: ajax, jsonp, other datatatypes supported by jQuery's $.ajax function or the transport function if specified
       * @param options.quietMillis (optional) milliseconds to wait before making the ajaxRequest, helps debounce the ajax function if invoked too often
       * @param options.results a function(remoteData, pageNumber) that converts data returned form the remote request to the format expected by Select2.
       *      The expected format is an object containing the following keys:
       *      results array of objects that will be used as choices
       *      more (optional) boolean indicating whether there are more results available
       *      Example: {results:[{id:1, text:'Red'},{id:2, text:'Blue'}], more:true}
       */
      function ajax(options) {
          var timeout, // current scheduled but not yet executed request
              handler = null,
              quietMillis = options.quietMillis || 100,
              ajaxUrl = options.url,
              self = this;
  
          return function (query) {
              window.clearTimeout(timeout);
              timeout = window.setTimeout(function () {
                  var data = options.data, // ajax data function
                      url = ajaxUrl, // ajax url string or function
                      transport = options.transport || $.fn.select2.ajaxDefaults.transport,
                  // deprecated - to be removed in 4.0  - use params instead
                      deprecated = {
                          type: options.type || 'GET', // set type of request (GET or POST)
                          cache: options.cache || false,
                          jsonpCallback: options.jsonpCallback||undefined,
                          dataType: options.dataType||"json"
                      },
                      params = $.extend({}, $.fn.select2.ajaxDefaults.params, deprecated);
  
                  data = data ? data.call(self, query.term, query.page, query.context) : null;
                  url = (typeof url === 'function') ? url.call(self, query.term, query.page, query.context) : url;
  
                  if (handler) { handler.abort(); }
  
                  if (options.params) {
                      if ($.isFunction(options.params)) {
                          $.extend(params, options.params.call(self));
                      } else {
                          $.extend(params, options.params);
                      }
                  }
  
                  $.extend(params, {
                      url: url,
                      dataType: options.dataType,
                      data: data,
                      success: function (data) {
                          // TODO - replace query.page with query so users have access to term, page, etc.
                          var results = options.results(data, query.page);
                          query.callback(results);
                      }
                  });
                  handler = transport.call(self, params);
              }, quietMillis);
          };
      }
  
      /**
       * Produces a query function that works with a local array
       *
       * @param options object containing configuration parameters. The options parameter can either be an array or an
       * object.
       *
       * If the array form is used it is assumed that it contains objects with 'id' and 'text' keys.
       *
       * If the object form is used ti is assumed that it contains 'data' and 'text' keys. The 'data' key should contain
       * an array of objects that will be used as choices. These objects must contain at least an 'id' key. The 'text'
       * key can either be a String in which case it is expected that each element in the 'data' array has a key with the
       * value of 'text' which will be used to match choices. Alternatively, text can be a function(item) that can extract
       * the text.
       */
      function local(options) {
          var data = options, // data elements
              dataText,
              tmp,
              text = function (item) { return ""+item.text; }; // function used to retrieve the text portion of a data item that is matched against the search
  
          if ($.isArray(data)) {
              tmp = data;
              data = { results: tmp };
          }
  
          if ($.isFunction(data) === false) {
              tmp = data;
              data = function() { return tmp; };
          }
  
          var dataItem = data();
          if (dataItem.text) {
              text = dataItem.text;
              // if text is not a function we assume it to be a key name
              if (!$.isFunction(text)) {
                  dataText = dataItem.text; // we need to store this in a separate variable because in the next step data gets reset and data.text is no longer available
                  text = function (item) { return item[dataText]; };
              }
          }
  
          return function (query) {
              var t = query.term, filtered = { results: [] }, process;
              if (t === "") {
                  query.callback(data());
                  return;
              }
  
              process = function(datum, collection) {
                  var group, attr;
                  datum = datum[0];
                  if (datum.children) {
                      group = {};
                      for (attr in datum) {
                          if (datum.hasOwnProperty(attr)) group[attr]=datum[attr];
                      }
                      group.children=[];
                      $(datum.children).each2(function(i, childDatum) { process(childDatum, group.children); });
                      if (group.children.length || query.matcher(t, text(group), datum)) {
                          collection.push(group);
                      }
                  } else {
                      if (query.matcher(t, text(datum), datum)) {
                          collection.push(datum);
                      }
                  }
              };
  
              $(data().results).each2(function(i, datum) { process(datum, filtered.results); });
              query.callback(filtered);
          };
      }
  
      // TODO javadoc
      function tags(data) {
          var isFunc = $.isFunction(data);
          return function (query) {
              var t = query.term, filtered = {results: []};
              $(isFunc ? data() : data).each(function () {
                  var isObject = this.text !== undefined,
                      text = isObject ? this.text : this;
                  if (t === "" || query.matcher(t, text)) {
                      filtered.results.push(isObject ? this : {id: this, text: this});
                  }
              });
              query.callback(filtered);
          };
      }
  
      /**
       * Checks if the formatter function should be used.
       *
       * Throws an error if it is not a function. Returns true if it should be used,
       * false if no formatting should be performed.
       *
       * @param formatter
       */
      function checkFormatter(formatter, formatterName) {
          if ($.isFunction(formatter)) return true;
          if (!formatter) return false;
          throw new Error(formatterName +" must be a function or a falsy value");
      }
  
      function evaluate(val) {
          return $.isFunction(val) ? val() : val;
      }
  
      function countResults(results) {
          var count = 0;
          $.each(results, function(i, item) {
              if (item.children) {
                  count += countResults(item.children);
              } else {
                  count++;
              }
          });
          return count;
      }
  
      /**
       * Default tokenizer. This function uses breaks the input on substring match of any string from the
       * opts.tokenSeparators array and uses opts.createSearchChoice to create the choice object. Both of those
       * two options have to be defined in order for the tokenizer to work.
       *
       * @param input text user has typed so far or pasted into the search field
       * @param selection currently selected choices
       * @param selectCallback function(choice) callback tho add the choice to selection
       * @param opts select2's opts
       * @return undefined/null to leave the current input unchanged, or a string to change the input to the returned value
       */
      function defaultTokenizer(input, selection, selectCallback, opts) {
          var original = input, // store the original so we can compare and know if we need to tell the search to update its text
              dupe = false, // check for whether a token we extracted represents a duplicate selected choice
              token, // token
              index, // position at which the separator was found
              i, l, // looping variables
              separator; // the matched separator
  
          if (!opts.createSearchChoice || !opts.tokenSeparators || opts.tokenSeparators.length < 1) return undefined;
  
          while (true) {
              index = -1;
  
              for (i = 0, l = opts.tokenSeparators.length; i < l; i++) {
                  separator = opts.tokenSeparators[i];
                  index = input.indexOf(separator);
                  if (index >= 0) break;
              }
  
              if (index < 0) break; // did not find any token separator in the input string, bail
  
              token = input.substring(0, index);
              input = input.substring(index + separator.length);
  
              if (token.length > 0) {
                  token = opts.createSearchChoice.call(this, token, selection);
                  if (token !== undefined && token !== null && opts.id(token) !== undefined && opts.id(token) !== null) {
                      dupe = false;
                      for (i = 0, l = selection.length; i < l; i++) {
                          if (equal(opts.id(token), opts.id(selection[i]))) {
                              dupe = true; break;
                          }
                      }
  
                      if (!dupe) selectCallback(token);
                  }
              }
          }
  
          if (original!==input) return input;
      }
  
      /**
       * Creates a new class
       *
       * @param superClass
       * @param methods
       */
      function clazz(SuperClass, methods) {
          var constructor = function () {};
          constructor.prototype = new SuperClass;
          constructor.prototype.constructor = constructor;
          constructor.prototype.parent = SuperClass.prototype;
          constructor.prototype = $.extend(constructor.prototype, methods);
          return constructor;
      }
  
      AbstractSelect2 = clazz(Object, {
  
          // abstract
          bind: function (func) {
              var self = this;
              return function () {
                  func.apply(self, arguments);
              };
          },
  
          // abstract
          init: function (opts) {
              var results, search, resultsSelector = ".select2-results";
  
              // prepare options
              this.opts = opts = this.prepareOpts(opts);
  
              this.id=opts.id;
  
              // destroy if called on an existing component
              if (opts.element.data("select2") !== undefined &&
                  opts.element.data("select2") !== null) {
                  opts.element.data("select2").destroy();
              }
  
              this.container = this.createContainer();
  
              this.containerId="s2id_"+(opts.element.attr("id") || "autogen"+nextUid());
              this.containerSelector="#"+this.containerId.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1');
              this.container.attr("id", this.containerId);
  
              // cache the body so future lookups are cheap
              this.body = thunk(function() { return opts.element.closest("body"); });
  
              syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);
  
              this.container.attr("style", opts.element.attr("style"));
              this.container.css(evaluate(opts.containerCss));
              this.container.addClass(evaluate(opts.containerCssClass));
  
              this.elementTabIndex = this.opts.element.attr("tabindex");
  
              // swap container for the element
              this.opts.element
                  .data("select2", this)
                  .attr("tabindex", "-1")
                  .before(this.container)
                  .on("click.select2", killEvent); // do not leak click events
  
              this.container.data("select2", this);
  
              this.dropdown = this.container.find(".select2-drop");
  
              syncCssClasses(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass);
  
              this.dropdown.addClass(evaluate(opts.dropdownCssClass));
              this.dropdown.data("select2", this);
              this.dropdown.on("click", killEvent);
  
              this.results = results = this.container.find(resultsSelector);
              this.search = search = this.container.find("input.select2-input");
  
              this.queryCount = 0;
              this.resultsPage = 0;
              this.context = null;
  
              // initialize the container
              this.initContainer();
  
              this.container.on("click", killEvent);
  
              installFilteredMouseMove(this.results);
              this.dropdown.on("mousemove-filtered touchstart touchmove touchend", resultsSelector, this.bind(this.highlightUnderEvent));
  
              installDebouncedScroll(80, this.results);
              this.dropdown.on("scroll-debounced", resultsSelector, this.bind(this.loadMoreIfNeeded));
  
              // do not propagate change event from the search field out of the component
              $(this.container).on("change", ".select2-input", function(e) {e.stopPropagation();});
              $(this.dropdown).on("change", ".select2-input", function(e) {e.stopPropagation();});
  
              // if jquery.mousewheel plugin is installed we can prevent out-of-bounds scrolling of results via mousewheel
              if ($.fn.mousewheel) {
                  results.mousewheel(function (e, delta, deltaX, deltaY) {
                      var top = results.scrollTop();
                      if (deltaY > 0 && top - deltaY <= 0) {
                          results.scrollTop(0);
                          killEvent(e);
                      } else if (deltaY < 0 && results.get(0).scrollHeight - results.scrollTop() + deltaY <= results.height()) {
                          results.scrollTop(results.get(0).scrollHeight - results.height());
                          killEvent(e);
                      }
                  });
              }
  
              installKeyUpChangeEvent(search);
              search.on("keyup-change input paste", this.bind(this.updateResults));
              search.on("focus", function () { search.addClass("select2-focused"); });
              search.on("blur", function () { search.removeClass("select2-focused");});
  
              this.dropdown.on("mouseup", resultsSelector, this.bind(function (e) {
                  if ($(e.target).closest(".select2-result-selectable").length > 0) {
                      this.highlightUnderEvent(e);
                      this.selectHighlighted(e);
                  }
              }));
  
              // trap all mouse events from leaving the dropdown. sometimes there may be a modal that is listening
              // for mouse events outside of itself so it can close itself. since the dropdown is now outside the select2's
              // dom it will trigger the popup close, which is not what we want
              this.dropdown.on("click mouseup mousedown", function (e) { e.stopPropagation(); });
  
              if ($.isFunction(this.opts.initSelection)) {
                  // initialize selection based on the current value of the source element
                  this.initSelection();
  
                  // if the user has provided a function that can set selection based on the value of the source element
                  // we monitor the change event on the element and trigger it, allowing for two way synchronization
                  this.monitorSource();
              }
  
              if (opts.maximumInputLength !== null) {
                  this.search.attr("maxlength", opts.maximumInputLength);
              }
  
              var disabled = opts.element.prop("disabled");
              if (disabled === undefined) disabled = false;
              this.enable(!disabled);
  
              var readonly = opts.element.prop("readonly");
              if (readonly === undefined) readonly = false;
              this.readonly(readonly);
  
              // Calculate size of scrollbar
              scrollBarDimensions = scrollBarDimensions || measureScrollbar();
  
              this.autofocus = opts.element.prop("autofocus");
              opts.element.prop("autofocus", false);
              if (this.autofocus) this.focus();
  
              this.nextSearchTerm = undefined;
          },
  
          // abstract
          destroy: function () {
              var element=this.opts.element, select2 = element.data("select2");
  
              this.close();
  
              if (this.propertyObserver) { delete this.propertyObserver; this.propertyObserver = null; }
  
              if (select2 !== undefined) {
                  select2.container.remove();
                  select2.dropdown.remove();
                  element
                      .removeClass("select2-offscreen")
                      .removeData("select2")
                      .off(".select2")
                      .prop("autofocus", this.autofocus || false);
                  if (this.elementTabIndex) {
                      element.attr({tabindex: this.elementTabIndex});
                  } else {
                      element.removeAttr("tabindex");
                  }
                  element.show();
              }
          },
  
          // abstract
          optionToData: function(element) {
              if (element.is("option")) {
                  return {
                      id:element.prop("value"),
                      text:element.text(),
                      element: element.get(),
                      css: element.attr("class"),
                      disabled: element.prop("disabled"),
                      locked: equal(element.attr("locked"), "locked") || equal(element.data("locked"), true)
                  };
              } else if (element.is("optgroup")) {
                  return {
                      text:element.attr("label"),
                      children:[],
                      element: element.get(),
                      css: element.attr("class")
                  };
              }
          },
  
          // abstract
          prepareOpts: function (opts) {
              var element, select, idKey, ajaxUrl, self = this;
  
              element = opts.element;
  
              if (element.get(0).tagName.toLowerCase() === "select") {
                  this.select = select = opts.element;
              }
  
              if (select) {
                  // these options are not allowed when attached to a select because they are picked up off the element itself
                  $.each(["id", "multiple", "ajax", "query", "createSearchChoice", "initSelection", "data", "tags"], function () {
                      if (this in opts) {
                          throw new Error("Option '" + this + "' is not allowed for Select2 when attached to a <select> element.");
                      }
                  });
              }
  
              opts = $.extend({}, {
                  populateResults: function(container, results, query) {
                      var populate, id=this.opts.id;
  
                      populate=function(results, container, depth) {
  
                          var i, l, result, selectable, disabled, compound, node, label, innerContainer, formatted;
  
                          results = opts.sortResults(results, container, query);
  
                          for (i = 0, l = results.length; i < l; i = i + 1) {
  
                              result=results[i];
  
                              disabled = (result.disabled === true);
                              selectable = (!disabled) && (id(result) !== undefined);
  
                              compound=result.children && result.children.length > 0;
  
                              node=$("<li></li>");
                              node.addClass("select2-results-dept-"+depth);
                              node.addClass("select2-result");
                              node.addClass(selectable ? "select2-result-selectable" : "select2-result-unselectable");
                              if (disabled) { node.addClass("select2-disabled"); }
                              if (compound) { node.addClass("select2-result-with-children"); }
                              node.addClass(self.opts.formatResultCssClass(result));
  
                              label=$(document.createElement("div"));
                              label.addClass("select2-result-label");
  
                              formatted=opts.formatResult(result, label, query, self.opts.escapeMarkup);
                              if (formatted!==undefined) {
                                  label.html(formatted);
                              }
  
                              node.append(label);
  
                              if (compound) {
  
                                  innerContainer=$("<ul></ul>");
                                  innerContainer.addClass("select2-result-sub");
                                  populate(result.children, innerContainer, depth+1);
                                  node.append(innerContainer);
                              }
  
                              node.data("select2-data", result);
                              container.append(node);
                          }
                      };
  
                      populate(results, container, 0);
                  }
              }, $.fn.select2.defaults, opts);
  
              if (typeof(opts.id) !== "function") {
                  idKey = opts.id;
                  opts.id = function (e) { return e[idKey]; };
              }
  
              if ($.isArray(opts.element.data("select2Tags"))) {
                  if ("tags" in opts) {
                      throw "tags specified as both an attribute 'data-select2-tags' and in options of Select2 " + opts.element.attr("id");
                  }
                  opts.tags=opts.element.data("select2Tags");
              }
  
              if (select) {
                  opts.query = this.bind(function (query) {
                      var data = { results: [], more: false },
                          term = query.term,
                          children, placeholderOption, process;
  
                      process=function(element, collection) {
                          var group;
                          if (element.is("option")) {
                              if (query.matcher(term, element.text(), element)) {
                                  collection.push(self.optionToData(element));
                              }
                          } else if (element.is("optgroup")) {
                              group=self.optionToData(element);
                              element.children().each2(function(i, elm) { process(elm, group.children); });
                              if (group.children.length>0) {
                                  collection.push(group);
                              }
                          }
                      };
  
                      children=element.children();
  
                      // ignore the placeholder option if there is one
                      if (this.getPlaceholder() !== undefined && children.length > 0) {
                          placeholderOption = this.getPlaceholderOption();
                          if (placeholderOption) {
                              children=children.not(placeholderOption);
                          }
                      }
  
                      children.each2(function(i, elm) { process(elm, data.results); });
  
                      query.callback(data);
                  });
                  // this is needed because inside val() we construct choices from options and there id is hardcoded
                  opts.id=function(e) { return e.id; };
                  opts.formatResultCssClass = function(data) { return data.css; };
              } else {
                  if (!("query" in opts)) {
  
                      if ("ajax" in opts) {
                          ajaxUrl = opts.element.data("ajax-url");
                          if (ajaxUrl && ajaxUrl.length > 0) {
                              opts.ajax.url = ajaxUrl;
                          }
                          opts.query = ajax.call(opts.element, opts.ajax);
                      } else if ("data" in opts) {
                          opts.query = local(opts.data);
                      } else if ("tags" in opts) {
                          opts.query = tags(opts.tags);
                          if (opts.createSearchChoice === undefined) {
                              opts.createSearchChoice = function (term) { return {id: $.trim(term), text: $.trim(term)}; };
                          }
                          if (opts.initSelection === undefined) {
                              opts.initSelection = function (element, callback) {
                                  var data = [];
                                  $(splitVal(element.val(), opts.separator)).each(function () {
                                      var obj = { id: this, text: this },
                                          tags = opts.tags;
                                      if ($.isFunction(tags)) tags=tags();
                                      $(tags).each(function() { if (equal(this.id, obj.id)) { obj = this; return false; } });
                                      data.push(obj);
                                  });
  
                                  callback(data);
                              };
                          }
                      }
                  }
              }
              if (typeof(opts.query) !== "function") {
                  throw "query function not defined for Select2 " + opts.element.attr("id");
              }
  
              return opts;
          },
  
          /**
           * Monitor the original element for changes and update select2 accordingly
           */
          // abstract
          monitorSource: function () {
              var el = this.opts.element, sync, observer;
  
              el.on("change.select2", this.bind(function (e) {
                  if (this.opts.element.data("select2-change-triggered") !== true) {
                      this.initSelection();
                  }
              }));
  
              sync = this.bind(function () {
  
                  // sync enabled state
                  var disabled = el.prop("disabled");
                  if (disabled === undefined) disabled = false;
                  this.enable(!disabled);
  
                  var readonly = el.prop("readonly");
                  if (readonly === undefined) readonly = false;
                  this.readonly(readonly);
  
                  syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);
                  this.container.addClass(evaluate(this.opts.containerCssClass));
  
                  syncCssClasses(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass);
                  this.dropdown.addClass(evaluate(this.opts.dropdownCssClass));
  
              });
  
              // IE8-10
              el.on("propertychange.select2", sync);
  
              // hold onto a reference of the callback to work around a chromium bug
              if (this.mutationCallback === undefined) {
                  this.mutationCallback = function (mutations) {
                      mutations.forEach(sync);
                  }
              }
  
              // safari, chrome, firefox, IE11
              observer = window.MutationObserver || window.WebKitMutationObserver|| window.MozMutationObserver;
              if (observer !== undefined) {
                  if (this.propertyObserver) { delete this.propertyObserver; this.propertyObserver = null; }
                  this.propertyObserver = new observer(this.mutationCallback);
                  this.propertyObserver.observe(el.get(0), { attributes:true, subtree:false });
              }
          },
  
          // abstract
          triggerSelect: function(data) {
              var evt = $.Event("select2-selecting", { val: this.id(data), object: data });
              this.opts.element.trigger(evt);
              return !evt.isDefaultPrevented();
          },
  
          /**
           * Triggers the change event on the source element
           */
          // abstract
          triggerChange: function (details) {
  
              details = details || {};
              details= $.extend({}, details, { type: "change", val: this.val() });
              // prevents recursive triggering
              this.opts.element.data("select2-change-triggered", true);
              this.opts.element.trigger(details);
              this.opts.element.data("select2-change-triggered", false);
  
              // some validation frameworks ignore the change event and listen instead to keyup, click for selects
              // so here we trigger the click event manually
              this.opts.element.click();
  
              // ValidationEngine ignorea the change event and listens instead to blur
              // so here we trigger the blur event manually if so desired
              if (this.opts.blurOnChange)
                  this.opts.element.blur();
          },
  
          //abstract
          isInterfaceEnabled: function()
          {
              return this.enabledInterface === true;
          },
  
          // abstract
          enableInterface: function() {
              var enabled = this._enabled && !this._readonly,
                  disabled = !enabled;
  
              if (enabled === this.enabledInterface) return false;
  
              this.container.toggleClass("select2-container-disabled", disabled);
              this.close();
              this.enabledInterface = enabled;
  
              return true;
          },
  
          // abstract
          enable: function(enabled) {
              if (enabled === undefined) enabled = true;
              if (this._enabled === enabled) return;
              this._enabled = enabled;
  
              this.opts.element.prop("disabled", !enabled);
              this.enableInterface();
          },
  
          // abstract
          disable: function() {
              this.enable(false);
          },
  
          // abstract
          readonly: function(enabled) {
              if (enabled === undefined) enabled = false;
              if (this._readonly === enabled) return false;
              this._readonly = enabled;
  
              this.opts.element.prop("readonly", enabled);
              this.enableInterface();
              return true;
          },
  
          // abstract
          opened: function () {
              return this.container.hasClass("select2-dropdown-open");
          },
  
          // abstract
          positionDropdown: function() {
              var $dropdown = this.dropdown,
                  offset = this.container.offset(),
                  height = this.container.outerHeight(false),
                  width = this.container.outerWidth(false),
                  dropHeight = $dropdown.outerHeight(false),
                  $window = $(window),
                  windowWidth = $window.width(),
                  windowHeight = $window.height(),
                  viewPortRight = $window.scrollLeft() + windowWidth,
                  viewportBottom = $window.scrollTop() + windowHeight,
                  dropTop = offset.top + height,
                  dropLeft = offset.left,
                  enoughRoomBelow = dropTop + dropHeight <= viewportBottom,
                  enoughRoomAbove = (offset.top - dropHeight) >= this.body().scrollTop(),
                  dropWidth = $dropdown.outerWidth(false),
                  enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight,
                  aboveNow = $dropdown.hasClass("select2-drop-above"),
                  bodyOffset,
                  above,
                  changeDirection,
                  css,
                  resultsListNode;
  
              // always prefer the current above/below alignment, unless there is not enough room
              if (aboveNow) {
                  above = true;
                  if (!enoughRoomAbove && enoughRoomBelow) {
                      changeDirection = true;
                      above = false;
                  }
              } else {
                  above = false;
                  if (!enoughRoomBelow && enoughRoomAbove) {
                      changeDirection = true;
                      above = true;
                  }
              }
  
              //if we are changing direction we need to get positions when dropdown is hidden;
              if (changeDirection) {
                  $dropdown.hide();
                  offset = this.container.offset();
                  height = this.container.outerHeight(false);
                  width = this.container.outerWidth(false);
                  dropHeight = $dropdown.outerHeight(false);
                  viewPortRight = $window.scrollLeft() + windowWidth;
                  viewportBottom = $window.scrollTop() + windowHeight;
                  dropTop = offset.top + height;
                  dropLeft = offset.left;
                  dropWidth = $dropdown.outerWidth(false);
                  enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight;
                  $dropdown.show();
              }
  
              if (this.opts.dropdownAutoWidth) {
                  resultsListNode = $('.select2-results', $dropdown)[0];
                  $dropdown.addClass('select2-drop-auto-width');
                  $dropdown.css('width', '');
                  // Add scrollbar width to dropdown if vertical scrollbar is present
                  dropWidth = $dropdown.outerWidth(false) + (resultsListNode.scrollHeight === resultsListNode.clientHeight ? 0 : scrollBarDimensions.width);
                  dropWidth > width ? width = dropWidth : dropWidth = width;
                  enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight;
              }
              else {
                  this.container.removeClass('select2-drop-auto-width');
              }
  
              //console.log("below/ droptop:", dropTop, "dropHeight", dropHeight, "sum", (dropTop+dropHeight)+" viewport bottom", viewportBottom, "enough?", enoughRoomBelow);
              //console.log("above/ offset.top", offset.top, "dropHeight", dropHeight, "top", (offset.top-dropHeight), "scrollTop", this.body().scrollTop(), "enough?", enoughRoomAbove);
  
              // fix positioning when body has an offset and is not position: static
              if (this.body().css('position') !== 'static') {
                  bodyOffset = this.body().offset();
                  dropTop -= bodyOffset.top;
                  dropLeft -= bodyOffset.left;
              }
  
              if (!enoughRoomOnRight) {
                  dropLeft = offset.left + width - dropWidth;
              }
  
              css =  {
                  left: dropLeft,
                  width: width
              };
  
              if (above) {
                  css.bottom = windowHeight - offset.top;
                  css.top = 'auto';
                  this.container.addClass("select2-drop-above");
                  $dropdown.addClass("select2-drop-above");
              }
              else {
                  css.top = dropTop;
                  css.bottom = 'auto';
                  this.container.removeClass("select2-drop-above");
                  $dropdown.removeClass("select2-drop-above");
              }
              css = $.extend(css, evaluate(this.opts.dropdownCss));
  
              $dropdown.css(css);
          },
  
          // abstract
          shouldOpen: function() {
              var event;
  
              if (this.opened()) return false;
  
              if (this._enabled === false || this._readonly === true) return false;
  
              event = $.Event("select2-opening");
              this.opts.element.trigger(event);
              return !event.isDefaultPrevented();
          },
  
          // abstract
          clearDropdownAlignmentPreference: function() {
              // clear the classes used to figure out the preference of where the dropdown should be opened
              this.container.removeClass("select2-drop-above");
              this.dropdown.removeClass("select2-drop-above");
          },
  
          /**
           * Opens the dropdown
           *
           * @return {Boolean} whether or not dropdown was opened. This method will return false if, for example,
           * the dropdown is already open, or if the 'open' event listener on the element called preventDefault().
           */
          // abstract
          open: function () {
  
              if (!this.shouldOpen()) return false;
  
              this.opening();
  
              return true;
          },
  
          /**
           * Performs the opening of the dropdown
           */
          // abstract
          opening: function() {
              var cid = this.containerId,
                  scroll = "scroll." + cid,
                  resize = "resize."+cid,
                  orient = "orientationchange."+cid,
                  mask;
  
              this.container.addClass("select2-dropdown-open").addClass("select2-container-active");
  
              this.clearDropdownAlignmentPreference();
  
              if(this.dropdown[0] !== this.body().children().last()[0]) {
                  this.dropdown.detach().appendTo(this.body());
              }
  
              // create the dropdown mask if doesnt already exist
              mask = $("#select2-drop-mask");
              if (mask.length == 0) {
                  mask = $(document.createElement("div"));
                  mask.attr("id","select2-drop-mask").attr("class","select2-drop-mask");
                  mask.hide();
                  mask.appendTo(this.body());
                  mask.on("mousedown touchstart click", function (e) {
                      var dropdown = $("#select2-drop"), self;
                      if (dropdown.length > 0) {
                          self=dropdown.data("select2");
                          if (self.opts.selectOnBlur) {
                              self.selectHighlighted({noFocus: true});
                          }
                          self.close({focus:true});
                          e.preventDefault();
                          e.stopPropagation();
                      }
                  });
              }
  
              // ensure the mask is always right before the dropdown
              if (this.dropdown.prev()[0] !== mask[0]) {
                  this.dropdown.before(mask);
              }
  
              // move the global id to the correct dropdown
              $("#select2-drop").removeAttr("id");
              this.dropdown.attr("id", "select2-drop");
  
              // show the elements
              mask.show();
  
              this.positionDropdown();
              this.dropdown.show();
              this.positionDropdown();
  
              this.dropdown.addClass("select2-drop-active");
  
              // attach listeners to events that can change the position of the container and thus require
              // the position of the dropdown to be updated as well so it does not come unglued from the container
              var that = this;
              this.container.parents().add(window).each(function () {
                  $(this).on(resize+" "+scroll+" "+orient, function (e) {
                      that.positionDropdown();
                  });
              });
  
  
          },
  
          // abstract
          close: function () {
              if (!this.opened()) return;
  
              var cid = this.containerId,
                  scroll = "scroll." + cid,
                  resize = "resize."+cid,
                  orient = "orientationchange."+cid;
  
              // unbind event listeners
              this.container.parents().add(window).each(function () { $(this).off(scroll).off(resize).off(orient); });
  
              this.clearDropdownAlignmentPreference();
  
              $("#select2-drop-mask").hide();
              this.dropdown.removeAttr("id"); // only the active dropdown has the select2-drop id
              this.dropdown.hide();
              this.container.removeClass("select2-dropdown-open").removeClass("select2-container-active");
              this.results.empty();
  
  
              this.clearSearch();
              this.search.removeClass("select2-active");
              this.opts.element.trigger($.Event("select2-close"));
          },
  
          /**
           * Opens control, sets input value, and updates results.
           */
          // abstract
          externalSearch: function (term) {
              this.open();
              this.search.val(term);
              this.updateResults(false);
          },
  
          // abstract
          clearSearch: function () {
  
          },
  
          //abstract
          getMaximumSelectionSize: function() {
              return evaluate(this.opts.maximumSelectionSize);
          },
  
          // abstract
          ensureHighlightVisible: function () {
              var results = this.results, children, index, child, hb, rb, y, more;
  
              index = this.highlight();
  
              if (index < 0) return;
  
              if (index == 0) {
  
                  // if the first element is highlighted scroll all the way to the top,
                  // that way any unselectable headers above it will also be scrolled
                  // into view
  
                  results.scrollTop(0);
                  return;
              }
  
              children = this.findHighlightableChoices().find('.select2-result-label');
  
              child = $(children[index]);
  
              hb = child.offset().top + child.outerHeight(true);
  
              // if this is the last child lets also make sure select2-more-results is visible
              if (index === children.length - 1) {
                  more = results.find("li.select2-more-results");
                  if (more.length > 0) {
                      hb = more.offset().top + more.outerHeight(true);
                  }
              }
  
              rb = results.offset().top + results.outerHeight(true);
              if (hb > rb) {
                  results.scrollTop(results.scrollTop() + (hb - rb));
              }
              y = child.offset().top - results.offset().top;
  
              // make sure the top of the element is visible
              if (y < 0 && child.css('display') != 'none' ) {
                  results.scrollTop(results.scrollTop() + y); // y is negative
              }
          },
  
          // abstract
          findHighlightableChoices: function() {
              return this.results.find(".select2-result-selectable:not(.select2-disabled, .select2-selected)");
          },
  
          // abstract
          moveHighlight: function (delta) {
              var choices = this.findHighlightableChoices(),
                  index = this.highlight();
  
              while (index > -1 && index < choices.length) {
                  index += delta;
                  var choice = $(choices[index]);
                  if (choice.hasClass("select2-result-selectable") && !choice.hasClass("select2-disabled") && !choice.hasClass("select2-selected")) {
                      this.highlight(index);
                      break;
                  }
              }
          },
  
          // abstract
          highlight: function (index) {
              var choices = this.findHighlightableChoices(),
                  choice,
                  data;
  
              if (arguments.length === 0) {
                  return indexOf(choices.filter(".select2-highlighted")[0], choices.get());
              }
  
              if (index >= choices.length) index = choices.length - 1;
              if (index < 0) index = 0;
  
              this.removeHighlight();
  
              choice = $(choices[index]);
              choice.addClass("select2-highlighted");
  
              this.ensureHighlightVisible();
  
              data = choice.data("select2-data");
              if (data) {
                  this.opts.element.trigger({ type: "select2-highlight", val: this.id(data), choice: data });
              }
          },
  
          removeHighlight: function() {
              this.results.find(".select2-highlighted").removeClass("select2-highlighted");
          },
  
          // abstract
          countSelectableResults: function() {
              return this.findHighlightableChoices().length;
          },
  
          // abstract
          highlightUnderEvent: function (event) {
              var el = $(event.target).closest(".select2-result-selectable");
              if (el.length > 0 && !el.is(".select2-highlighted")) {
                  var choices = this.findHighlightableChoices();
                  this.highlight(choices.index(el));
              } else if (el.length == 0) {
                  // if we are over an unselectable item remove all highlights
                  this.removeHighlight();
              }
          },
  
          // abstract
          loadMoreIfNeeded: function () {
              var results = this.results,
                  more = results.find("li.select2-more-results"),
                  below, // pixels the element is below the scroll fold, below==0 is when the element is starting to be visible
                  page = this.resultsPage + 1,
                  self=this,
                  term=this.search.val(),
                  context=this.context;
  
              if (more.length === 0) return;
              below = more.offset().top - results.offset().top - results.height();
  
              if (below <= this.opts.loadMorePadding) {
                  more.addClass("select2-active");
                  this.opts.query({
                      element: this.opts.element,
                      term: term,
                      page: page,
                      context: context,
                      matcher: this.opts.matcher,
                      callback: this.bind(function (data) {
  
                          // ignore a response if the select2 has been closed before it was received
                          if (!self.opened()) return;
  
  
                          self.opts.populateResults.call(this, results, data.results, {term: term, page: page, context:context});
                          self.postprocessResults(data, false, false);
  
                          if (data.more===true) {
                              more.detach().appendTo(results).text(self.opts.formatLoadMore(page+1));
                              window.setTimeout(function() { self.loadMoreIfNeeded(); }, 10);
                          } else {
                              more.remove();
                          }
                          self.positionDropdown();
                          self.resultsPage = page;
                          self.context = data.context;
                          this.opts.element.trigger({ type: "select2-loaded", items: data });
                      })});
              }
          },
  
          /**
           * Default tokenizer function which does nothing
           */
          tokenize: function() {
  
          },
  
          /**
           * @param initial whether or not this is the call to this method right after the dropdown has been opened
           */
          // abstract
          updateResults: function (initial) {
              var search = this.search,
                  results = this.results,
                  opts = this.opts,
                  data,
                  self = this,
                  input,
                  term = search.val(),
                  lastTerm = $.data(this.container, "select2-last-term"),
              // sequence number used to drop out-of-order responses
                  queryNumber;
  
              // prevent duplicate queries against the same term
              if (initial !== true && lastTerm && equal(term, lastTerm)) return;
  
              $.data(this.container, "select2-last-term", term);
  
              // if the search is currently hidden we do not alter the results
              if (initial !== true && (this.showSearchInput === false || !this.opened())) {
                  return;
              }
  
              function postRender() {
                  search.removeClass("select2-active");
                  self.positionDropdown();
              }
  
              function render(html) {
                  results.html(html);
                  postRender();
              }
  
              queryNumber = ++this.queryCount;
  
              var maxSelSize = this.getMaximumSelectionSize();
              if (maxSelSize >=1) {
                  data = this.data();
                  if ($.isArray(data) && data.length >= maxSelSize && checkFormatter(opts.formatSelectionTooBig, "formatSelectionTooBig")) {
                      render("<li class='select2-selection-limit'>" + opts.formatSelectionTooBig(maxSelSize) + "</li>");
                      return;
                  }
              }
  
              if (search.val().length < opts.minimumInputLength) {
                  if (checkFormatter(opts.formatInputTooShort, "formatInputTooShort")) {
                      render("<li class='select2-no-results'>" + opts.formatInputTooShort(search.val(), opts.minimumInputLength) + "</li>");
                  } else {
                      render("");
                  }
                  if (initial && this.showSearch) this.showSearch(true);
                  return;
              }
  
              if (opts.maximumInputLength && search.val().length > opts.maximumInputLength) {
                  if (checkFormatter(opts.formatInputTooLong, "formatInputTooLong")) {
                      render("<li class='select2-no-results'>" + opts.formatInputTooLong(search.val(), opts.maximumInputLength) + "</li>");
                  } else {
                      render("");
                  }
                  return;
              }
  
              if (opts.formatSearching && this.findHighlightableChoices().length === 0) {
                  render("<li class='select2-searching'>" + opts.formatSearching() + "</li>");
              }
  
              search.addClass("select2-active");
  
              this.removeHighlight();
  
              // give the tokenizer a chance to pre-process the input
              input = this.tokenize();
              if (input != undefined && input != null) {
                  search.val(input);
              }
  
              this.resultsPage = 1;
  
              opts.query({
                  element: opts.element,
                  term: search.val(),
                  page: this.resultsPage,
                  context: null,
                  matcher: opts.matcher,
                  callback: this.bind(function (data) {
                      var def; // default choice
  
                      // ignore old responses
                      if (queryNumber != this.queryCount) {
                          return;
                      }
  
                      // ignore a response if the select2 has been closed before it was received
                      if (!this.opened()) {
                          this.search.removeClass("select2-active");
                          return;
                      }
  
                      // save context, if any
                      this.context = (data.context===undefined) ? null : data.context;
                      // create a default choice and prepend it to the list
                      if (this.opts.createSearchChoice && search.val() !== "") {
                          def = this.opts.createSearchChoice.call(self, search.val(), data.results);
                          if (def !== undefined && def !== null && self.id(def) !== undefined && self.id(def) !== null) {
                              if ($(data.results).filter(
                                  function () {
                                      return equal(self.id(this), self.id(def));
                                  }).length === 0) {
                                  data.results.unshift(def);
                              }
                          }
                      }
  
                      if (data.results.length === 0 && checkFormatter(opts.formatNoMatches, "formatNoMatches")) {
                          render("<li class='select2-no-results'>" + opts.formatNoMatches(search.val()) + "</li>");
                          return;
                      }
  
                      results.empty();
                      self.opts.populateResults.call(this, results, data.results, {term: search.val(), page: this.resultsPage, context:null});
  
                      if (data.more === true && checkFormatter(opts.formatLoadMore, "formatLoadMore")) {
                          results.append("<li class='select2-more-results'>" + self.opts.escapeMarkup(opts.formatLoadMore(this.resultsPage)) + "</li>");
                          window.setTimeout(function() { self.loadMoreIfNeeded(); }, 10);
                      }
  
                      this.postprocessResults(data, initial);
  
                      postRender();
  
                      this.opts.element.trigger({ type: "select2-loaded", items: data });
                  })});
          },
  
          // abstract
          cancel: function () {
              this.close();
          },
  
          // abstract
          blur: function () {
              // if selectOnBlur == true, select the currently highlighted option
              if (this.opts.selectOnBlur)
                  this.selectHighlighted({noFocus: true});
  
              this.close();
              this.container.removeClass("select2-container-active");
              // synonymous to .is(':focus'), which is available in jquery >= 1.6
              if (this.search[0] === document.activeElement) { this.search.blur(); }
              this.clearSearch();
              this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
          },
  
          // abstract
          focusSearch: function () {
              focus(this.search);
          },
  
          // abstract
          selectHighlighted: function (options) {
              var index=this.highlight(),
                  highlighted=this.results.find(".select2-highlighted"),
                  data = highlighted.closest('.select2-result').data("select2-data");
  
              if (data) {
                  this.highlight(index);
                  this.onSelect(data, options);
              } else if (options && options.noFocus) {
                  this.close();
              }
          },
  
          // abstract
          getPlaceholder: function () {
              var placeholderOption;
              return this.opts.element.attr("placeholder") ||
                  this.opts.element.attr("data-placeholder") || // jquery 1.4 compat
                  this.opts.element.data("placeholder") ||
                  this.opts.placeholder ||
                  ((placeholderOption = this.getPlaceholderOption()) !== undefined ? placeholderOption.text() : undefined);
          },
  
          // abstract
          getPlaceholderOption: function() {
              if (this.select) {
                  var firstOption = this.select.children('option').first();
                  if (this.opts.placeholderOption !== undefined ) {
                      //Determine the placeholder option based on the specified placeholderOption setting
                      return (this.opts.placeholderOption === "first" && firstOption) ||
                          (typeof this.opts.placeholderOption === "function" && this.opts.placeholderOption(this.select));
                  } else if (firstOption.text() === "" && firstOption.val() === "") {
                      //No explicit placeholder option specified, use the first if it's blank
                      return firstOption;
                  }
              }
          },
  
          /**
           * Get the desired width for the container element.  This is
           * derived first from option `width` passed to select2, then
           * the inline 'style' on the original element, and finally
           * falls back to the jQuery calculated element width.
           */
          // abstract
          initContainerWidth: function () {
              function resolveContainerWidth() {
                  var style, attrs, matches, i, l, attr;
  
                  if (this.opts.width === "off") {
                      return null;
                  } else if (this.opts.width === "element"){
                      return this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px';
                  } else if (this.opts.width === "copy" || this.opts.width === "resolve") {
                      // check if there is inline style on the element that contains width
                      style = this.opts.element.attr('style');
                      if (style !== undefined) {
                          attrs = style.split(';');
                          for (i = 0, l = attrs.length; i < l; i = i + 1) {
                              attr = attrs[i].replace(/\s/g, '');
                              matches = attr.match(/^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i);
                              if (matches !== null && matches.length >= 1)
                                  return matches[1];
                          }
                      }
  
                      if (this.opts.width === "resolve") {
                          // next check if css('width') can resolve a width that is percent based, this is sometimes possible
                          // when attached to input type=hidden or elements hidden via css
                          style = this.opts.element.css('width');
                          if (style.indexOf("%") > 0) return style;
  
                          // finally, fallback on the calculated width of the element
                          return (this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px');
                      }
  
                      return null;
                  } else if ($.isFunction(this.opts.width)) {
                      return this.opts.width();
                  } else {
                      return this.opts.width;
                  }
              };
  
              var width = resolveContainerWidth.call(this);
              if (width !== null) {
                  this.container.css("width", width);
              }
          }
      });
  
      SingleSelect2 = clazz(AbstractSelect2, {
  
          // single
  
          createContainer: function () {
              var container = $(document.createElement("div")).attr({
                  "class": "select2-container"
              }).html([
                      "<a href='javascript:void(0)' onclick='return false;' class='select2-choice' tabindex='-1'>",
                      "   <span class='select2-chosen'>&nbsp;</span><abbr class='select2-search-choice-close'></abbr>",
                      "   <span class='select2-arrow'><b></b></span>",
                      "</a>",
                      "<input class='select2-focusser select2-offscreen' type='text'/>",
                      "<div class='select2-drop select2-display-none'>",
                      "   <div class='select2-search'>",
                      "       <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'/>",
                      "   </div>",
                      "   <ul class='select2-results'>",
                      "   </ul>",
                      "</div>"].join(""));
              return container;
          },
  
          // single
          enableInterface: function() {
              if (this.parent.enableInterface.apply(this, arguments)) {
                  this.focusser.prop("disabled", !this.isInterfaceEnabled());
              }
          },
  
          // single
          opening: function () {
              var el, range, len;
  
              if (this.opts.minimumResultsForSearch >= 0) {
                  this.showSearch(true);
              }
  
              this.parent.opening.apply(this, arguments);
  
              if (this.showSearchInput !== false) {
                  // IE appends focusser.val() at the end of field :/ so we manually insert it at the beginning using a range
                  // all other browsers handle this just fine
  
                  this.search.val(this.focusser.val());
              }
              this.search.focus();
              // move the cursor to the end after focussing, otherwise it will be at the beginning and
              // new text will appear *before* focusser.val()
              el = this.search.get(0);
              if (el.createTextRange) {
                  range = el.createTextRange();
                  range.collapse(false);
                  range.select();
              } else if (el.setSelectionRange) {
                  len = this.search.val().length;
                  el.setSelectionRange(len, len);
              }
  
              // initializes search's value with nextSearchTerm (if defined by user)
              // ignore nextSearchTerm if the dropdown is opened by the user pressing a letter
              if(this.search.val() === "") {
                  if(this.nextSearchTerm != undefined){
                      this.search.val(this.nextSearchTerm);
                      this.search.select();
                  }
              }
  
              this.focusser.prop("disabled", true).val("");
              this.updateResults(true);
              this.opts.element.trigger($.Event("select2-open"));
          },
  
          // single
          close: function (params) {
              if (!this.opened()) return;
              this.parent.close.apply(this, arguments);
  
              params = params || {focus: true};
              this.focusser.removeAttr("disabled");
  
              if (params.focus) {
                  this.focusser.focus();
              }
          },
  
          // single
          focus: function () {
              if (this.opened()) {
                  this.close();
              } else {
                  this.focusser.removeAttr("disabled");
                  this.focusser.focus();
              }
          },
  
          // single
          isFocused: function () {
              return this.container.hasClass("select2-container-active");
          },
  
          // single
          cancel: function () {
              this.parent.cancel.apply(this, arguments);
              this.focusser.removeAttr("disabled");
              this.focusser.focus();
          },
  
          // single
          destroy: function() {
              $("label[for='" + this.focusser.attr('id') + "']")
                  .attr('for', this.opts.element.attr("id"));
              this.parent.destroy.apply(this, arguments);
          },
  
          // single
          initContainer: function () {
  
              var selection,
                  container = this.container,
                  dropdown = this.dropdown;
  
              if (this.opts.minimumResultsForSearch < 0) {
                  this.showSearch(false);
              } else {
                  this.showSearch(true);
              }
  
              this.selection = selection = container.find(".select2-choice");
  
              this.focusser = container.find(".select2-focusser");
  
              // rewrite labels from original element to focusser
              this.focusser.attr("id", "s2id_autogen"+nextUid());
  
              $("label[for='" + this.opts.element.attr("id") + "']")
                  .attr('for', this.focusser.attr('id'));
  
              this.focusser.attr("tabindex", this.elementTabIndex);
  
              this.search.on("keydown", this.bind(function (e) {
                  if (!this.isInterfaceEnabled()) return;
  
                  if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
                      // prevent the page from scrolling
                      killEvent(e);
                      return;
                  }
  
                  switch (e.which) {
                      case KEY.UP:
                      case KEY.DOWN:
                          this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
                          killEvent(e);
                          return;
                      case KEY.ENTER:
                          this.selectHighlighted();
                          killEvent(e);
                          return;
                      case KEY.TAB:
                          this.selectHighlighted({noFocus: true});
                          return;
                      case KEY.ESC:
                          this.cancel(e);
                          killEvent(e);
                          return;
                  }
              }));
  
              this.search.on("blur", this.bind(function(e) {
                  // a workaround for chrome to keep the search field focussed when the scroll bar is used to scroll the dropdown.
                  // without this the search field loses focus which is annoying
                  if (document.activeElement === this.body().get(0)) {
                      window.setTimeout(this.bind(function() {
                          this.search.focus();
                      }), 0);
                  }
              }));
  
              this.focusser.on("keydown", this.bind(function (e) {
                  if (!this.isInterfaceEnabled()) return;
  
                  if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e) || e.which === KEY.ESC) {
                      return;
                  }
  
                  if (this.opts.openOnEnter === false && e.which === KEY.ENTER) {
                      killEvent(e);
                      return;
                  }
  
                  if (e.which == KEY.DOWN || e.which == KEY.UP
                      || (e.which == KEY.ENTER && this.opts.openOnEnter)) {
  
                      if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) return;
  
                      this.open();
                      killEvent(e);
                      return;
                  }
  
                  if (e.which == KEY.DELETE || e.which == KEY.BACKSPACE) {
                      if (this.opts.allowClear) {
                          this.clear();
                      }
                      killEvent(e);
                      return;
                  }
              }));
  
  
              installKeyUpChangeEvent(this.focusser);
              this.focusser.on("keyup-change input", this.bind(function(e) {
                  if (this.opts.minimumResultsForSearch >= 0) {
                      e.stopPropagation();
                      if (this.opened()) return;
                      this.open();
                  }
              }));
  
              selection.on("mousedown", "abbr", this.bind(function (e) {
                  if (!this.isInterfaceEnabled()) return;
                  this.clear();
                  killEventImmediately(e);
                  this.close();
                  this.selection.focus();
              }));
  
              selection.on("mousedown", this.bind(function (e) {
  
                  if (!this.container.hasClass("select2-container-active")) {
                      this.opts.element.trigger($.Event("select2-focus"));
                  }
  
                  if (this.opened()) {
                      this.close();
                  } else if (this.isInterfaceEnabled()) {
                      this.open();
                  }
  
                  killEvent(e);
              }));
  
              dropdown.on("mousedown", this.bind(function() { this.search.focus(); }));
  
              selection.on("focus", this.bind(function(e) {
                  killEvent(e);
              }));
  
              this.focusser.on("focus", this.bind(function(){
                      if (!this.container.hasClass("select2-container-active")) {
                          this.opts.element.trigger($.Event("select2-focus"));
                      }
                      this.container.addClass("select2-container-active");
                  })).on("blur", this.bind(function() {
                      if (!this.opened()) {
                          this.container.removeClass("select2-container-active");
                          this.opts.element.trigger($.Event("select2-blur"));
                      }
                  }));
              this.search.on("focus", this.bind(function(){
                  if (!this.container.hasClass("select2-container-active")) {
                      this.opts.element.trigger($.Event("select2-focus"));
                  }
                  this.container.addClass("select2-container-active");
              }));
  
              this.initContainerWidth();
              this.opts.element.addClass("select2-offscreen");
              this.setPlaceholder();
  
          },
  
          // single
          clear: function(triggerChange) {
              var data=this.selection.data("select2-data");
              if (data) { // guard against queued quick consecutive clicks
                  var evt = $.Event("select2-clearing");
                  this.opts.element.trigger(evt);
                  if (evt.isDefaultPrevented()) {
                      return;
                  }
                  var placeholderOption = this.getPlaceholderOption();
                  this.opts.element.val(placeholderOption ? placeholderOption.val() : "");
                  this.selection.find(".select2-chosen").empty();
                  this.selection.removeData("select2-data");
                  this.setPlaceholder();
  
                  if (triggerChange !== false){
                      this.opts.element.trigger({ type: "select2-removed", val: this.id(data), choice: data });
                      this.triggerChange({removed:data});
                  }
              }
          },
  
          /**
           * Sets selection based on source element's value
           */
          // single
          initSelection: function () {
              var selected;
              if (this.isPlaceholderOptionSelected()) {
                  this.updateSelection(null);
                  this.close();
                  this.setPlaceholder();
              } else {
                  var self = this;
                  this.opts.initSelection.call(null, this.opts.element, function(selected){
                      if (selected !== undefined && selected !== null) {
                          self.updateSelection(selected);
                          self.close();
                          self.setPlaceholder();
                      }
                  });
              }
          },
  
          isPlaceholderOptionSelected: function() {
              var placeholderOption;
              if (!this.getPlaceholder()) return false; // no placeholder specified so no option should be considered
              return ((placeholderOption = this.getPlaceholderOption()) !== undefined && placeholderOption.prop("selected"))
                  || (this.opts.element.val() === "")
                  || (this.opts.element.val() === undefined)
                  || (this.opts.element.val() === null);
          },
  
          // single
          prepareOpts: function () {
              var opts = this.parent.prepareOpts.apply(this, arguments),
                  self=this;
  
              if (opts.element.get(0).tagName.toLowerCase() === "select") {
                  // install the selection initializer
                  opts.initSelection = function (element, callback) {
                      var selected = element.find("option").filter(function() { return this.selected });
                      // a single select box always has a value, no need to null check 'selected'
                      callback(self.optionToData(selected));
                  };
              } else if ("data" in opts) {
                  // install default initSelection when applied to hidden input and data is local
                  opts.initSelection = opts.initSelection || function (element, callback) {
                      var id = element.val();
                      //search in data by id, storing the actual matching item
                      var match = null;
                      opts.query({
                          matcher: function(term, text, el){
                              var is_match = equal(id, opts.id(el));
                              if (is_match) {
                                  match = el;
                              }
                              return is_match;
                          },
                          callback: !$.isFunction(callback) ? $.noop : function() {
                              callback(match);
                          }
                      });
                  };
              }
  
              return opts;
          },
  
          // single
          getPlaceholder: function() {
              // if a placeholder is specified on a single select without a valid placeholder option ignore it
              if (this.select) {
                  if (this.getPlaceholderOption() === undefined) {
                      return undefined;
                  }
              }
  
              return this.parent.getPlaceholder.apply(this, arguments);
          },
  
          // single
          setPlaceholder: function () {
              var placeholder = this.getPlaceholder();
  
              if (this.isPlaceholderOptionSelected() && placeholder !== undefined) {
  
                  // check for a placeholder option if attached to a select
                  if (this.select && this.getPlaceholderOption() === undefined) return;
  
                  this.selection.find(".select2-chosen").html(this.opts.escapeMarkup(placeholder));
  
                  this.selection.addClass("select2-default");
  
                  this.container.removeClass("select2-allowclear");
              }
          },
  
          // single
          postprocessResults: function (data, initial, noHighlightUpdate) {
              var selected = 0, self = this, showSearchInput = true;
  
              // find the selected element in the result list
  
              this.findHighlightableChoices().each2(function (i, elm) {
                  if (equal(self.id(elm.data("select2-data")), self.opts.element.val())) {
                      selected = i;
                      return false;
                  }
              });
  
              // and highlight it
              if (noHighlightUpdate !== false) {
                  if (initial === true && selected >= 0) {
                      this.highlight(selected);
                  } else {
                      this.highlight(0);
                  }
              }
  
              // hide the search box if this is the first we got the results and there are enough of them for search
  
              if (initial === true) {
                  var min = this.opts.minimumResultsForSearch;
                  if (min >= 0) {
                      this.showSearch(countResults(data.results) >= min);
                  }
              }
          },
  
          // single
          showSearch: function(showSearchInput) {
              if (this.showSearchInput === showSearchInput) return;
  
              this.showSearchInput = showSearchInput;
  
              this.dropdown.find(".select2-search").toggleClass("select2-search-hidden", !showSearchInput);
              this.dropdown.find(".select2-search").toggleClass("select2-offscreen", !showSearchInput);
              //add "select2-with-searchbox" to the container if search box is shown
              $(this.dropdown, this.container).toggleClass("select2-with-searchbox", showSearchInput);
          },
  
          // single
          onSelect: function (data, options) {
  
              if (!this.triggerSelect(data)) { return; }
  
              var old = this.opts.element.val(),
                  oldData = this.data();
  
              this.opts.element.val(this.id(data));
              this.updateSelection(data);
  
              this.opts.element.trigger({ type: "select2-selected", val: this.id(data), choice: data });
  
              this.nextSearchTerm = this.opts.nextSearchTerm(data, this.search.val());
              this.close();
  
              if (!options || !options.noFocus)
                  this.focusser.focus();
  
              if (!equal(old, this.id(data))) { this.triggerChange({added:data,removed:oldData}); }
          },
  
          // single
          updateSelection: function (data) {
  
              var container=this.selection.find(".select2-chosen"), formatted, cssClass;
  
              this.selection.data("select2-data", data);
  
              container.empty();
              if (data !== null) {
                  formatted=this.opts.formatSelection(data, container, this.opts.escapeMarkup);
              }
              if (formatted !== undefined) {
                  container.append(formatted);
              }
              cssClass=this.opts.formatSelectionCssClass(data, container);
              if (cssClass !== undefined) {
                  container.addClass(cssClass);
              }
  
              this.selection.removeClass("select2-default");
  
              if (this.opts.allowClear && this.getPlaceholder() !== undefined) {
                  this.container.addClass("select2-allowclear");
              }
          },
  
          // single
          val: function () {
              var val,
                  triggerChange = false,
                  data = null,
                  self = this,
                  oldData = this.data();
  
              if (arguments.length === 0) {
                  return this.opts.element.val();
              }
  
              val = arguments[0];
  
              if (arguments.length > 1) {
                  triggerChange = arguments[1];
              }
  
              if (this.select) {
                  this.select
                      .val(val)
                      .find("option").filter(function() { return this.selected }).each2(function (i, elm) {
                          data = self.optionToData(elm);
                          return false;
                      });
                  this.updateSelection(data);
                  this.setPlaceholder();
                  if (triggerChange) {
                      this.triggerChange({added: data, removed:oldData});
                  }
              } else {
                  // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
                  if (!val && val !== 0) {
                      this.clear(triggerChange);
                      return;
                  }
                  if (this.opts.initSelection === undefined) {
                      throw new Error("cannot call val() if initSelection() is not defined");
                  }
                  this.opts.element.val(val);
                  this.opts.initSelection(this.opts.element, function(data){
                      self.opts.element.val(!data ? "" : self.id(data));
                      self.updateSelection(data);
                      self.setPlaceholder();
                      if (triggerChange) {
                          self.triggerChange({added: data, removed:oldData});
                      }
                  });
              }
          },
  
          // single
          clearSearch: function () {
              this.search.val("");
              this.focusser.val("");
          },
  
          // single
          data: function(value) {
              var data,
                  triggerChange = false;
  
              if (arguments.length === 0) {
                  data = this.selection.data("select2-data");
                  if (data == undefined) data = null;
                  return data;
              } else {
                  if (arguments.length > 1) {
                      triggerChange = arguments[1];
                  }
                  if (!value) {
                      this.clear(triggerChange);
                  } else {
                      data = this.data();
                      this.opts.element.val(!value ? "" : this.id(value));
                      this.updateSelection(value);
                      if (triggerChange) {
                          this.triggerChange({added: value, removed:data});
                      }
                  }
              }
          }
      });
  
      MultiSelect2 = clazz(AbstractSelect2, {
  
          // multi
          createContainer: function () {
              var container = $(document.createElement("div")).attr({
                  "class": "select2-container select2-container-multi"
              }).html([
                      "<ul class='select2-choices'>",
                      "  <li class='select2-search-field'>",
                      "    <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'>",
                      "  </li>",
                      "</ul>",
                      "<div class='select2-drop select2-drop-multi select2-display-none'>",
                      "   <ul class='select2-results'>",
                      "   </ul>",
                      "</div>"].join(""));
              return container;
          },
  
          // multi
          prepareOpts: function () {
              var opts = this.parent.prepareOpts.apply(this, arguments),
                  self=this;
  
              // TODO validate placeholder is a string if specified
  
              if (opts.element.get(0).tagName.toLowerCase() === "select") {
                  // install sthe selection initializer
                  opts.initSelection = function (element, callback) {
  
                      var data = [];
  
                      element.find("option").filter(function() { return this.selected }).each2(function (i, elm) {
                          data.push(self.optionToData(elm));
                      });
                      callback(data);
                  };
              } else if ("data" in opts) {
                  // install default initSelection when applied to hidden input and data is local
                  opts.initSelection = opts.initSelection || function (element, callback) {
                      var ids = splitVal(element.val(), opts.separator);
                      //search in data by array of ids, storing matching items in a list
                      var matches = [];
                      opts.query({
                          matcher: function(term, text, el){
                              var is_match = $.grep(ids, function(id) {
                                  return equal(id, opts.id(el));
                              }).length;
                              if (is_match) {
                                  matches.push(el);
                              }
                              return is_match;
                          },
                          callback: !$.isFunction(callback) ? $.noop : function() {
                              // reorder matches based on the order they appear in the ids array because right now
                              // they are in the order in which they appear in data array
                              var ordered = [];
                              for (var i = 0; i < ids.length; i++) {
                                  var id = ids[i];
                                  for (var j = 0; j < matches.length; j++) {
                                      var match = matches[j];
                                      if (equal(id, opts.id(match))) {
                                          ordered.push(match);
                                          matches.splice(j, 1);
                                          break;
                                      }
                                  }
                              }
                              callback(ordered);
                          }
                      });
                  };
              }
  
              return opts;
          },
  
          // multi
          selectChoice: function (choice) {
  
              var selected = this.container.find(".select2-search-choice-focus");
              if (selected.length && choice && choice[0] == selected[0]) {
  
              } else {
                  if (selected.length) {
                      this.opts.element.trigger("choice-deselected", selected);
                  }
                  selected.removeClass("select2-search-choice-focus");
                  if (choice && choice.length) {
                      this.close();
                      choice.addClass("select2-search-choice-focus");
                      this.opts.element.trigger("choice-selected", choice);
                  }
              }
          },
  
          // multi
          destroy: function() {
              $("label[for='" + this.search.attr('id') + "']")
                  .attr('for', this.opts.element.attr("id"));
              this.parent.destroy.apply(this, arguments);
          },
  
          // multi
          initContainer: function () {
  
              var selector = ".select2-choices", selection;
  
              this.searchContainer = this.container.find(".select2-search-field");
              this.selection = selection = this.container.find(selector);
  
              var _this = this;
              this.selection.on("click", ".select2-search-choice:not(.select2-locked)", function (e) {
                  //killEvent(e);
                  _this.search[0].focus();
                  _this.selectChoice($(this));
              });
  
              // rewrite labels from original element to focusser
              this.search.attr("id", "s2id_autogen"+nextUid());
              $("label[for='" + this.opts.element.attr("id") + "']")
                  .attr('for', this.search.attr('id'));
  
              this.search.on("input paste", this.bind(function() {
                  if (!this.isInterfaceEnabled()) return;
                  if (!this.opened()) {
                      this.open();
                  }
              }));
  
              this.search.attr("tabindex", this.elementTabIndex);
  
              this.keydowns = 0;
              this.search.on("keydown", this.bind(function (e) {
                  if (!this.isInterfaceEnabled()) return;
  
                  ++this.keydowns;
                  var selected = selection.find(".select2-search-choice-focus");
                  var prev = selected.prev(".select2-search-choice:not(.select2-locked)");
                  var next = selected.next(".select2-search-choice:not(.select2-locked)");
                  var pos = getCursorInfo(this.search);
  
                  if (selected.length &&
                      (e.which == KEY.LEFT || e.which == KEY.RIGHT || e.which == KEY.BACKSPACE || e.which == KEY.DELETE || e.which == KEY.ENTER)) {
                      var selectedChoice = selected;
                      if (e.which == KEY.LEFT && prev.length) {
                          selectedChoice = prev;
                      }
                      else if (e.which == KEY.RIGHT) {
                          selectedChoice = next.length ? next : null;
                      }
                      else if (e.which === KEY.BACKSPACE) {
                          this.unselect(selected.first());
                          this.search.width(10);
                          selectedChoice = prev.length ? prev : next;
                      } else if (e.which == KEY.DELETE) {
                          this.unselect(selected.first());
                          this.search.width(10);
                          selectedChoice = next.length ? next : null;
                      } else if (e.which == KEY.ENTER) {
                          selectedChoice = null;
                      }
  
                      this.selectChoice(selectedChoice);
                      killEvent(e);
                      if (!selectedChoice || !selectedChoice.length) {
                          this.open();
                      }
                      return;
                  } else if (((e.which === KEY.BACKSPACE && this.keydowns == 1)
                      || e.which == KEY.LEFT) && (pos.offset == 0 && !pos.length)) {
  
                      this.selectChoice(selection.find(".select2-search-choice:not(.select2-locked)").last());
                      killEvent(e);
                      return;
                  } else {
                      this.selectChoice(null);
                  }
  
                  if (this.opened()) {
                      switch (e.which) {
                          case KEY.UP:
                          case KEY.DOWN:
                              this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
                              killEvent(e);
                              return;
                          case KEY.ENTER:
                              this.selectHighlighted();
                              killEvent(e);
                              return;
                          case KEY.TAB:
                              this.selectHighlighted({noFocus:true});
                              this.close();
                              return;
                          case KEY.ESC:
                              this.cancel(e);
                              killEvent(e);
                              return;
                      }
                  }
  
                  if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e)
                      || e.which === KEY.BACKSPACE || e.which === KEY.ESC) {
                      return;
                  }
  
                  if (e.which === KEY.ENTER) {
                      if (this.opts.openOnEnter === false) {
                          return;
                      } else if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                          return;
                      }
                  }
  
                  this.open();
  
                  if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
                      // prevent the page from scrolling
                      killEvent(e);
                  }
  
                  if (e.which === KEY.ENTER) {
                      // prevent form from being submitted
                      killEvent(e);
                  }
  
              }));
  
              this.search.on("keyup", this.bind(function (e) {
                  this.keydowns = 0;
                  this.resizeSearch();
              })
              );
  
              this.search.on("blur", this.bind(function(e) {
                  this.container.removeClass("select2-container-active");
                  this.search.removeClass("select2-focused");
                  this.selectChoice(null);
                  if (!this.opened()) this.clearSearch();
                  e.stopImmediatePropagation();
                  this.opts.element.trigger($.Event("select2-blur"));
              }));
  
              this.container.on("click", selector, this.bind(function (e) {
                  if (!this.isInterfaceEnabled()) return;
                  if ($(e.target).closest(".select2-search-choice").length > 0) {
                      // clicked inside a select2 search choice, do not open
                      return;
                  }
                  this.selectChoice(null);
                  this.clearPlaceholder();
                  if (!this.container.hasClass("select2-container-active")) {
                      this.opts.element.trigger($.Event("select2-focus"));
                  }
                  this.open();
                  this.focusSearch();
                  e.preventDefault();
              }));
  
              this.container.on("focus", selector, this.bind(function () {
                  if (!this.isInterfaceEnabled()) return;
                  if (!this.container.hasClass("select2-container-active")) {
                      this.opts.element.trigger($.Event("select2-focus"));
                  }
                  this.container.addClass("select2-container-active");
                  this.dropdown.addClass("select2-drop-active");
                  this.clearPlaceholder();
              }));
  
              this.initContainerWidth();
              this.opts.element.addClass("select2-offscreen");
  
              // set the placeholder if necessary
              this.clearSearch();
          },
  
          // multi
          enableInterface: function() {
              if (this.parent.enableInterface.apply(this, arguments)) {
                  this.search.prop("disabled", !this.isInterfaceEnabled());
              }
          },
  
          // multi
          initSelection: function () {
              var data;
              if (this.opts.element.val() === "" && this.opts.element.text() === "") {
                  this.updateSelection([]);
                  this.close();
                  // set the placeholder if necessary
                  this.clearSearch();
              }
              if (this.select || this.opts.element.val() !== "") {
                  var self = this;
                  this.opts.initSelection.call(null, this.opts.element, function(data){
                      if (data !== undefined && data !== null) {
                          self.updateSelection(data);
                          self.close();
                          // set the placeholder if necessary
                          self.clearSearch();
                      }
                  });
              }
          },
  
          // multi
          clearSearch: function () {
              var placeholder = this.getPlaceholder(),
                  maxWidth = this.getMaxSearchWidth();
  
              if (placeholder !== undefined  && this.getVal().length === 0 && this.search.hasClass("select2-focused") === false) {
                  this.search.val(placeholder).addClass("select2-default");
                  // stretch the search box to full width of the container so as much of the placeholder is visible as possible
                  // we could call this.resizeSearch(), but we do not because that requires a sizer and we do not want to create one so early because of a firefox bug, see #944
                  this.search.width(maxWidth > 0 ? maxWidth : this.container.css("width"));
              } else {
                  this.search.val("").width(10);
              }
          },
  
          // multi
          clearPlaceholder: function () {
              if (this.search.hasClass("select2-default")) {
                  this.search.val("").removeClass("select2-default");
              }
          },
  
          // multi
          opening: function () {
              this.clearPlaceholder(); // should be done before super so placeholder is not used to search
              this.resizeSearch();
  
              this.parent.opening.apply(this, arguments);
  
              this.focusSearch();
  
              this.updateResults(true);
              this.search.focus();
              this.opts.element.trigger($.Event("select2-open"));
          },
  
          // multi
          close: function () {
              if (!this.opened()) return;
              this.parent.close.apply(this, arguments);
          },
  
          // multi
          focus: function () {
              this.close();
              this.search.focus();
          },
  
          // multi
          isFocused: function () {
              return this.search.hasClass("select2-focused");
          },
  
          // multi
          updateSelection: function (data) {
              var ids = [], filtered = [], self = this;
  
              // filter out duplicates
              $(data).each(function () {
                  if (indexOf(self.id(this), ids) < 0) {
                      ids.push(self.id(this));
                      filtered.push(this);
                  }
              });
              data = filtered;
  
              this.selection.find(".select2-search-choice").remove();
              $(data).each(function () {
                  self.addSelectedChoice(this);
              });
              self.postprocessResults();
          },
  
          // multi
          tokenize: function() {
              var input = this.search.val();
              input = this.opts.tokenizer.call(this, input, this.data(), this.bind(this.onSelect), this.opts);
              if (input != null && input != undefined) {
                  this.search.val(input);
                  if (input.length > 0) {
                      this.open();
                  }
              }
  
          },
  
          // multi
          onSelect: function (data, options) {
  
              if (!this.triggerSelect(data)) { return; }
  
              this.addSelectedChoice(data);
  
              this.opts.element.trigger({ type: "selected", val: this.id(data), choice: data });
  
              if (this.select || !this.opts.closeOnSelect) this.postprocessResults(data, false, this.opts.closeOnSelect===true);
  
              if (this.opts.closeOnSelect) {
                  this.close();
                  this.search.width(10);
              } else {
                  if (this.countSelectableResults()>0) {
                      this.search.width(10);
                      this.resizeSearch();
                      if (this.getMaximumSelectionSize() > 0 && this.val().length >= this.getMaximumSelectionSize()) {
                          // if we reached max selection size repaint the results so choices
                          // are replaced with the max selection reached message
                          this.updateResults(true);
                      }
                      this.positionDropdown();
                  } else {
                      // if nothing left to select close
                      this.close();
                      this.search.width(10);
                  }
              }
  
              // since its not possible to select an element that has already been
              // added we do not need to check if this is a new element before firing change
              this.triggerChange({ added: data });
  
              if (!options || !options.noFocus)
                  this.focusSearch();
          },
  
          // multi
          cancel: function () {
              this.close();
              this.focusSearch();
          },
  
          addSelectedChoice: function (data) {
              var enableChoice = !data.locked,
                  enabledItem = $(
                      "<li class='select2-search-choice'>" +
                          "    <div></div>" +
                          "    <a href='#' onclick='return false;' class='select2-search-choice-close' tabindex='-1'></a>" +
                          "</li>"),
                  disabledItem = $(
                      "<li class='select2-search-choice select2-locked'>" +
                          "<div></div>" +
                          "</li>");
              var choice = enableChoice ? enabledItem : disabledItem,
                  id = this.id(data),
                  val = this.getVal(),
                  formatted,
                  cssClass;
  
              formatted=this.opts.formatSelection(data, choice.find("div"), this.opts.escapeMarkup);
              if (formatted != undefined) {
                  choice.find("div").replaceWith("<div>"+formatted+"</div>");
              }
              cssClass=this.opts.formatSelectionCssClass(data, choice.find("div"));
              if (cssClass != undefined) {
                  choice.addClass(cssClass);
              }
  
              if(enableChoice){
                  choice.find(".select2-search-choice-close")
                      .on("mousedown", killEvent)
                      .on("click dblclick", this.bind(function (e) {
                          if (!this.isInterfaceEnabled()) return;
  
                          $(e.target).closest(".select2-search-choice").fadeOut('fast', this.bind(function(){
                              this.unselect($(e.target));
                              this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
                              this.close();
                              this.focusSearch();
                          })).dequeue();
                          killEvent(e);
                      })).on("focus", this.bind(function () {
                          if (!this.isInterfaceEnabled()) return;
                          this.container.addClass("select2-container-active");
                          this.dropdown.addClass("select2-drop-active");
                      }));
              }
  
              choice.data("select2-data", data);
              choice.insertBefore(this.searchContainer);
  
              val.push(id);
              this.setVal(val);
          },
  
          // multi
          unselect: function (selected) {
              var val = this.getVal(),
                  data,
                  index;
              selected = selected.closest(".select2-search-choice");
  
              if (selected.length === 0) {
                  throw "Invalid argument: " + selected + ". Must be .select2-search-choice";
              }
  
              data = selected.data("select2-data");
  
              if (!data) {
                  // prevent a race condition when the 'x' is clicked really fast repeatedly the event can be queued
                  // and invoked on an element already removed
                  return;
              }
  
              while((index = indexOf(this.id(data), val)) >= 0) {
                  val.splice(index, 1);
                  this.setVal(val);
                  if (this.select) this.postprocessResults();
              }
  
              var evt = $.Event("select2-removing");
              evt.val = this.id(data);
              evt.choice = data;
              this.opts.element.trigger(evt);
  
              if (evt.isDefaultPrevented()) {
                  return;
              }
  
              selected.remove();
  
              this.opts.element.trigger({ type: "select2-removed", val: this.id(data), choice: data });
              this.triggerChange({ removed: data });
          },
  
          // multi
          postprocessResults: function (data, initial, noHighlightUpdate) {
              var val = this.getVal(),
                  choices = this.results.find(".select2-result"),
                  compound = this.results.find(".select2-result-with-children"),
                  self = this;
  
              choices.each2(function (i, choice) {
                  var id = self.id(choice.data("select2-data"));
                  if (indexOf(id, val) >= 0) {
                      choice.addClass("select2-selected");
                      // mark all children of the selected parent as selected
                      choice.find(".select2-result-selectable").addClass("select2-selected");
                  }
              });
  
              compound.each2(function(i, choice) {
                  // hide an optgroup if it doesnt have any selectable children
                  if (!choice.is('.select2-result-selectable')
                      && choice.find(".select2-result-selectable:not(.select2-selected)").length === 0) {
                      choice.addClass("select2-selected");
                  }
              });
  
              if (this.highlight() == -1 && noHighlightUpdate !== false){
                  self.highlight(0);
              }
  
              //If all results are chosen render formatNoMAtches
              if(!this.opts.createSearchChoice && !choices.filter('.select2-result:not(.select2-selected)').length > 0){
                  if(!data || data && !data.more && this.results.find(".select2-no-results").length === 0) {
                      if (checkFormatter(self.opts.formatNoMatches, "formatNoMatches")) {
                          this.results.append("<li class='select2-no-results'>" + self.opts.formatNoMatches(self.search.val()) + "</li>");
                      }
                  }
              }
  
          },
  
          // multi
          getMaxSearchWidth: function() {
              return this.selection.width() - getSideBorderPadding(this.search);
          },
  
          // multi
          resizeSearch: function () {
              var minimumWidth, left, maxWidth, containerLeft, searchWidth,
                  sideBorderPadding = getSideBorderPadding(this.search);
  
              minimumWidth = measureTextWidth(this.search) + 10;
  
              left = this.search.offset().left;
  
              maxWidth = this.selection.width();
              containerLeft = this.selection.offset().left;
  
              searchWidth = maxWidth - (left - containerLeft) - sideBorderPadding;
  
              if (searchWidth < minimumWidth) {
                  searchWidth = maxWidth - sideBorderPadding;
              }
  
              if (searchWidth < 40) {
                  searchWidth = maxWidth - sideBorderPadding;
              }
  
              if (searchWidth <= 0) {
                  searchWidth = minimumWidth;
              }
  
              this.search.width(Math.floor(searchWidth));
          },
  
          // multi
          getVal: function () {
              var val;
              if (this.select) {
                  val = this.select.val();
                  return val === null ? [] : val;
              } else {
                  val = this.opts.element.val();
                  return splitVal(val, this.opts.separator);
              }
          },
  
          // multi
          setVal: function (val) {
              var unique;
              if (this.select) {
                  this.select.val(val);
              } else {
                  unique = [];
                  // filter out duplicates
                  $(val).each(function () {
                      if (indexOf(this, unique) < 0) unique.push(this);
                  });
                  this.opts.element.val(unique.length === 0 ? "" : unique.join(this.opts.separator));
              }
          },
  
          // multi
          buildChangeDetails: function (old, current) {
              var current = current.slice(0),
                  old = old.slice(0);
  
              // remove intersection from each array
              for (var i = 0; i < current.length; i++) {
                  for (var j = 0; j < old.length; j++) {
                      if (equal(this.opts.id(current[i]), this.opts.id(old[j]))) {
                          current.splice(i, 1);
                          if(i>0){
                              i--;
                          }
                          old.splice(j, 1);
                          j--;
                      }
                  }
              }
  
              return {added: current, removed: old};
          },
  
  
          // multi
          val: function (val, triggerChange) {
              var oldData, self=this;
  
              if (arguments.length === 0) {
                  return this.getVal();
              }
  
              oldData=this.data();
              if (!oldData.length) oldData=[];
  
              // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
              if (!val && val !== 0) {
                  this.opts.element.val("");
                  this.updateSelection([]);
                  this.clearSearch();
                  if (triggerChange) {
                      this.triggerChange({added: this.data(), removed: oldData});
                  }
                  return;
              }
  
              // val is a list of ids
              this.setVal(val);
  
              if (this.select) {
                  this.opts.initSelection(this.select, this.bind(this.updateSelection));
                  if (triggerChange) {
                      this.triggerChange(this.buildChangeDetails(oldData, this.data()));
                  }
              } else {
                  if (this.opts.initSelection === undefined) {
                      throw new Error("val() cannot be called if initSelection() is not defined");
                  }
  
                  this.opts.initSelection(this.opts.element, function(data){
                      var ids=$.map(data, self.id);
                      self.setVal(ids);
                      self.updateSelection(data);
                      self.clearSearch();
                      if (triggerChange) {
                          self.triggerChange(self.buildChangeDetails(oldData, self.data()));
                      }
                  });
              }
              this.clearSearch();
          },
  
          // multi
          onSortStart: function() {
              if (this.select) {
                  throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");
              }
  
              // collapse search field into 0 width so its container can be collapsed as well
              this.search.width(0);
              // hide the container
              this.searchContainer.hide();
          },
  
          // multi
          onSortEnd:function() {
  
              var val=[], self=this;
  
              // show search and move it to the end of the list
              this.searchContainer.show();
              // make sure the search container is the last item in the list
              this.searchContainer.appendTo(this.searchContainer.parent());
              // since we collapsed the width in dragStarted, we resize it here
              this.resizeSearch();
  
              // update selection
              this.selection.find(".select2-search-choice").each(function() {
                  val.push(self.opts.id($(this).data("select2-data")));
              });
              this.setVal(val);
              this.triggerChange();
          },
  
          // multi
          data: function(values, triggerChange) {
              var self=this, ids, old;
              if (arguments.length === 0) {
                  return this.selection
                      .find(".select2-search-choice")
                      .map(function() { return $(this).data("select2-data"); })
                      .get();
              } else {
                  old = this.data();
                  if (!values) { values = []; }
                  ids = $.map(values, function(e) { return self.opts.id(e); });
                  this.setVal(ids);
                  this.updateSelection(values);
                  this.clearSearch();
                  if (triggerChange) {
                      this.triggerChange(this.buildChangeDetails(old, this.data()));
                  }
              }
          }
      });
  
      $.fn.select2 = function () {
  
          var args = Array.prototype.slice.call(arguments, 0),
              opts,
              select2,
              method, value, multiple,
              allowedMethods = ["val", "destroy", "opened", "open", "close", "focus", "isFocused", "container", "dropdown", "onSortStart", "onSortEnd", "enable", "disable", "readonly", "positionDropdown", "data", "search"],
              valueMethods = ["opened", "isFocused", "container", "dropdown"],
              propertyMethods = ["val", "data"],
              methodsMap = { search: "externalSearch" };
  
          this.each(function () {
              if (args.length === 0 || typeof(args[0]) === "object") {
                  opts = args.length === 0 ? {} : $.extend({}, args[0]);
                  opts.element = $(this);
  
                  if (opts.element.get(0).tagName.toLowerCase() === "select") {
                      multiple = opts.element.prop("multiple");
                  } else {
                      multiple = opts.multiple || false;
                      if ("tags" in opts) {opts.multiple = multiple = true;}
                  }
  
                  select2 = multiple ? new MultiSelect2() : new SingleSelect2();
                  select2.init(opts);
              } else if (typeof(args[0]) === "string") {
  
                  if (indexOf(args[0], allowedMethods) < 0) {
                      throw "Unknown method: " + args[0];
                  }
  
                  value = undefined;
                  select2 = $(this).data("select2");
                  if (select2 === undefined) return;
  
                  method=args[0];
  
                  if (method === "container") {
                      value = select2.container;
                  } else if (method === "dropdown") {
                      value = select2.dropdown;
                  } else {
                      if (methodsMap[method]) method = methodsMap[method];
  
                      value = select2[method].apply(select2, args.slice(1));
                  }
                  if (indexOf(args[0], valueMethods) >= 0
                      || (indexOf(args[0], propertyMethods) && args.length == 1)) {
                      return false; // abort the iteration, ready to return first matched value
                  }
              } else {
                  throw "Invalid arguments to select2 plugin: " + args;
              }
          });
          return (value === undefined) ? this : value;
      };
  
      // plugin defaults, accessible to users
      $.fn.select2.defaults = {
          width: "copy",
          loadMorePadding: 0,
          closeOnSelect: true,
          openOnEnter: true,
          containerCss: {},
          dropdownCss: {},
          containerCssClass: "",
          dropdownCssClass: "",
          formatResult: function(result, container, query, escapeMarkup) {
              var markup=[];
              markMatch(result.text, query.term, markup, escapeMarkup);
              return markup.join("");
          },
          formatSelection: function (data, container, escapeMarkup) {
              return data ? escapeMarkup(data.text) : undefined;
          },
          sortResults: function (results, container, query) {
              return results;
          },
          formatResultCssClass: function(data) {return undefined;},
          formatSelectionCssClass: function(data, container) {return undefined;},
          formatNoMatches: function () { return "No matches found"; },
          formatInputTooShort: function (input, min) { var n = min - input.length; return "Please enter " + n + " more character" + (n == 1? "" : "s"); },
          formatInputTooLong: function (input, max) { var n = input.length - max; return "Please delete " + n + " character" + (n == 1? "" : "s"); },
          formatSelectionTooBig: function (limit) { return "You can only select " + limit + " item" + (limit == 1 ? "" : "s"); },
          formatLoadMore: function (pageNumber) { return "Loading more results..."; },
          formatSearching: function () { return "Searching..."; },
          minimumResultsForSearch: 0,
          minimumInputLength: 0,
          maximumInputLength: null,
          maximumSelectionSize: 0,
          id: function (e) { return e.id; },
          matcher: function(term, text) {
              return stripDiacritics(''+text).toUpperCase().indexOf(stripDiacritics(''+term).toUpperCase()) >= 0;
          },
          separator: ",",
          tokenSeparators: [],
          tokenizer: defaultTokenizer,
          escapeMarkup: defaultEscapeMarkup,
          blurOnChange: false,
          selectOnBlur: false,
          adaptContainerCssClass: function(c) { return c; },
          adaptDropdownCssClass: function(c) { return null; },
          nextSearchTerm: function(selectedObject, currentSearchTerm) { return undefined; }
      };
  
      $.fn.select2.ajaxDefaults = {
          transport: $.ajax,
          params: {
              type: "GET",
              cache: false,
              dataType: "json"
          }
      };
  
      // exports
      window.Select2 = {
          query: {
              ajax: ajax,
              local: local,
              tags: tags
          }, util: {
              debounce: debounce,
              markMatch: markMatch,
              escapeMarkup: defaultEscapeMarkup,
              stripDiacritics: stripDiacritics
          }, "class": {
              "abstract": AbstractSelect2,
              "single": SingleSelect2,
              "multi": MultiSelect2
          }
      };
  
  }(jQuery));
  
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/select2.js
(typeof window === 'undefined' ? global : window).__046e6926067d85eab02f7d242b467f2c = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  __cb40cd044a911820c6668078d4113d99;
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  /**
   * Wraps a vanilla Select2 with ADG _style_, as an auiSelect2 method on jQuery objects.
   *
   * @since 5.2
   */
  
  /**
   * We make a copy of the original select2 so that later we might re-specify $.fn.auiSelect2 as $.fn.select2. That
   * way, calling code will be able to call $thing.select2() as if they were calling the original library,
   * and ADG styling will just magically happen.
   */
  var originalSelect2 = _jquery2.default.fn.select2;
  
  // AUI-specific classes
  var auiContainer = 'aui-select2-container';
  var auiDropdown = 'aui-select2-drop aui-dropdown2 aui-style-default';
  var auiHasAvatar = 'aui-has-avatar';
  
  _jquery2.default.fn.auiSelect2 = function (first) {
      var updatedArgs;
  
      if (_jquery2.default.isPlainObject(first)) {
          var auiOpts = _jquery2.default.extend({}, first);
          var auiAvatarClass = auiOpts.hasAvatar ? ' ' + auiHasAvatar : '';
          //add our classes in addition to those the caller specified
          auiOpts.containerCssClass = auiContainer + auiAvatarClass + (auiOpts.containerCssClass ? ' ' + auiOpts.containerCssClass : '');
          auiOpts.dropdownCssClass = auiDropdown + auiAvatarClass + (auiOpts.dropdownCssClass ? ' ' + auiOpts.dropdownCssClass : '');
          updatedArgs = Array.prototype.slice.call(arguments, 1);
          updatedArgs.unshift(auiOpts);
      } else if (!arguments.length) {
          updatedArgs = [{
              containerCssClass: auiContainer,
              dropdownCssClass: auiDropdown
          }];
      } else {
          updatedArgs = arguments;
      }
  
      return originalSelect2.apply(this, updatedArgs);
  };
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js-vendor/jquery/jquery.tipsy.js
(typeof window === 'undefined' ? global : window).__baae3fd52c7cb4dfd4b31bed331ca422 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  // tipsy, facebook style tooltips for jquery
  // version 1.0.0a
  // (c) 2008-2010 jason frame [jason@onehackoranother.com]
  // released under the MIT license
  //
  // Modified by Atlassian
  // https://github.com/atlassian/tipsy/tree/d1d360c881dc58bfebbed449739189b35895a5be
  
  (function($) {
  
      function maybeCall(thing, ctx) {
          return (typeof thing == 'function') ? (thing.call(ctx)) : thing;
      };
  
      function isElementInDOM(ele) {
          while (ele = ele.parentNode) {
              if (ele == document) return true;
          }
          return false;
      };
  
      var tipsyIDcounter = 0;
      function tipsyID() {
          var tipsyID = tipsyIDcounter++;
          return "tipsyuid" + tipsyID;
      };
  
      function Tipsy(element, options) {
          this.$element = $(element);
          this.options = options;
          this.enabled = true;
          this.fixTitle();
      };
  
      Tipsy.prototype = {
          show: function() {
              // if element is not in the DOM then don't show the Tipsy and return early
              if (!isElementInDOM(this.$element[0])) {
                  return;
              }
  
              var title = this.getTitle();
              if (title && this.enabled) {
                  var $tip = this.tip();
  
                  $tip.find('.tipsy-inner')[this.options.html ? 'html' : 'text'](title);
                  $tip[0].className = 'tipsy'; // reset classname in case of dynamic gravity
                  $tip.remove().css({top: 0, left: 0, visibility: 'hidden', display: 'block'}).appendTo(document.body);
  
  
                  var that = this;
                  function tipOver() {
                      that.hoverTooltip = true;
                  }
                  function tipOut() {
                      if (that.hoverState == 'in') return;  // If field is still focused.
                      that.hoverTooltip = false;
                      if (that.options.trigger != 'manual') {
                          var eventOut = that.options.trigger == 'hover' ? 'mouseleave.tipsy' : 'blur.tipsy';
                          that.$element.trigger(eventOut);
                      }
                  }
  
                  if (this.options.hoverable) {
                      $tip.hover(tipOver, tipOut);
                  }
  
                  if (this.options.className) {
                      $tip.addClass(maybeCall(this.options.className, this.$element[0]));
                  }
  
                  var pos = $.extend({}, this.$element.offset(), {
                      width: this.$element[0].getBoundingClientRect().width,
                      height: this.$element[0].getBoundingClientRect().height
                  });
  
                  var actualWidth = $tip[0].offsetWidth,
                      actualHeight = $tip[0].offsetHeight,
                      gravity = maybeCall(this.options.gravity, this.$element[0]);
  
                  var tp;
                  switch (gravity.charAt(0)) {
                      case 'n':
                          tp = {top: pos.top + pos.height + this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                          break;
                      case 's':
                          tp = {top: pos.top - actualHeight - this.options.offset, left: pos.left + pos.width / 2 - actualWidth / 2};
                          break;
                      case 'e':
                          tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth - this.options.offset};
                          break;
                      case 'w':
                          tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width + this.options.offset};
                          break;
                  }
  
                  if (gravity.length == 2) {
                      if (gravity.charAt(1) == 'w') {
                          tp.left = pos.left + pos.width / 2 - 15;
                      } else {
                          tp.left = pos.left + pos.width / 2 - actualWidth + 15;
                      }
                  }
  
                  $tip.css(tp).addClass('tipsy-' + gravity);
                  $tip.find('.tipsy-arrow')[0].className = 'tipsy-arrow tipsy-arrow-' + gravity.charAt(0);
  
                  if (this.options.fade) {
                      $tip.stop().css({opacity: 0, display: 'block', visibility: 'visible'}).animate({opacity: this.options.opacity});
                  } else {
                      $tip.css({visibility: 'visible', opacity: this.options.opacity});
                  }
  
                  if (this.options.aria) {
                      var $tipID = tipsyID();
                      $tip.attr("id", $tipID);
                      this.$element.attr("aria-describedby", $tipID);
                  }
              }
          },
  
          destroy: function(){
              this.$element.removeData('tipsy');
  
              this.unbindHandlers();
              this.hide();
          },
  
          unbindHandlers: function() {
              if(this.options.live){
                  $(this.$element.context).off('.tipsy');
              } else {
                  this.$element.unbind('.tipsy');
              }
          },
  
          hide: function() {
              if (this.options.fade) {
                  this.tip().stop().fadeOut(function() { $(this).remove(); });
              } else {
                  this.tip().remove();
              }
              if (this.options.aria) {
                  this.$element.removeAttr("aria-describedby");
              }
          },
  
          fixTitle: function() {
              var $e = this.$element;
              if ($e.attr('title') || typeof($e.attr('original-title')) != 'string') {
                  $e.attr('original-title', $e.attr('title') || '').removeAttr('title');
              }
          },
  
          getTitle: function() {
              var title, $e = this.$element, o = this.options;
              this.fixTitle();
              var title, o = this.options;
              if (typeof o.title == 'string') {
                  title = $e.attr(o.title == 'title' ? 'original-title' : o.title);
              } else if (typeof o.title == 'function') {
                  title = o.title.call($e[0]);
              }
              title = ('' + title).replace(/(^\s*|\s*$)/, "");
              return title || o.fallback;
          },
  
          tip: function() {
              if (!this.$tip) {
                  this.$tip = $('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>').attr("role","tooltip");
                  this.$tip.data('tipsy-pointee', this.$element[0]);
              }
              return this.$tip;
          },
  
          validate: function() {
              if (!this.$element[0].parentNode) {
                  this.hide();
                  this.$element = null;
                  this.options = null;
              }
          },
  
          enable: function() { this.enabled = true; },
          disable: function() { this.enabled = false; },
          toggleEnabled: function() { this.enabled = !this.enabled; }
      };
  
      $.fn.tipsy = function(options) {
  
          if (options === true) {
              return this.data('tipsy');
          } else if (typeof options == 'string') {
              var tipsy = this.data('tipsy');
              if (tipsy) tipsy[options]();
              return this;
          }
  
          options = $.extend({}, $.fn.tipsy.defaults, options);
          if (options.hoverable) {
              options.delayOut = options.delayOut || 20;
          }
  
          function get(ele) {
              var tipsy = $.data(ele, 'tipsy');
              if (!tipsy) {
                  tipsy = new Tipsy(ele, $.fn.tipsy.elementOptions(ele, options));
                  $.data(ele, 'tipsy', tipsy);
              }
              return tipsy;
          }
  
          function enter() {
              var tipsy = get(this);
              tipsy.hoverState = 'in';
              if (options.delayIn == 0) {
                  tipsy.show();
              } else {
                  tipsy.fixTitle();
                  setTimeout(function() { if (tipsy.hoverState == 'in') tipsy.show(); }, options.delayIn);
              }
          };
  
          function leave() {
              var tipsy = get(this);
              tipsy.hoverState = 'out';
              if (options.delayOut == 0) {
                  tipsy.hide();
              } else {
                  setTimeout(function() { if (tipsy.hoverState == 'out' && !tipsy.hoverTooltip) tipsy.hide(); }, options.delayOut);
              }
          };
  
          if (!options.live) this.each(function() { get(this); });
  
          if (options.trigger != 'manual') {
              var eventIn  = options.trigger == 'hover' ? 'mouseenter.tipsy focus.tipsy' : 'focus.tipsy',
                  eventOut = options.trigger == 'hover' ? 'mouseleave.tipsy blur.tipsy' : 'blur.tipsy';
              if (options.live) {
                  $(this.context).on(eventIn, this.selector, enter).on(eventOut, this.selector, leave);
              } else {
                  this.bind(eventIn, enter).bind(eventOut, leave);
              }
          }
  
          return this;
  
      };
  
      $.fn.tipsy.defaults = {
          aria: false,
          className: null,
          delayIn: 0,
          delayOut: 0,
          fade: false,
          fallback: '',
          gravity: 'n',
          html: false,
          live: false,
          hoverable: false,
          offset: 0,
          opacity: 0.8,
          title: 'title',
          trigger: 'hover'
      };
  
      $.fn.tipsy.revalidate = function() {
          $('.tipsy').each(function() {
              var pointee = $.data(this, 'tipsy-pointee');
              if (!pointee || !isElementInDOM(pointee)) {
                  $(this).remove();
              }
          });
      };
  
      // Overwrite this method to provide options on a per-element basis.
      // For example, you could store the gravity in a 'tipsy-gravity' attribute:
      // return $.extend({}, options, {gravity: $(ele).attr('tipsy-gravity') || 'n' });
      // (remember - do not modify 'options' in place!)
      $.fn.tipsy.elementOptions = function(ele, options) {
          return $.metadata ? $.extend({}, options, $(ele).metadata()) : options;
      };
  
      $.fn.tipsy.autoNS = function() {
          return $(this).offset().top > ($(document).scrollTop() + $(window).height() / 2) ? 's' : 'n';
      };
  
      $.fn.tipsy.autoWE = function() {
          return $(this).offset().left > ($(document).scrollLeft() + $(window).width() / 2) ? 'e' : 'w';
      };
  
      /**
       * yields a closure of the supplied parameters, producing a function that takes
       * no arguments and is suitable for use as an autogravity function like so:
       *
       * @param margin (int) - distance from the viewable region edge that an
       *        element should be before setting its tooltip's gravity to be away
       *        from that edge.
       * @param prefer (string, e.g. 'n', 'sw', 'w') - the direction to prefer
       *        if there are no viewable region edges effecting the tooltip's
       *        gravity. It will try to vary from this minimally, for example,
       *        if 'sw' is preferred and an element is near the right viewable
       *        region edge, but not the top edge, it will set the gravity for
       *        that element's tooltip to be 'se', preserving the southern
       *        component.
       */
      $.fn.tipsy.autoBounds = function(margin, prefer) {
          return function() {
              var dir = {ns: prefer[0], ew: (prefer.length > 1 ? prefer[1] : false)},
                  boundTop = $(document).scrollTop() + margin,
                  boundLeft = $(document).scrollLeft() + margin,
                  $this = $(this);
  
              if ($this.offset().top < boundTop) dir.ns = 'n';
              if ($this.offset().left < boundLeft) dir.ew = 'w';
              if ($(window).width() + $(document).scrollLeft() - $this.offset().left < margin) dir.ew = 'e';
              if ($(window).height() + $(document).scrollTop() - $this.offset().top < margin) dir.ns = 's';
  
              return dir.ns + (dir.ew ? dir.ew : '');
          }
      };
  
  })(jQuery);
  
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/tooltip.js
(typeof window === 'undefined' ? global : window).__0c3611e861f995f21843e90f51ae6229 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  __baae3fd52c7cb4dfd4b31bed331ca422;
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function handleStringOption($self, options, stringOption) {
      // Pass string values straight to tipsy
      $self.tipsy(stringOption);
  
      if (stringOption === 'destroy') {
          if (options.live) {
              (0, _jquery2.default)($self.context).off('.tipsy', $self.selector);
          } else {
              $self.unbind('.tipsy');
          }
      }
  
      return $self;
  }
  
  function bindTooltip($self, options) {
      $self.tipsy(options);
  
      var hideOnClick = options && options.hideOnClick && (options.trigger === 'hover' || !options.trigger && $self.tipsy.defaults.trigger === 'hover');
      if (hideOnClick) {
          var onClick = function onClick() {
              (0, _jquery2.default)(this).tipsy('hide');
          };
          if (options.live) {
              (0, _jquery2.default)($self.context).on('click.tipsy', $self.selector, onClick);
          } else {
              $self.bind('click.tipsy', onClick);
          }
      }
      return $self;
  }
  
  _jquery2.default.fn.tooltip = function (options) {
      var allOptions = _jquery2.default.extend({}, _jquery2.default.fn.tooltip.defaults, options);
  
      // Handle live option
      if (allOptions.live) {
          if (typeof options === 'string') {
              handleStringOption(this, allOptions, options);
          } else {
              bindTooltip(this, allOptions);
          }
          return this;
      }
  
      // If not live, bind each object in the collection
      return this.each(function () {
          var $this = (0, _jquery2.default)(this);
          if (typeof options === 'string') {
              handleStringOption($this, allOptions, options);
          } else {
              bindTooltip($this, allOptions);
          }
          return $this;
      });
  };
  
  _jquery2.default.fn.tooltip.defaults = {
      opacity: 1.0,
      offset: 1,
      delayIn: 500,
      hoverable: true,
      hideOnClick: true,
      aria: true
  };
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js-vendor/spin/spin.js
(typeof window === 'undefined' ? global : window).__3649c63f5f29e9c27493296b4eb1bbb9 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  var defineDependencies = {
    "module": module,
    "exports": exports
  };
  var define = function defineReplacementWrapper(generatedModuleName) {
    return function defineReplacement(name, deps, func) {
      var root = (typeof window === 'undefined' ? global : window);
      var defineGlobal = root.define;
      var rval;
      var type;
  
      func = [func, deps, name].filter(function (cur) {
        return typeof cur === 'function';
      })[0];
      deps = [deps, name, []].filter(Array.isArray)[0];
      rval = func.apply(null, deps.map(function (value) {
        return defineDependencies[value];
      }));
      type = typeof rval;
  
      // Support existing AMD libs.
      if (typeof defineGlobal === 'function') {
        // Almond always expects a name so resolve one (#29).
        defineGlobal(typeof name === 'string' ? name : generatedModuleName, deps, func);
      }
  
      // Some processors like Babel don't check to make sure that the module value
      // is not a primitive before calling Object.defineProperty() on it. We ensure
      // it is an instance so that it can.
      if (type === 'string') {
        rval = String(rval);
      } else if (type === 'number') {
        rval = Number(rval);
      } else if (type === 'boolean') {
        rval = Boolean(rval);
      }
  
      // Reset the exports to the defined module. This is how we convert AMD to
      // CommonJS and ensures both can either co-exist, or be used separately. We
      // only set it if it is not defined because there is no object representation
      // of undefined, thus calling Object.defineProperty() on it would fail.
      if (rval !== undefined) {
        exports = module.exports = rval;
      }
    };
  }("__3649c63f5f29e9c27493296b4eb1bbb9");
  define.amd = true;
  
  //fgnass.github.com/spin.js#v1.3.3
  /*
      Modified by Atlassian
   */
  
  /**
   * Copyright (c) 2011-2013 Felix Gnass
   * Licensed under the MIT license
   */
  (function(root, factory) {
  
    /* CommonJS */
    if (typeof exports == 'object')  module.exports = factory()
  
    /* AMD module */
    // ATLASSIAN - don't check define.amd for products who deleted it.
    else if (typeof define == 'function') define('aui/internal/spin', factory)
  
    /* Browser global */
    // ATLASSIAN - always expose Spinner globally
    root.Spinner = factory()
  }
  (this, function() {
  
    var prefixes = ['webkit', 'Moz', 'ms', 'O'] /* Vendor prefixes */
      , animations = {} /* Animation rules keyed by their name */
      , useCssAnimations /* Whether to use CSS animations or setTimeout */
  
    /**
     * Utility function to create elements. If no tag name is given,
     * a DIV is created. Optionally properties can be passed.
     */
    function createEl(tag, prop) {
      var el = document.createElement(tag || 'div')
        , n
  
      for(n in prop) el[n] = prop[n]
      return el
    }
  
    /**
     * Appends children and returns the parent.
     */
    function ins(parent /* child1, child2, ...*/) {
      for (var i=1, n=arguments.length; i<n; i++)
        parent.appendChild(arguments[i])
  
      return parent
    }
  
    /**
     * Insert a new stylesheet to hold the @keyframe or VML rules.
     */
    var sheet = (function() {
      var el = createEl('style', {type : 'text/css'})
      ins(document.getElementsByTagName('head')[0], el)
      return el.sheet || el.styleSheet
    }())
  
    /**
     * Creates an opacity keyframe animation rule and returns its name.
     * Since most mobile Webkits have timing issues with animation-delay,
     * we create separate rules for each line/segment.
     */
    function addAnimation(alpha, trail, i, lines) {
      var name = ['opacity', trail, ~~(alpha*100), i, lines].join('-')
        , start = 0.01 + i/lines * 100
        , z = Math.max(1 - (1-alpha) / trail * (100-start), alpha)
        , prefix = useCssAnimations.substring(0, useCssAnimations.indexOf('Animation')).toLowerCase()
        , pre = prefix && '-' + prefix + '-' || ''
  
      if (!animations[name]) {
        sheet.insertRule(
          '@' + pre + 'keyframes ' + name + '{' +
          '0%{opacity:' + z + '}' +
          start + '%{opacity:' + alpha + '}' +
          (start+0.01) + '%{opacity:1}' +
          (start+trail) % 100 + '%{opacity:' + alpha + '}' +
          '100%{opacity:' + z + '}' +
          '}', sheet.cssRules.length)
  
        animations[name] = 1
      }
  
      return name
    }
  
    /**
     * Tries various vendor prefixes and returns the first supported property.
     */
    function vendor(el, prop) {
      var s = el.style
        , pp
        , i
  
      prop = prop.charAt(0).toUpperCase() + prop.slice(1)
      for(i=0; i<prefixes.length; i++) {
        pp = prefixes[i]+prop
        if(s[pp] !== undefined) return pp
      }
      if(s[prop] !== undefined) return prop
    }
  
    /**
     * Sets multiple style properties at once.
     */
    function css(el, prop) {
      for (var n in prop)
        el.style[vendor(el, n)||n] = prop[n]
  
      return el
    }
  
    /**
     * Fills in default values.
     */
    function merge(obj) {
      for (var i=1; i < arguments.length; i++) {
        var def = arguments[i]
        for (var n in def)
          if (obj[n] === undefined) obj[n] = def[n]
      }
      return obj
    }
  
    /**
     * Returns the absolute page-offset of the given element.
     */
    function pos(el) {
      var o = { x:el.offsetLeft, y:el.offsetTop }
      while((el = el.offsetParent))
        // ATLASSIAN - AUI-3542 - add border width to the calculation of o.x and o.y
        o.x+=el.offsetLeft+el.clientLeft, o.y+=el.offsetTop+el.clientTop
  
      return o
    }
  
    /**
     * Returns the line color from the given string or array.
     */
    function getColor(color, idx) {
      return typeof color == 'string' ? color : color[idx % color.length]
    }
  
    // Built-in defaults
  
    var defaults = {
      lines: 12,            // The number of lines to draw
      length: 7,            // The length of each line
      width: 5,             // The line thickness
      radius: 10,           // The radius of the inner circle
      rotate: 0,            // Rotation offset
      corners: 1,           // Roundness (0..1)
      color: '#000',        // #rgb or #rrggbb
      direction: 1,         // 1: clockwise, -1: counterclockwise
      speed: 1,             // Rounds per second
      trail: 100,           // Afterglow percentage
      opacity: 1/4,         // Opacity of the lines
      fps: 20,              // Frames per second when using setTimeout()
      zIndex: 2e9,          // Use a high z-index by default
      className: 'spinner', // CSS class to assign to the element
      top: 'auto',          // center vertically
      left: 'auto',         // center horizontally
      position: 'relative'  // element position
    }
  
    /** The constructor */
    function Spinner(o) {
      if (typeof this == 'undefined') return new Spinner(o)
      this.opts = merge(o || {}, Spinner.defaults, defaults)
    }
  
    // Global defaults that override the built-ins:
    Spinner.defaults = {}
  
    merge(Spinner.prototype, {
  
      /**
       * Adds the spinner to the given target element. If this instance is already
       * spinning, it is automatically removed from its previous target b calling
       * stop() internally.
       */
      spin: function(target) {
        this.stop()
  
        var self = this
          , o = self.opts
          , el = self.el = css(createEl(0, {className: o.className}), {position: o.position, width: 0, zIndex: o.zIndex})
          , mid = o.radius+o.length+o.width
          , ep // element position
          , tp // target position
  
        if (target) {
          target.insertBefore(el, target.firstChild||null)
          tp = pos(target)
          ep = pos(el)
          css(el, {
            left: (o.left == 'auto' ? tp.x-ep.x + (target.offsetWidth >> 1) : parseInt(o.left, 10) + mid) + 'px',
            top: (o.top == 'auto' ? tp.y-ep.y + (target.offsetHeight >> 1) : parseInt(o.top, 10) + mid)  + 'px'
          })
        }
  
        el.setAttribute('role', 'progressbar')
        self.lines(el, self.opts)
  
        if (!useCssAnimations) {
          // No CSS animation support, use setTimeout() instead
          var i = 0
            , start = (o.lines - 1) * (1 - o.direction) / 2
            , alpha
            , fps = o.fps
            , f = fps/o.speed
            , ostep = (1-o.opacity) / (f*o.trail / 100)
            , astep = f/o.lines
  
          ;(function anim() {
            i++;
            for (var j = 0; j < o.lines; j++) {
              alpha = Math.max(1 - (i + (o.lines - j) * astep) % f * ostep, o.opacity)
  
              self.opacity(el, j * o.direction + start, alpha, o)
            }
            self.timeout = self.el && setTimeout(anim, ~~(1000/fps))
          })()
        }
        return self
      },
  
      /**
       * Stops and removes the Spinner.
       */
      stop: function() {
        var el = this.el
        if (el) {
          clearTimeout(this.timeout)
          if (el.parentNode) el.parentNode.removeChild(el)
          this.el = undefined
        }
        return this
      },
  
      /**
       * Internal method that draws the individual lines. Will be overwritten
       * in VML fallback mode below.
       */
      lines: function(el, o) {
        var i = 0
          , start = (o.lines - 1) * (1 - o.direction) / 2
          , seg
  
        function fill(color, shadow) {
          return css(createEl(), {
            position: 'absolute',
            width: (o.length+o.width) + 'px',
            height: o.width + 'px',
            background: color,
            boxShadow: shadow,
            transformOrigin: 'left',
            transform: 'rotate(' + ~~(360/o.lines*i+o.rotate) + 'deg) translate(' + o.radius+'px' +',0)',
            borderRadius: (o.corners * o.width>>1) + 'px'
          })
        }
  
        for (; i < o.lines; i++) {
          seg = css(createEl(), {
            position: 'absolute',
            top: 1+~(o.width/2) + 'px',
            transform: o.hwaccel ? 'translate3d(0,0,0)' : '',
            opacity: o.opacity,
            animation: useCssAnimations && addAnimation(o.opacity, o.trail, start + i * o.direction, o.lines) + ' ' + 1/o.speed + 's linear infinite'
          })
  
          if (o.shadow) ins(seg, css(fill('#000', '0 0 4px ' + '#000'), {top: 2+'px'}))
          ins(el, ins(seg, fill(getColor(o.color, i), '0 0 1px rgba(0,0,0,.1)')))
        }
        return el
      },
  
      /**
       * Internal method that adjusts the opacity of a single line.
       * Will be overwritten in VML fallback mode below.
       */
      opacity: function(el, i, val) {
        if (i < el.childNodes.length) el.childNodes[i].style.opacity = val
      }
  
    })
  
  
    function initVML() {
  
      /* Utility function to create a VML tag */
      function vml(tag, attr) {
        return createEl('<' + tag + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', attr)
      }
  
      // No CSS transforms but VML support, add a CSS rule for VML elements:
      sheet.addRule('.spin-vml', 'behavior:url(#default#VML)')
  
      Spinner.prototype.lines = function(el, o) {
        var r = o.length+o.width
          , s = 2*r
  
        function grp() {
          return css(
            vml('group', {
              coordsize: s + ' ' + s,
              coordorigin: -r + ' ' + -r
            }),
            { width: s, height: s }
          )
        }
  
        var margin = -(o.width+o.length)*2 + 'px'
          , g = css(grp(), {position: 'absolute', top: margin, left: margin})
          , i
  
        function seg(i, dx, filter) {
          ins(g,
            ins(css(grp(), {rotation: 360 / o.lines * i + 'deg', left: ~~dx}),
              ins(css(vml('roundrect', {arcsize: o.corners}), {
                  width: r,
                  height: o.width,
                  left: o.radius,
                  top: -o.width>>1,
                  filter: filter
                }),
                vml('fill', {color: getColor(o.color, i), opacity: o.opacity}),
                vml('stroke', {opacity: 0}) // transparent stroke to fix color bleeding upon opacity change
              )
            )
          )
        }
  
        if (o.shadow)
          for (i = 1; i <= o.lines; i++)
            seg(i, -2, 'progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)')
  
        for (i = 1; i <= o.lines; i++) seg(i)
        return ins(el, g)
      }
  
      Spinner.prototype.opacity = function(el, i, val, o) {
        var c = el.firstChild
        o = o.shadow && o.lines || 0
        if (c && i+o < c.childNodes.length) {
          c = c.childNodes[i+o]; c = c && c.firstChild; c = c && c.firstChild
          if (c) c.opacity = val
        }
      }
    }
  
    var probe = css(createEl('group'), {behavior: 'url(#default#VML)'})
  
    if (!vendor(probe, 'transform') && probe.adj) initVML()
    else useCssAnimations = vendor(probe, 'animation')
  
    return Spinner
  
  }));
  
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js-vendor/jquery/jquery.spin.js
(typeof window === 'undefined' ? global : window).__94f9bd890b0cda28fea9063195ed8ca1 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  /*
   * Ideas from https://gist.github.com/its-florida/1290439 are acknowledged and used here.
   * Resulting file is heavily modified from that gist so is licensed under AUI's license.
   *
   * You can now create a spinner using any of the variants below:
   *
   * $("#el").spin(); // Produces default Spinner using the text color of #el.
   * $("#el").spin("small"); // Produces a 'small' Spinner using the text color of #el.
   * $("#el").spin("large", { ... }); // Produces a 'large' Spinner with your custom settings.
   * $("#el").spin({ ... }); // Produces a Spinner using your custom settings.
   *
   * $("#el").spin(false); // Kills the spinner.
   * $("#el").spinStop(); // Also kills the spinner.
   *
   */
  (function($) {
      $.fn.spin = function(optsOrPreset, opts) {
          var preset, options;
  
          if (typeof optsOrPreset === 'string') {
              if (! optsOrPreset in $.fn.spin.presets) {
                  throw new Error("Preset '" + optsOrPreset + "' isn't defined");
              }
              preset = $.fn.spin.presets[optsOrPreset];
              options = opts || {};
          } else {
              if (opts) {
                  throw new Error('Invalid arguments. Accepted arguments:\n' +
                      '$.spin([String preset[, Object options]]),\n' +
                      '$.spin(Object options),\n' +
                      '$.spin(Boolean shouldSpin)');
              }
              preset = $.fn.spin.presets.small;
              options = $.isPlainObject(optsOrPreset) ? optsOrPreset : {};
          }
  
          if (window.Spinner) {
              return this.each(function() {
                  var $this = $(this),
                      data = $this.data();
  
                  if (data.spinner) {
                      data.spinner.stop();
                      delete data.spinner;
                  }
  
                  if (optsOrPreset === false) { // just stop it spinning.
                      return;
                  }
  
                  options = $.extend({ color: $this.css('color') }, preset, options);
                  data.spinner = new Spinner(options).spin(this);
              });
          } else {
              throw "Spinner class not available.";
          }
      };
      $.fn.spin.presets = {
          "small": { lines: 12, length: 3, width: 2, radius: 3, trail: 60, speed: 1.5 },
          "medium": { lines: 12, length: 5, width: 3, radius: 8, trail: 60, speed: 1.5 },
          "large": { lines: 12, length: 8, width: 4, radius: 10, trail: 60, speed: 1.5 }
      };
  
      $.fn.spinStop = function() {
          if (window.Spinner) {
              return this.each(function() {
                  var $this = $(this),
                      data = $this.data();
  
                  if (data.spinner) {
                      data.spinner.stop();
                      delete data.spinner;
                  }
  
              });
          } else {
              throw "Spinner class not available.";
          }
      };
  })(jQuery);
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/spin.js
(typeof window === 'undefined' ? global : window).__05c01776cbfc61968de375e6766b621e = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  __3649c63f5f29e9c27493296b4eb1bbb9;
  
  __94f9bd890b0cda28fea9063195ed8ca1;
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/attributes.js
(typeof window === 'undefined' ? global : window).__c86795ff15889f1079ca13abda75a9e8 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  exports.computeBooleanValue = computeBooleanValue;
  exports.setBooleanAttribute = setBooleanAttribute;
  exports.computeEnumValue = computeEnumValue;
  exports.setEnumAttribute = setEnumAttribute;
  /**
   * Like el.hasAttribute(attr) but designed for use within a skate attribute
   * change handler where you only have access to change.oldValue.
   */
  function computeBooleanValue(attrValue) {
      return attrValue !== null;
  }
  
  function setBooleanAttribute(el, attr, newValue) {
      if (newValue) {
          el.setAttribute(attr, '');
      } else {
          el.removeAttribute(attr);
      }
  }
  
  function computeEnumValue(enumOptions, value) {
      var matchesEnumValue = function matchesEnumValue(enumValue) {
          return enumValue.toLowerCase() === value.toLowerCase();
      };
  
      var isMissing = value === null;
      var isInvalid = !isMissing && !enumOptions.values.filter(matchesEnumValue).length;
  
      if (isMissing) {
          if (enumOptions.hasOwnProperty('missingDefault')) {
              return enumOptions.missingDefault;
          }
          return null;
      }
  
      if (isInvalid) {
          if (enumOptions.hasOwnProperty('invalidDefault')) {
              return enumOptions.invalidDefault;
          } else if (enumOptions.hasOwnProperty('missingDefault')) {
              return enumOptions.missingDefault;
          }
          return null;
      }
  
      return enumOptions.values.length ? enumOptions.values.filter(matchesEnumValue)[0] : null;
  }
  
  function setEnumAttribute(el, enumOptions, newValue) {
      el.setAttribute(enumOptions.attribute, newValue);
  }
  
  /**
   * Helper functions useful for implementing reflected boolean and enumerated
   * attributes and properties.
   *
   * @see https://html.spec.whatwg.org/multipage/infrastructure.html#reflecting-content-attributes-in-idl-attributes
   * @see https://html.spec.whatwg.org/multipage/infrastructure.html#boolean-attribute
   * @see https://html.spec.whatwg.org/multipage/infrastructure.html#enumerated-attribute
   */
  exports.default = {
      computeBooleanValue: computeBooleanValue,
      setBooleanAttribute: setBooleanAttribute,
  
      computeEnumValue: computeEnumValue,
      setEnumAttribute: setEnumAttribute
  };
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/enforcer.js
(typeof window === 'undefined' ? global : window).__3cc35d3d24228ec6aae9edd67e3b685c = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  
  var _log = __3db7d70f11d6400b2fe84fc4d957c72d;
  
  var logger = _interopRequireWildcard(_log);
  
  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
  
  function enforcer(element) {
  
      function attributeExists(attributeName) {
          var errorMessage = attributeName + ' wasn\'t defined';
  
          return satisfiesRules(function () {
              return element.hasAttribute(attributeName);
          }, errorMessage);
      }
  
      function refersTo(attributeName) {
  
          if (!attributeExists(attributeName, element)) {
              return false;
          }
  
          var desiredId = element.getAttribute(attributeName);
          var errorMessage = 'an element with id set to "' + desiredId + '" was not found';
  
          return satisfiesRules(function () {
              return document.getElementById(desiredId);
          }, errorMessage);
      }
  
      function ariaControls() {
          return refersTo('aria-controls');
      }
  
      function ariaOwns() {
          return refersTo('aria-owns');
      }
  
      function satisfiesRules(predicate, message) {
          if (!predicate()) {
              if (element) {
                  logger.error(message, element);
              } else {
                  logger.error(message);
              }
              return false;
          }
          return true;
      }
  
      return {
          attributeExists: attributeExists,
          refersTo: refersTo,
          satisfiesRules: satisfiesRules,
          ariaControls: ariaControls,
          ariaOwns: ariaOwns
      };
  }
  
  exports.default = enforcer;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/internal/constants.js
(typeof window === 'undefined' ? global : window).__dc6956425ba26c9863c295cbcdecb6a0 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
      value: true
  });
  var INPUT_SUFFIX = '-input';
  
  exports.default = {
      INPUT_SUFFIX: INPUT_SUFFIX
  };
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);
// node_modules/@atlassian/aui/src/js/aui/toggle.js
(typeof window === 'undefined' ? global : window).__19da4b233650ddabf42736b8eabb9c7a = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  __05c01776cbfc61968de375e6766b621e;
  
  var _attributes = __c86795ff15889f1079ca13abda75a9e8;
  
  var _jquery = __2c07683a8e1fdd32a12b7cc2f919ce59;
  
  var _jquery2 = _interopRequireDefault(_jquery);
  
  var _enforcer = __3cc35d3d24228ec6aae9edd67e3b685c;
  
  var _enforcer2 = _interopRequireDefault(_enforcer);
  
  var _skatejsTemplateHtml = __13ac5d5912c1c2250ae1f594e62da187;
  
  var _skatejsTemplateHtml2 = _interopRequireDefault(_skatejsTemplateHtml);
  
  var _skate = __77840d725b87e770c115b18925208796;
  
  var _skate2 = _interopRequireDefault(_skate);
  
  var _constants = __dc6956425ba26c9863c295cbcdecb6a0;
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
  
  function getInput(element) {
      return element._input || (element._input = element.querySelector('input'));
  }
  
  function removedAttributeHandler(attributeName, element) {
      getInput(element).removeAttribute(attributeName);
  }
  
  function fallbackAttributeHandler(attributeName, element, change) {
      getInput(element).setAttribute(attributeName, change.newValue);
  }
  
  function getAttributeHandler(attributeName) {
      return {
          removed: removedAttributeHandler.bind(this, attributeName),
          fallback: fallbackAttributeHandler.bind(this, attributeName)
      };
  }
  
  var formAttributeHandler = {
      removed: function removed(element) {
          removedAttributeHandler.call(this, 'form', element);
          element._formId = null;
      },
      fallback: function fallback(element, change) {
          fallbackAttributeHandler.call(this, 'form', element, change);
          element._formId = change.newValue;
      }
  };
  
  var idAttributeHandler = {
      removed: removedAttributeHandler.bind(undefined, 'id'),
      fallback: function fallback(element, change) {
          getInput(element).setAttribute('id', '' + change.newValue + _constants.INPUT_SUFFIX);
      }
  };
  
  var checkedAttributeHandler = {
      removed: function removed(element) {
          removedAttributeHandler.call(this, 'checked', element);
          getInput(element).checked = false;
      },
      fallback: function fallback(element, change) {
          fallbackAttributeHandler.call(this, 'checked', element, change);
          getInput(element).checked = true;
      }
  };
  
  var labelHandler = {
      removed: function removed(element) {
          getInput(element).removeAttribute('aria-label');
      },
      fallback: function fallback(element, change) {
          getInput(element).setAttribute('aria-label', change.newValue);
      }
  };
  
  function clickHandler(element, e) {
      if (!element.disabled && !element.busy && e.target !== element._input) {
          element._input.click();
      }
      (0, _attributes.setBooleanAttribute)(element, 'checked', getInput(element).checked);
  }
  
  function setDisabledForLabels(element, disabled) {
      if (!element.id) {
          return;
      }
      Array.prototype.forEach.call(document.querySelectorAll('aui-label[for="' + element.id + '"]'), function (el) {
          el.disabled = disabled;
      });
  }
  
  /**
   * Workaround to prevent pressing SPACE on busy state.
   * Preventing click event still makes the toggle flip and revert back.
   * So on CSS side, the input has "pointer-events: none" on busy state.
   */
  function bindEventsToInput(element) {
      element._input.addEventListener('keydown', function (e) {
          if (element.busy && e.keyCode === AJS.keyCode.SPACE) {
              e.preventDefault();
          }
      });
      // prevent toggle can be trigger through SPACE key on Firefox
      if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
          element._input.addEventListener('click', function (e) {
              if (element.busy) {
                  e.preventDefault();
              }
          });
      }
  }
  
  (0, _skate2.default)('aui-toggle', {
      template: (0, _skatejsTemplateHtml2.default)('<input type="checkbox" class="aui-toggle-input">', '<span class="aui-toggle-view">', '<span class="aui-toggle-tick aui-icon aui-icon-small aui-iconfont-success"></span>', '<span class="aui-toggle-cross aui-icon aui-icon-small aui-iconfont-close-dialog"></span>', '</span>'),
      created: function created(element) {
          element._input = getInput(element); // avoid using _input in attribute handlers
          element._tick = element.querySelector('.aui-toggle-tick');
          element._cross = element.querySelector('.aui-toggle-cross');
  
          (0, _jquery2.default)(element._input).tooltip({
              title: function title() {
                  return this.checked ? this.getAttribute('tooltip-on') : this.getAttribute('tooltip-off');
              },
              gravity: 's',
              hoverable: false
          });
          bindEventsToInput(element);
      },
      attached: function attached(element) {
          (0, _enforcer2.default)(element).attributeExists('label');
      },
      events: {
          click: clickHandler
      },
      attributes: {
          id: idAttributeHandler,
          checked: checkedAttributeHandler,
          disabled: getAttributeHandler('disabled'),
          form: formAttributeHandler,
          name: getAttributeHandler('name'),
          value: getAttributeHandler('value'),
          'tooltip-on': {
              value: AJS.I18n.getText('aui.toggle.on'),
              fallback: function fallback(element, change) {
                  getInput(element).setAttribute('tooltip-on', change.newValue || AJS.I18n.getText('aui.toggle.on'));
              }
          },
          'tooltip-off': {
              value: AJS.I18n.getText('aui.toggle.off'),
              fallback: function fallback(element, change) {
                  getInput(element).setAttribute('tooltip-off', change.newValue || AJS.I18n.getText('aui.toggle.off'));
              }
          },
          label: labelHandler
      },
      prototype: {
          focus: function focus() {
              this._input.focus();
              return this;
          },
          get checked() {
              return this._input.checked;
          },
          set checked(value) {
              // Need to explicitly set the property on the checkbox because the
              // checkbox's property doesn't change with it's attribute after it
              // is clicked.
              this._input.checked = value;
              return (0, _attributes.setBooleanAttribute)(this, 'checked', value);
          },
          get disabled() {
              return this._input.disabled;
          },
          set disabled(value) {
              return (0, _attributes.setBooleanAttribute)(this, 'disabled', value);
          },
          get form() {
              return document.getElementById(this._formId);
          },
          set form(value) {
              formAttributeHandler.fallback.call(this, this, { newValue: value || null });
              return this.form;
          },
          get name() {
              return this._input.name;
          },
          set name(value) {
              this.setAttribute('name', value);
              return value;
          },
          get value() {
              return this._input.value;
          },
          set value(value) {
              // Setting the value of an input to null sets it to empty string.
              this.setAttribute('value', value === null ? '' : value);
              return value;
          },
          get busy() {
              return this._input.getAttribute('aria-busy') === 'true';
          },
          set busy(value) {
              (0, _attributes.setBooleanAttribute)(this, 'busy', value);
              if (value) {
                  this._input.setAttribute('aria-busy', 'true');
                  this._input.indeterminate = true;
                  if (this.checked) {
                      (0, _jquery2.default)(this._input).addClass('indeterminate-checked');
                      (0, _jquery2.default)(this._tick).spin({ zIndex: null });
                  } else {
                      (0, _jquery2.default)(this._cross).spin({ zIndex: null, color: 'black' });
                  }
              } else {
                  (0, _jquery2.default)(this._input).removeClass('indeterminate-checked');
                  this._input.indeterminate = false;
                  this._input.removeAttribute('aria-busy');
                  (0, _jquery2.default)(this._cross).spinStop();
                  (0, _jquery2.default)(this._tick).spinStop();
              }
              setDisabledForLabels(this, !!value);
              return value;
          }
      }
  });
  
  return module.exports;
}).call(this);
// src/js/aui-hipchat.js
(typeof window === 'undefined' ? global : window).__b73405a7089368dbd43787aa30ece495 = (function () {
  var module = {
    exports: {}
  };
  var exports = module.exports;
  
  'use strict';
  
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  
  __fee2fb03e0889c96059239d94019b7c0;
  
  __046e6926067d85eab02f7d242b467f2c;
  
  __0c3611e861f995f21843e90f51ae6229;
  
  __19da4b233650ddabf42736b8eabb9c7a;
  
  exports.default = window.AJS;
  module.exports = exports['default'];
  
  return module.exports;
}).call(this);